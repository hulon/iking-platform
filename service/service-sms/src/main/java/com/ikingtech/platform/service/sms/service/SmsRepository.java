package com.ikingtech.platform.service.sms.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.sms.entity.SmsDO;
import com.ikingtech.platform.service.sms.mapper.SmsMapper;

/**
 * @author tie yan
 */
public class SmsRepository extends ServiceImpl<SmsMapper, SmsDO> {
}

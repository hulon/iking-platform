package com.ikingtech.platform.service.pay.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.pay.entity.CashierSupplierDO;
import com.ikingtech.platform.service.pay.mapper.CashierSupplierMapper;

/**
 * @author tie yan
 */
public class CashierSupplierRepository extends ServiceImpl<CashierSupplierMapper, CashierSupplierDO> {
}

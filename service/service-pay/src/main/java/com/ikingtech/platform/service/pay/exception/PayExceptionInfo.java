package com.ikingtech.platform.service.pay.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum PayExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 支付记录不存在
     */
    PAY_RECORD_NOT_FOUND("payRecordNotFound"),

    /**
     * 支付平台不存在
     */
    CASHIER_SUPPLIER_NOT_FOUND("cashierSupplierNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "service-pay";
    }
}

package com.ikingtech.platform.service.cluster.server;

import com.ikingtech.framework.sdk.cluster.client.ClusterClient;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class ClusterNodeHeartBeatThread extends Thread{

    private boolean stop = false;

    private final ClusterClient client;

    @Override
    public void run() {
        while (!stop) {
            this.client.publishHeartBeatMessage();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                this.stop = true;
                Thread.currentThread().interrupt();
            }
        }
    }
}

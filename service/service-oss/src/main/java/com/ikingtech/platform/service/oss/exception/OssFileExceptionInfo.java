package com.ikingtech.platform.service.oss.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum OssFileExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 文件不允许为空
     */
    FILE_CAN_NOT_BE_EMPTY("fileCannotBeEmpty"),

    /**
     * 文件不存在
     */
    FILE_NOT_FOUND("fileNotFound"),

    /**
     * 文件名称不允许为空
     */
    FILE_ORIGIN_NAME_CAN_NOT_BE_EMPTY("fileOriginNameCannotBeEmpty"),

    /**
     * 上传文件失败
     */
    UPLOAD_FILE_ERROR("uploadFileError"),

    /**
     * 获取文件输入流失败
     */
    GET_FILE_INPUT_STREAM_FAIL("getFileInputStreamFail"),

    /**
     * 获取文件大小失败
     */
    GET_FILE_SIZE_FAIL("getFileSizeFail");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "service-oss";
    }
}

package com.ikingtech.platform.service.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.oss.entity.OssFileDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface OssFileMapper extends BaseMapper<OssFileDO> {
}

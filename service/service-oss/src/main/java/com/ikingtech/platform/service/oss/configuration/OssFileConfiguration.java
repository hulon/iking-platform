package com.ikingtech.platform.service.oss.configuration;

import com.ikingtech.platform.service.oss.service.OssFileRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class OssFileConfiguration {

    @Bean
    public OssFileRepository ossFileRepository() {
        return new OssFileRepository();
    }
}

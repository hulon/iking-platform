package com.ikingtech.platform.service.oss.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.io.ByteSource;
import com.google.common.io.ByteStreams;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.log.embedded.annotation.OperationLog;
import com.ikingtech.framework.sdk.oss.api.OssApi;
import com.ikingtech.framework.sdk.oss.embedded.core.FileTemplate;
import com.ikingtech.framework.sdk.oss.embedded.core.OssResponse;
import com.ikingtech.framework.sdk.oss.embedded.properties.OssProperties;
import com.ikingtech.framework.sdk.oss.model.OssFileDTO;
import com.ikingtech.framework.sdk.oss.model.OssFileQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.oss.entity.OssFileDO;
import com.ikingtech.platform.service.oss.exception.OssFileExceptionInfo;
import com.ikingtech.platform.service.oss.service.OssFileRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/oss", name = "文件中心", description = "文件中心")
public class OssFileController implements OssApi {

    private final OssFileRepository repo;

    private final FileTemplate template;

    private final OssProperties properties;

    /**
     * 删除文件
     *
     * @param fileUrl 文件URL
     * @return 删除结果
     */
    @Override
    @OperationLog(value = "删除文件")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> delete(String fileUrl) {
        // 查询文件实体
        OssFileDO entity = this.repo.getOne(Wrappers.<OssFileDO>lambdaQuery().eq(OssFileDO::getUrl, fileUrl));
        if (null == entity) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
        // 删除文件
        this.template.removeObject(entity.getDirName(), entity.getPath());
        // 删除文件实体
        this.repo.remove(Wrappers.<OssFileDO>lambdaQuery().eq(OssFileDO::getUrl, fileUrl));
        return R.ok();
    }

    /**
     * 更新文件
     *
     * @param file 文件对象
     * @return 更新结果
     */
    @Override
    @OperationLog(value = "更新文件")
    @Transactional(rollbackFor = Exception.class)
    public R<OssFileDTO> update(OssFileDTO file) {
        // 检查文件是否存在
        if (!this.repo.exists(Wrappers.<OssFileDO>lambdaQuery().eq(OssFileDO::getId, file.getId()))) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
        // 更新文件信息
        this.repo.updateById(Tools.Bean.copy(file, OssFileDO.class));
        return R.ok();
    }

    /**
     * 分页查询OssFileDTO列表
     *
     * @param queryParam 查询参数
     * @return 分页结果
     */
    @Override
    public R<List<OssFileDTO>> page(OssFileQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<OssFileDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), OssFileDO::getOriginName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getDirName()), OssFileDO::getDirName, queryParam.getDirName())
                .eq(Tools.Str.isNotBlank(Me.tenantCode()), OssFileDO::getTenantCode, Me.tenantCode()))).convert(entity -> Tools.Bean.copy(entity, OssFileDTO.class)));
    }

    /**
     * 获取所有OssFileDTO对象
     *
     * @return 所有OssFileDTO对象的列表
     */
    @Override
    public R<List<OssFileDTO>> all() {
        // 将repo中的所有对象转换为OssFileDTO对象，并返回
        return R.ok(Tools.Coll.convertList(this.repo.list(Wrappers.<OssFileDO>lambdaQuery().eq(Tools.Str.isNotBlank(Me.tenantCode()), OssFileDO::getTenantCode, Me.tenantCode())), entity -> Tools.Bean.copy(entity, OssFileDTO.class)));
    }

    /**
     * 根据id批量查询OssFileDTO列表
     *
     * @param ids 批量查询的id列表
     * @return 查询结果
     */
    @Override
    public R<List<OssFileDTO>> listByIds(BatchParam<String> ids) {
        // 如果id列表为空，则返回空列表
        if (Tools.Coll.isBlank(ids.getList())) {
            return R.ok(new ArrayList<>());
        }
        // 调用repo的listByIds方法查询id列表对应的实体列表
        // 将查询结果转换为OssFileDTO列表并返回
        return R.ok(Tools.Coll.convertList(this.repo.listByIds(ids.getList()), entity -> Tools.Bean.copy(entity, OssFileDTO.class)));
    }

    /**
     * 上传文件
     *
     * @param file 要上传的文件
     * @param dir  上传目录
     * @return 上传结果
     */
    @Override
    public R<OssFileDTO> upload(MultipartFile file, String dir) {
        return this.upload(file, dir, false);
    }

    /**
     * 上传文件
     *
     * @param file           要上传的文件
     * @param dir            上传目录
     * @param pathIncludeDir 路径中是否包含目录
     * @return 上传结果
     */
    public R<OssFileDTO> upload(MultipartFile file, String dir, boolean pathIncludeDir) {
        // 预检查文件
        preCheck(file);
        // 预检查文件名
        String originalFilename = this.preCheck(file);
        try (InputStream inputStream = file.getInputStream()) {
            // 调用上传方法
            OssFileDTO result = this.upload(dir, originalFilename, inputStream, file.getContentType(), pathIncludeDir);
            // 返回上传结果
            return R.ok(result);
        } catch (Exception e) {
            // 抛出异常
            throw new FrameworkException(OssFileExceptionInfo.GET_FILE_INPUT_STREAM_FAIL);
        }
    }

    /**
     * 上传字节数组文件
     *
     * @param fileName 文件名
     * @param dir      存储目录
     * @param fileByte 文件字节数组
     * @return 上传结果
     */
    @Override
    public R<OssFileDTO> uploadByte(String fileName, String dir, byte[] fileByte) {
        return this.uploadByte(fileName, dir, fileByte, false);
    }

    /**
     * 上传字节数组文件
     *
     * @param fileName 文件名
     * @param dir      存储目录
     * @param fileByte 文件字节数组
     * @return 上传结果
     */
    public R<OssFileDTO> uploadByte(String fileName, String dir, byte[] fileByte, boolean pathIncludeDir) {
        if (Tools.Str.isBlank(fileName)) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_ORIGIN_NAME_CAN_NOT_BE_EMPTY);
        }
        try {
            return R.ok(this.upload(dir, fileName, ByteSource.wrap(fileByte).openStream(), MediaType.APPLICATION_OCTET_STREAM_VALUE, pathIncludeDir));
        } catch (NullPointerException | IOException e) {
            throw new FrameworkException(OssFileExceptionInfo.UPLOAD_FILE_ERROR);
        }
    }

    @Override
    public void download(String fileUrl, Boolean preview, HttpServletResponse response) {
        OssFileDO entity = this.repo.getOne(Wrappers.<OssFileDO>lambdaQuery().eq(OssFileDO::getUrl, fileUrl));
        if (null == entity) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
        try {
            OssResponse result = this.template.getObject(this.properties.getDefaultBucketName(), entity.getPath());
            response.setContentType(result.getContentType());
            if (!Boolean.TRUE.equals(preview)) {
                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(entity.getOriginName(), Charset.defaultCharset()));
            }
            ByteStreams.copy(result.getObjectStream(), response.getOutputStream());
        } catch (Exception e) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
    }

    /**
     * 下载文件
     *
     * @param fileUrl 文件URL
     * @return 文件内容
     */
    @Override
    public R<byte[]> downloadByte(String fileUrl) {
        // 根据文件URL获取文件信息
        OssFileDO entity = this.repo.getOne(Wrappers.<OssFileDO>lambdaQuery().eq(OssFileDO::getUrl, fileUrl));
        // 如果文件信息为空，则抛出异常
        if (null == entity) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
        try {
            // 获取文件内容
            OssResponse result = this.template.getObject(entity.getDirName(), entity.getPath());
            return R.ok(ByteStreams.toByteArray(result.getObjectStream()));
        } catch (Exception e) {
            // 如果获取文件内容失败，则抛出异常
            throw new FrameworkException(OssFileExceptionInfo.FILE_NOT_FOUND);
        }
    }

    /**
     * 上传文件到OSS
     *
     * @param dir              文件目录 例如 : /upload/images
     * @param originalFileName 原始文件名
     * @param inputStream      输入流
     * @param contentType      内容类型
     * @return OssFileDTO对象
     */
    private OssFileDTO upload(String dir, String originalFileName, InputStream inputStream, String contentType, boolean pathIncludeDir) {
        // 生成文件ID
        String fileId = Tools.Id.uuid();
        // 文件路径
        String filePath;
        // 获取文件后缀名
        String fileSuffix = originalFileName.substring(originalFileName.lastIndexOf('.') + 1);
        // 文件大小
        long fileSize;

        try {
            // 获取输入流大小
            fileSize = inputStream.available();
        } catch (IOException ignored) {
            // 抛出异常
            throw new FrameworkException(OssFileExceptionInfo.GET_FILE_SIZE_FAIL);
        }
        String prefix = pathIncludeDir && Tools.Str.isNotBlank(dir) ? dir : "";
        try {
            // 上传文件到OSS
            filePath = this.template.putObject(this.properties.getDefaultBucketName(), prefix, fileId + "." + fileSuffix, inputStream, contentType);
        } catch (Exception ignored) {
            // 抛出异常
            throw new FrameworkException(OssFileExceptionInfo.UPLOAD_FILE_ERROR);
        }
        // 保存OSS文件信息
        OssFileDO entity = new OssFileDO();
        entity.setId(fileId);
        entity.setDomainCode(Me.domainCode());
        entity.setTenantCode(Me.tenantCode());
        entity.setAppCode(Me.appCode());
        entity.setDirName(dir);
        entity.setOriginName(originalFileName);
        entity.setSuffix(fileSuffix);
        entity.setFileSize(fileSize);
        entity.setUrl(fileId);
        entity.setPath(filePath);
        this.repo.save(entity);
        return Tools.Bean.copy(entity, OssFileDTO.class);
    }


    private String preCheck(MultipartFile file) {
        if (file == null || file.isEmpty()) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_CAN_NOT_BE_EMPTY);
        }

        String originalFilename = file.getOriginalFilename();
        if (null == originalFilename || Tools.Str.isBlank(originalFilename)) {
            throw new FrameworkException(OssFileExceptionInfo.FILE_ORIGIN_NAME_CAN_NOT_BE_EMPTY);
        }
        return originalFilename;
    }
}

package com.ikingtech.platform.service.oss.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.oss.entity.OssFileDO;
import com.ikingtech.platform.service.oss.mapper.OssFileMapper;

/**
 * @author tie yan
 */
public class OssFileRepository extends ServiceImpl<OssFileMapper, OssFileDO> {
}

package com.ikingtech.platform.service.wechat.mini.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.wechat.mini.entity.WechatMiniDO;
import com.ikingtech.platform.service.wechat.mini.mapper.WechatMiniMapper;

/**
 * @author tie yan
 */
public class WechatMiniRepository extends ServiceImpl<WechatMiniMapper, WechatMiniDO> {
}

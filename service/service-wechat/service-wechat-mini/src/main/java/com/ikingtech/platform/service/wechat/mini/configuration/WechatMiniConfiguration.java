package com.ikingtech.platform.service.wechat.mini.configuration;

import com.ikingtech.platform.service.wechat.mini.service.WechatMiniRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class WechatMiniConfiguration {

    @Bean
    public WechatMiniRepository wechatMiniRepository() {
        return new WechatMiniRepository();
    }
}

package com.ikingtech.platform.service.tenant.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.enums.system.tenant.TenantStatusEnum;
import com.ikingtech.framework.sdk.user.api.UserTenantApi;
import com.ikingtech.framework.sdk.user.model.UserTenantDTO;
import com.ikingtech.platform.service.tenant.entity.TenantDO;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class UserTenantService implements UserTenantApi {

    private final TenantRepository repo;

    /**
     * 根据租户代码加载租户信息
     * @param code 租户代码
     * @return UserTenantDTO 租户信息
     */
    @Override
    public UserTenantDTO loadByCode(String code) {
        // 根据租户代码获取租户实体
        TenantDO entity = this.repo.getOne(Wrappers.<TenantDO>lambdaQuery().eq(TenantDO::getCode, code));
        // 创建租户信息对象
        UserTenantDTO userTenant = new UserTenantDTO();
        // 设置租户ID
        userTenant.setTenantId(entity.getId());
        // 设置租户代码
        userTenant.setTenantCode(entity.getCode());
        // 设置租户名称
        userTenant.setTenantName(entity.getName());
        // 设置租户状态
        userTenant.setTenantStatus(TenantStatusEnum.valueOf(entity.getStatus()));
        // 设置租户状态名称
        userTenant.setTenantStatusName(userTenant.getTenantStatus().description);
        // 设置租户图标
        userTenant.setTenantIcon(entity.getIcon());
        // 设置租户Logo
        userTenant.setTenantLogo(entity.getLogo());
        // 返回租户信息
        return userTenant;
    }
}

package com.ikingtech.platform.service.tenant.configuration;

import com.ikingtech.platform.service.tenant.service.TenantRepository;
import com.ikingtech.platform.service.tenant.service.UserTenantService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class TenantConfiguration {

    @Bean
    public TenantRepository tenantRepository() {
        return new TenantRepository();
    }

    @Bean
    public UserTenantService userTenantService(TenantRepository repo) {
        return new UserTenantService(repo);
    }
}

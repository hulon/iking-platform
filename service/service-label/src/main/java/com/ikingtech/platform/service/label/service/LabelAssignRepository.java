package com.ikingtech.platform.service.label.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.label.entity.LabelAssignDO;
import com.ikingtech.platform.service.label.mapper.LabelAssignMapper;

/**
 * @author tie yan
 */
public class LabelAssignRepository extends ServiceImpl<LabelAssignMapper, LabelAssignDO> {
}

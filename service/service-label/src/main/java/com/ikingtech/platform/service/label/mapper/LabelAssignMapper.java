package com.ikingtech.platform.service.label.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.label.entity.LabelAssignDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface LabelAssignMapper extends BaseMapper<LabelAssignDO> {
}

package com.ikingtech.platform.service.label.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.label.entity.LabelDO;
import com.ikingtech.platform.service.label.mapper.LabelMapper;

/**
 * @author tie yan
 */
public class LabelRepository extends ServiceImpl<LabelMapper, LabelDO> {
}

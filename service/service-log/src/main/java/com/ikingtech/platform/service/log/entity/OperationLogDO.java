package com.ikingtech.platform.service.log.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志记录
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "operation_log")
public class OperationLogDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6315937870243240637L;

    @TableField("domain_code")
    private String domainCode;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("app_code")
    private String appCode;

    @TableField(value = "module")
    private String module;

    @TableField(value = "operation")
    private String operation;

    @TableField(value = "method")
    private String method;

    @TableField(value = "request_method")
    private String requestMethod;

    @TableField(value = "request_url")
    private String requestUrl;

    @TableField(value = "request_param")
    private String requestParam;

    @TableField(value = "response_body")
    private String responseBody;

    @TableField(value = "operate_user_id")
    private String operateUserId;

    @TableField(value = "operate_username")
    private String operateUsername;

    @TableField(value = "ip")
    private String ip;

    @TableField(value = "location")
    private String location;

    @TableField(value = "success")
    private Boolean success;

    @TableField(value = "message")
    private String message;

    @TableField(value = "operation_time")
    private LocalDateTime operationTime;
}

package com.ikingtech.platform.service.log.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.log.model.SystemLogQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.log.entity.SystemLogDO;
import com.ikingtech.platform.service.log.mapper.SystemLogMapper;

/**
 * 操作日志实现类
 *
 * @author wangbo
 */
public class SystemLogRepository extends ServiceImpl<SystemLogMapper, SystemLogDO> {

    public static LambdaQueryWrapper<SystemLogDO> createWrapper(SystemLogQueryParamDTO queryParam) {
        return Wrappers.<SystemLogDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getMethod()), SystemLogDO::getMethod, queryParam.getMethod())
                .like(Tools.Str.isNotBlank(queryParam.getPackageName()), SystemLogDO::getPackageName, queryParam.getPackageName())
                .like(Tools.Str.isNotBlank(queryParam.getTraceId()), SystemLogDO::getTraceId, queryParam.getTraceId())
                .ge(null != queryParam.getExecuteStartTime(), SystemLogDO::getExecuteTime, queryParam.getExecuteStartTime())
                .le(null != queryParam.getExecuteEndTime(), SystemLogDO::getExecuteTime, queryParam.getExecuteEndTime())
                .orderByDesc(SystemLogDO::getCreateTime);
    }
}

package com.ikingtech.platform.service.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.log.entity.SystemLogDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface SystemLogMapper extends BaseMapper<SystemLogDO> {
}

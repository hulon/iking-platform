package com.ikingtech.platform.service.plugin.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.plugin.entity.PluginDO;
import com.ikingtech.platform.service.plugin.mapper.PluginMapper;
import org.springframework.stereotype.Service;

@Service
public class PluginRepository extends ServiceImpl<PluginMapper, PluginDO> {
}

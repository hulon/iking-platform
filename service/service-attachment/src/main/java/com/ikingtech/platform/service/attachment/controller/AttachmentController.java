package com.ikingtech.platform.service.attachment.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.attachment.api.AttachmentApi;
import com.ikingtech.framework.sdk.attachment.model.AttachmentDTO;
import com.ikingtech.framework.sdk.attachment.model.AttachmentQueryParamDTO;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.attachment.entity.AttachmentDO;
import com.ikingtech.platform.service.attachment.service.AttachmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/attachment", name = "附件管理", description = "附件管理")
public class AttachmentController implements AttachmentApi {

    private final AttachmentRepository repo;

    /**
     * 保存附件
     * @param attachments 附件DTO对象
     * @return 保存结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Object> save(AttachmentDTO attachments) {
        // 将附件DTO对象转换为附件实体对象
        AttachmentDO entity = Tools.Bean.copy(attachments, AttachmentDO.class);
        // 生成附件ID
        entity.setId(Tools.Id.uuid());
        // 保存附件实体对象
        this.repo.save(entity);
        // 返回保存结果
        return R.ok();
    }

    /**
     * 批量删除数据
     * @param businessIds 批量删除的业务ID
     * @return 删除结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Object> deleteBatch(BatchParam<String> businessIds) {
        if (Tools.Coll.isNotBlank(businessIds.getList())) {
            this.repo.remove(Wrappers.<AttachmentDO>lambdaQuery().in(AttachmentDO::getBusinessId, businessIds));
        }
        return R.ok();
    }

    /**
     * 获取附件详情
     * @param id 附件ID
     * @return 附件详情
     */
    @Override
    public R<AttachmentDTO> detail(String id) {
        // 通过ID获取附件实体
        AttachmentDO entity = this.repo.getById(id);
        // 如果实体为空，则抛出异常
        if (null == entity) {
            throw new FrameworkException("对象不存在");
        }
        // 将实体转换为附件DTO对象
        AttachmentDTO attachmentFile = Tools.Bean.copy(entity, AttachmentDTO.class);
        // 返回附件详情
        return R.ok(attachmentFile);
    }

    /**
     * 分页查询附件列表
     *
     * @param queryParam 查询参数
     * @return 分页查询结果
     */
    @Override
    public R<List<AttachmentDTO>> page(AttachmentQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<AttachmentDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(queryParam.getIds()), AttachmentDO::getId, queryParam.getIds())
                .like(Tools.Str.isNotBlank(queryParam.getBusinessId()), AttachmentDO::getBusinessId, queryParam.getBusinessId())
                .orderByDesc(AttachmentDO::getCreateTime))).convert(entity -> Tools.Bean.copy(entity, AttachmentDTO.class)));
    }

    /**
     * 获取所有附件信息
     *
     * @return 返回包含附件信息的列表
     */
    @Override
    public R<List<AttachmentDTO>> all() {
        // 将数据库中的附件信息转换为附件DTO对象列表
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, AttachmentDTO.class)));
    }

    /**
     * 根据业务ID获取附件列表
     * @param businessId 业务ID
     * @return 附件列表
     */
    @Override
    public R<List<AttachmentDTO>> listByBusinessId(String businessId) {
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, AttachmentDTO.class)));
    }

    /**
     * 根据业务ID批量查询附件列表
     *
     * @param businessIds 业务ID列表
     * @return 查询结果
     */
    @Override
    public R<List<AttachmentDTO>> listByBusinessIds(BatchParam<String> businessIds) {
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, AttachmentDTO.class)));
    }
}
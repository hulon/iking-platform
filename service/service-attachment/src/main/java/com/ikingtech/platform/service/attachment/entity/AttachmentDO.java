package com.ikingtech.platform.service.attachment.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;


/**
 * @author tie yan
 */
@Data
@TableName("attachment")
@EqualsAndHashCode(callSuper = true)
public class AttachmentDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -8609764760970268542L;

    @TableField("business_id")
    private String businessId;

    @TableField("oss_file_id")
    private String ossFileId;

    @TableField("type")
    private String type;
}
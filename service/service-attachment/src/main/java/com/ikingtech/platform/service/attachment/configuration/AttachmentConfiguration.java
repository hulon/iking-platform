package com.ikingtech.platform.service.attachment.configuration;

import com.ikingtech.platform.service.attachment.service.AttachmentRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class AttachmentConfiguration {

    @Bean
    public AttachmentRepository attachmentRepository() {
        return new AttachmentRepository();
    }
}

package com.ikingtech.platform.service.magicapi;

import org.ssssssss.magicapi.core.config.MagicFunction;
import org.ssssssss.script.annotation.Comment;
import org.ssssssss.script.annotation.Function;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author tie yan
 */
public class FrameworkFunction implements MagicFunction {

    /**
     * 获取当前时间
     *
     * @return 当前时间
     */
    @Function
    @Comment("取当前时间")
    public static Date now() {
        return new Date();
    }

    /**
     * 日期格式化
     * @param target 目标日期
     * @return 格式化后的日期字符串
     */
    @Function
    public static String dateFormat(@Comment("目标日期") Date target) {
        return target == null ? null : Instant.ofEpochMilli(target.getTime()).atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 日期格式化
     * @param target 目标日期
     * @param pattern 格式
     * @return 格式化后的日期字符串
     */
    @Function
    public static String dateFormat(@Comment("目标日期") Date target, @Comment("格式") String pattern) {
        return target == null ? null : Instant.ofEpochMilli(target.getTime()).atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(pattern));
    }
}

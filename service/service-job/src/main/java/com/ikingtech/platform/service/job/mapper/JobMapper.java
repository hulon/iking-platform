package com.ikingtech.platform.service.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.job.entity.JobDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yna
 */
@Mapper
public interface JobMapper extends BaseMapper<JobDO> {
}

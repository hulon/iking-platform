package com.ikingtech.platform.service.datasource.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DatasourceExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 指定数据源不存在
     */
    DATASOURCE_NOT_FOUND("datasourceNotFound");

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-datasource";
    }
}

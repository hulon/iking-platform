package com.ikingtech.platform.service.system.dict.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dict")
public class DictDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 8868344402584193128L;

	@TableField("name")
	private String name;

	@TableField("code")
	private String code;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("remark")
	private String remark;

	@TableField("type")
	private String type;

	@TableLogic
	@TableField("del_flag")
	private Boolean delFlag;
}

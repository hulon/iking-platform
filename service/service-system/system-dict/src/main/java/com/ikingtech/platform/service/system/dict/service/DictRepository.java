package com.ikingtech.platform.service.system.dict.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.dict.model.DictQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.dict.entity.DictDO;
import com.ikingtech.platform.service.system.dict.mapper.DictMapper;

/**
 * @author tie yan
 */
public class DictRepository extends ServiceImpl<DictMapper, DictDO> {

    public static LambdaQueryWrapper<DictDO> createWrapper(DictQueryParamDTO queryParam, String tenantCode) {
        return Wrappers.<DictDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), DictDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), DictDO::getType, queryParam.getType())
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), DictDO::getRemark, queryParam.getRemark())
                .eq(DictDO::getTenantCode, tenantCode)
                .orderByDesc(DictDO::getCreateTime);
    }
}

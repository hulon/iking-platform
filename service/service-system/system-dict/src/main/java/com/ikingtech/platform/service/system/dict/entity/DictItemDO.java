package com.ikingtech.platform.service.system.dict.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dict_item")
public class DictItemDO extends SortEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = -1267736552694508540L;

	@TableField("parent_id")
	private String parentId;

	@TableField("dict_id")
	private String dictId;

	@TableField("dict_code")
	private String dictCode;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("label")
	private String label;

	@TableField("value")
	private String value;

	@TableField("remark")
	private String remark;

	@TableField("full_path")
	private String fullPath;

	@TableField("preset")
	private Boolean preset;

	@TableLogic
	@TableField("del_flag")
	private Boolean delFlag;
}

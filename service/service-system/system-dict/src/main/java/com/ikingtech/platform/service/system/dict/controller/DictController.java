package com.ikingtech.platform.service.system.dict.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.event.TenantDeleteEvent;
import com.ikingtech.framework.sdk.context.event.TenantInitEvent;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.dict.api.DictApi;
import com.ikingtech.framework.sdk.dict.model.DictDTO;
import com.ikingtech.framework.sdk.dict.model.DictQueryParamDTO;
import com.ikingtech.framework.sdk.enums.system.dictionary.DictEnum;
import com.ikingtech.framework.sdk.enums.system.dictionary.DictItemEnum;
import com.ikingtech.framework.sdk.enums.system.dictionary.DictTypeEnum;
import com.ikingtech.framework.sdk.log.embedded.annotation.OperationLog;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.platform.service.system.dict.entity.DictDO;
import com.ikingtech.platform.service.system.dict.entity.DictItemDO;
import com.ikingtech.platform.service.system.dict.exception.DictExceptionInfo;
import com.ikingtech.platform.service.system.dict.service.DictItemRepository;
import com.ikingtech.platform.service.system.dict.service.DictRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 系统管理-字典管理
 * <p>1. 字典数据需按租户隔离
 *
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/system/dict", name = "系统管理-字典管理", description = "系统管理-字典管理")
public class DictController implements DictApi {

    private final DictRepository repo;

    private final DictItemRepository itemRepo;

    /**
     * 添加字典
     *
     * @param dict 字典DTO对象
     * @return 添加结果
     */
    @Override
    @OperationLog(value = "新增字典", dataId = "#_res.getData()")
    @Transactional(rollbackFor = Exception.class)
    public R<String> add(DictDTO dict) {
        // 将字典DTO对象转换为字典DO对象
        DictDO entity = Tools.Bean.copy(dict, DictDO.class);
        // 生成唯一ID
        entity.setId(Tools.Id.uuid());
        // 设置租户代码
        entity.setTenantCode(Me.tenantCode());
        // 设置字典编码，如果字典编码为空则使用生成的ID作为编码
        entity.setCode(Tools.Str.isBlank(dict.getCode()) ? entity.getId() : dict.getCode());
        // 设置字典类型为BUSINESS
        entity.setType(DictTypeEnum.BUSINESS.name());
        // 保存字典DO对象到数据库
        this.repo.save(entity);
        // 返回添加结果
        return R.ok(entity.getId());
    }

    /**
     * 删除字典
     *
     * @param id 字典ID
     * @return 删除结果
     */
    @Override
    @OperationLog(value = "删除字典")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> delete(String id) {
        // 根据ID获取字典实体
        DictDO entity = this.repo.getById(id);
        if (null == entity) {
            return R.ok();
        }
        // 如果字典类型为系统字典，则抛出异常
        if (DictTypeEnum.SYSTEM.name().equals(entity.getType())) {
            throw new FrameworkException(DictExceptionInfo.DELETE_SYSTEM_DICT_IS_NOT_ALLOWED);
        }
        // 删除字典实体
        this.repo.removeById(id);
        // 删除关联的字典项
        this.itemRepo.remove(Wrappers.<DictItemDO>query().lambda().eq(DictItemDO::getDictId, id).eq(DictItemDO::getTenantCode, Me.tenantCode()));
        return R.ok();
    }

    /**
     * 更新字典
     *
     * @param dict 字典DTO对象
     * @return 更新结果
     */
    @Override
    @OperationLog(value = "更新字典")
    @Transactional(rollbackFor = Exception.class)
    public R<Object> update(DictDTO dict) {
        // 检查字典是否存在
        if (!this.repo.exists(Wrappers.<DictDO>lambdaQuery().eq(DictDO::getId, dict.getId()))) {
            throw new FrameworkException(DictExceptionInfo.DICT_NOT_FOUND);
        }
        // 更新字典
        this.repo.updateById(Tools.Bean.copy(dict, DictDO.class));
        return R.ok();
    }

    /**
     * 分页查询字典数据
     *
     * @param queryParam 查询参数
     * @return 分页结果
     */
    @Override
    public R<List<DictDTO>> page(DictQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()),
                        DictRepository.createWrapper(queryParam, Me.tenantCode())))
                .convert(this::modelConvert));
    }

    /**
     * 获取所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public R<List<DictDTO>> all() {
        // 根据当前用户的租户标识查询
        return R.ok(this.modelConvert(this.repo.list(Wrappers.<DictDO>lambdaQuery().eq(DictDO::getTenantCode, Me.tenantCode()).orderByDesc(DictDO::getCreateTime))));
    }

    /**
     * 获取指定id的详情信息
     *
     * @param id 指定id
     * @return 返回详情信息
     */
    @Override
    public R<DictDTO> detail(String id) {
        return R.ok(this.modelConvert(this.repo.getById(id)));
    }

    private List<DictDTO> modelConvert(List<DictDO> entities) {
        return Tools.Coll.convertList(entities, this::modelConvert);
    }

    private DictDTO modelConvert(DictDO entity) {
        DictDTO dict = Tools.Bean.copy(entity, DictDTO.class);
        if (null != dict.getType()) {
            dict.setTypeName(DictTypeEnum.valueOf(entity.getType()).description);
        }
        return dict;
    }

    /**
     * 处理租户初始化事件的监听器
     */
    @EventListener
    public void tenantInitEventListener(TenantInitEvent event) {
        // 将枚举值转换为DictDO实体列表
        List<DictDO> entities = Tools.Array.convertList(DictEnum.values(), value -> {
            DictDO entity = new DictDO();
            entity.setId(Tools.Id.uuid());
            entity.setName(value.description);
            entity.setRemark(value.description);
            entity.setCode(value.name());
            entity.setType(DictTypeEnum.SYSTEM.name());
            entity.setTenantCode(event.getCode());
            return entity;
        });
        // 将DictDO实体列表转换为DictMap
        Map<String, String> dictMap = Tools.Coll.convertMap(entities, DictDO::getCode, DictDO::getId);
        // 将枚举值转换为DictItemDO实体列表
        List<DictItemDO> itemEntities = Tools.Array.convertList(DictItemEnum.values(), value -> {
            DictItemDO entity = new DictItemDO();
            entity.setId(Tools.Id.uuid());
            entity.setLabel(value.description);
            entity.setValue(value.name());
            entity.setRemark(value.description);
            entity.setDictCode(value.dict.name());
            entity.setDictId(dictMap.get(value.dict.name()));
            entity.setFullPath(value.name());
            entity.setTenantCode(event.getCode());
            return entity;
        });
        // 如果DictDO实体列表不为空，则保存到数据库
        if (Tools.Coll.isNotBlank(entities)) {
            this.repo.saveBatch(entities);
            // 如果DictItemDO实体列表不为空，则保存到数据库
            if (Tools.Coll.isNotBlank(itemEntities)) {
                this.itemRepo.saveBatch(itemEntities);
            }
        }
    }

    /**
     * 处理租户删除事件
     *
     * @param event 租户删除事件对象
     */
    @EventListener
    public void tenantDeleteEventListener(TenantDeleteEvent event) {
        // 根据租户代码查询并删除字典数据
        this.repo.remove(Wrappers.<DictDO>query().lambda().eq(DictDO::getTenantCode, event.getCode()));
        // 根据租户代码查询并删除字典项数据
        this.itemRepo.remove(Wrappers.<DictItemDO>query().lambda().eq(DictItemDO::getTenantCode, event.getCode()));
    }
}

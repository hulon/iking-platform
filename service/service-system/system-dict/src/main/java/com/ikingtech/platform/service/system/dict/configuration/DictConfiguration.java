package com.ikingtech.platform.service.system.dict.configuration;

import com.ikingtech.platform.service.system.dict.service.DictItemRepository;
import com.ikingtech.platform.service.system.dict.service.DictRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class DictConfiguration {

    @Bean
    public DictRepository dictRepository() {
        return new DictRepository();
    }

    @Bean
    public DictItemRepository dictItemRepository() {
        return new DictItemRepository();
    }
}

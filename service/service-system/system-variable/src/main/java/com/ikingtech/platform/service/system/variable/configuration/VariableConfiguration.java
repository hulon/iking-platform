package com.ikingtech.platform.service.system.variable.configuration;

import com.ikingtech.platform.service.system.variable.service.VariableRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class VariableConfiguration {

    @Bean
    public VariableRepository variableRepository() {
        return new VariableRepository();
    }
}

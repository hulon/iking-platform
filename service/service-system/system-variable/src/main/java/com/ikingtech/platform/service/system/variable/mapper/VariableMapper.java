package com.ikingtech.platform.service.system.variable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.variable.entity.VariableDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface VariableMapper extends BaseMapper<VariableDO> {
}

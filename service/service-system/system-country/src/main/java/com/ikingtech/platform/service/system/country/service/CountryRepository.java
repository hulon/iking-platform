package com.ikingtech.platform.service.system.country.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.country.entity.CountryDO;
import com.ikingtech.platform.service.system.country.mapper.CountryMapper;

/**
 * <a href="https://github.com/hlbj105/country-code">country-code</a>
 *
 * @author tie yan
 */
public class CountryRepository extends ServiceImpl<CountryMapper, CountryDO> {
}

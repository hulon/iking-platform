package com.ikingtech.platform.service.system.country.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.country.entity.CountryDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface CountryMapper extends BaseMapper<CountryDO> {
}

package com.ikingtech.platform.service.system.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.post.api.PostUserApi;
import com.ikingtech.platform.service.system.user.entity.UserPostDO;
import com.ikingtech.platform.service.system.user.service.repository.UserPostRepository;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class PostUserService implements PostUserApi {

    private final UserPostRepository userPostRepo;

    @Override
    public void removeUserPost(String postId) {
        this.userPostRepo.remove(Wrappers.<UserPostDO>query().lambda().eq(UserPostDO::getPostId, postId));
    }
}

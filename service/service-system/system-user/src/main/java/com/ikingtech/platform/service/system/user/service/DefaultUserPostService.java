package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.user.api.UserPostApi;
import com.ikingtech.framework.sdk.user.model.UserPostDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultUserPostService implements UserPostApi {

    @Override
    public List<UserPostDTO> loadByIds(List<String> postIds) {
        return new ArrayList<>();
    }
}

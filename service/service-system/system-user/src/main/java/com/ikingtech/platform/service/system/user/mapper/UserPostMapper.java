package com.ikingtech.platform.service.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.user.entity.UserPostDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface UserPostMapper extends BaseMapper<UserPostDO> {
}

package com.ikingtech.platform.service.system.user.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.iking.framework.sdk.authorization.api.AuthorizationUserApi;
import com.iking.framework.sdk.authorization.model.AuthorizationUser;
import com.iking.framework.sdk.authorization.model.AuthorizationUserDepartment;
import com.iking.framework.sdk.authorization.model.AuthorizationUserRole;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.user.api.UserDeptApi;
import com.ikingtech.framework.sdk.user.api.UserRoleApi;
import com.ikingtech.framework.sdk.user.model.UserDeptDTO;
import com.ikingtech.framework.sdk.user.model.UserRoleDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.user.entity.UserDO;
import com.ikingtech.platform.service.system.user.entity.UserDeptDO;
import com.ikingtech.platform.service.system.user.entity.UserRoleDO;
import com.ikingtech.platform.service.system.user.service.repository.UserDeptRepository;
import com.ikingtech.platform.service.system.user.service.repository.UserRepository;
import com.ikingtech.platform.service.system.user.service.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class AuthorizationUserService implements AuthorizationUserApi {

    private final UserRepository repo;

    private final UserRoleRepository userRoleRepo;

    private final UserDeptRepository userDeptRepo;

    private final UserRoleApi userRoleApi;

    private final UserDeptApi userDeptApi;

    @Override
    public AuthorizationUser loadUser(String userId) {
        UserDO entity = this.repo.getById(userId);
        if (null == entity) {
            return null;
        }
        List<UserRoleDTO> roles = this.userRoleApi.loadByIds(this.userRoleRepo.listObjs(Wrappers.<UserRoleDO>lambdaQuery().select(UserRoleDO::getRoleId).eq(UserRoleDO::getUserId, entity.getId()).eq(UserRoleDO::getTenantCode, Me.tenantCode())));
        List<UserDeptDTO> departments = this.userDeptApi.loadByIds(this.userDeptRepo.listObjs(Wrappers.<UserDeptDO>lambdaQuery().select(UserDeptDO::getDeptId).in(UserDeptDO::getUserId, entity.getId())));

        AuthorizationUser user = new AuthorizationUser();
        user.setUserId(entity.getId());
        user.setUsername(entity.getUsername());
        user.setRoles(Tools.Coll.convertList(roles, role -> {
            AuthorizationUserRole authorizationUserRole = new AuthorizationUserRole();
            authorizationUserRole.setUserId(entity.getId());
            authorizationUserRole.setRoleId(role.getRoleId());
            authorizationUserRole.setDataScopeType(role.getRoleDataScopeType());
            authorizationUserRole.setDataScopeCodes(role.getRoleDataScopeCodes());
            return authorizationUserRole;
        }));
        user.setDepartments(Tools.Coll.convertList(departments, department -> {
            AuthorizationUserDepartment authorizationUserDepartment = new AuthorizationUserDepartment();
            authorizationUserDepartment.setUserId(entity.getId());
            authorizationUserDepartment.setDeptId(department.getDeptId());
            return authorizationUserDepartment;
        }));
        return user;
    }
}

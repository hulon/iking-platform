package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.user.api.UserDeptApi;
import com.ikingtech.framework.sdk.user.model.UserDeptDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultUserDeptService implements UserDeptApi {

    @Override
    public List<UserDeptDTO> loadByIds(List<String> deptIds) {
        return new ArrayList<>();
    }
}

package com.ikingtech.platform.service.system.user.service;

import com.ikingtech.framework.sdk.user.api.UserRoleApi;
import com.ikingtech.framework.sdk.user.model.UserRoleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
public class DefaultUserRoleService implements UserRoleApi {

    @Override
    public List<UserRoleDTO> loadByIds(List<String> roleIds) {
        return new ArrayList<>();
    }

    @Override
    public List<String> loadIdByMenuIds(List<String> menuIds, String tenantCode) {
        return new ArrayList<>();
    }

    @Override
    public List<String> loadIdByMenuId(String menuId, String tenantCode) {
        return new ArrayList<>();
    }
}

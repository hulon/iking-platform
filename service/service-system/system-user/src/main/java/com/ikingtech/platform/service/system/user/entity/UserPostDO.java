package com.ikingtech.platform.service.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("user_post")
public class UserPostDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 1281401001909467193L;

	@TableField(value = "user_id")
	private String userId;

	@TableField(value = "tenant_code")
	private String tenantCode;

	@TableField(value = "post_id")
	private String postId;
}

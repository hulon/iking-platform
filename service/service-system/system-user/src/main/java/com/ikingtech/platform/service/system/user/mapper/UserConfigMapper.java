package com.ikingtech.platform.service.system.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.user.entity.UserConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface UserConfigMapper extends BaseMapper<UserConfigDO> {
}

package com.ikingtech.platform.service.system.user.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.user.entity.UserRoleDO;
import com.ikingtech.platform.service.system.user.mapper.UserRoleMapper;

/**
 * @author tie yan
 */
public class UserRoleRepository extends ServiceImpl<UserRoleMapper, UserRoleDO> {
}

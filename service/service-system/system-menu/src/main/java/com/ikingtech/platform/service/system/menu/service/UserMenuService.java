package com.ikingtech.platform.service.system.menu.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.user.api.UserMenuApi;
import com.ikingtech.platform.service.system.menu.entity.MenuDO;
import com.ikingtech.platform.service.system.menu.service.repository.MenuRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class UserMenuService implements UserMenuApi {

    private final MenuRepository repo;

    /**
     * 根据菜单编码列表和租户编码加载菜单ID列表
     *
     * @param menuCodes 菜单编码列表
     * @param tenantCode 租户编码
     * @return 菜单ID列表
     */
    @Override
    public List<String> loadIdByCodes(List<String> menuCodes, String tenantCode) {
        return this.repo.listObjs(Wrappers.<MenuDO>lambdaQuery()
                .select(MenuDO::getId)
                .in(MenuDO::getPermissionCode, menuCodes)
                .eq(MenuDO::getTenantCode, tenantCode), String.class::cast);
    }

    /**
     * 根据菜单编码和租户编码加载菜单ID
     *
     * @param menuCode 菜单编码
     * @param tenantCode 租户编码
     * @return 菜单ID
     */
    @Override
    public String loadIdByCode(String menuCode, String tenantCode) {
        return this.repo.getObj(Wrappers.<MenuDO>lambdaQuery()
                .select(MenuDO::getId)
                .eq(MenuDO::getPermissionCode, menuCode)
                .eq(MenuDO::getTenantCode, tenantCode), String.class::cast);
    }
}

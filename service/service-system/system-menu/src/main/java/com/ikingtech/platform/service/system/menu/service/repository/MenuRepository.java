package com.ikingtech.platform.service.system.menu.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.menu.entity.MenuDO;
import com.ikingtech.platform.service.system.menu.exception.MenuExceptionInfo;
import com.ikingtech.platform.service.system.menu.mapper.MenuMapper;

/**
 * @author tie yan
 */
public class MenuRepository extends ServiceImpl<MenuMapper, MenuDO> {

    public String parseFullPath(String parentId, String id) {
        if (Tools.Str.isNotBlank(parentId)) {
            MenuDO parentMenuEntity = this.getById(parentId);
            if (null == parentMenuEntity) {
                throw new FrameworkException(MenuExceptionInfo.PARENT_MENU_NOT_FOUND);
            }
            return parentMenuEntity.getFullPath() + "@" + id;
        } else {
            return id;
        }
    }
}

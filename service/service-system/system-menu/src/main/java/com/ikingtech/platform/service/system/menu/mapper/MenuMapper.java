package com.ikingtech.platform.service.system.menu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.menu.entity.MenuDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface MenuMapper extends BaseMapper<MenuDO> {
}

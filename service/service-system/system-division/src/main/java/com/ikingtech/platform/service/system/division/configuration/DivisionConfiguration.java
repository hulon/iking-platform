package com.ikingtech.platform.service.system.division.configuration;

import com.ikingtech.platform.service.system.division.service.DivisionRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tie yan
 */
@Configuration
public class DivisionConfiguration {

    @Bean
    public DivisionRepository divisionRepository() {
        return new DivisionRepository();
    }
}

package com.ikingtech.platform.service.system.division.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.division.api.DivisionApi;
import com.ikingtech.framework.sdk.division.model.DivisionDTO;
import com.ikingtech.framework.sdk.enums.system.division.DivisionLevelEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.service.system.division.entity.DivisionDO;
import com.ikingtech.platform.service.system.division.service.DivisionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 系统管理-行政区划模块
 *
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
@ApiController(value = "/system/division", name = "系统管理-行政区划模块", description = "系统管理-行政区划模块")
public class DivisionController implements DivisionApi {

    private final DivisionRepository repo;

    /**
     * 获取所有行政区域
     *
     * @return 返回包含所有行政区域的列表
     */
    @Override
    public R<List<DivisionDTO>> all() {
        return R.ok(this.modelConvert(this.repo.list()));
    }

    /**
     * 根据父ID获取行政区域列表
     *
     * @param parentId 父ID
     * @return 行政区域列表
     */
    @PostRequest(order = 2, value = "/list/parent-id", summary = "根据父ID获取行政区域列表", description = "根据父ID获取行政区域列表")
    public R<List<DivisionDTO>> listByParentId(@RequestBody String parentId) {
        return R.ok(this.modelConvert(this.repo.list(Wrappers.<DivisionDO>lambdaQuery().eq(DivisionDO::getParentId, parentId))));
    }

    /**
     * 根据父级编号查询行政区域列表
     *
     * @param parentCode 父级编号
     * @return 行政区域列表
     */
    @PostRequest(order = 3, value = "/list/parent-no", summary = "根据父级编号查询行政区域列表", description = "根据父级编号查询行政区域列表")
    public R<List<DivisionDTO>> listByParentNo(@RequestBody String parentCode) {
        // 调用模型转换方法将查询结果转换为行政区域DTO列表
        return R.ok(this.modelConvert(this.repo.list(Wrappers.<DivisionDO>lambdaQuery().eq(DivisionDO::getParentNo, parentCode))));
    }

    /**
     * 根据等级列表查询行政区域列表
     *
     * @param levels 等级列表
     * @return 行政区域列表
     */
    @PostRequest(order = 4, value = "/list/levels", summary = "根据等级列表查询行政区域列表", description = "根据等级列表查询行政区域列表")
    public R<List<DivisionDTO>> listByLevel(@RequestBody BatchParam<String> levels) {
        // 调用模型转换方法将查询结果转换为行政区域DTO列表
        return R.ok(this.modelConvert(this.repo.list(Wrappers.<DivisionDO>lambdaQuery().in(DivisionDO::getDivisionLevel, levels.getList()))));
    }

    /**
     * 获取所有省份和城市列表
     *
     * @return 返回省份和城市列表
     */
    @PostRequest(order = 5, value = "/list/province-city", summary = "获取所有省份和城市列表", description = "获取所有省份和城市列表")
    public R<List<DivisionDTO>> allProvinceAndCity() {
        // 调用存储层方法获取省份和城市列表
        return R.ok(this.modelConvert(this.repo.list(Wrappers.<DivisionDO>lambdaQuery().in(DivisionDO::getDivisionLevel, Tools.Coll.newList(DivisionLevelEnum.PROVINCE.name(), DivisionLevelEnum.CITY.name())))));
    }

    private List<DivisionDTO> modelConvert(List<DivisionDO> entities) {
        return Tools.Coll.convertList(entities, entity -> {
            DivisionDTO division = Tools.Bean.copy(entity, DivisionDTO.class);
            if (null != division.getDivisionLevel()) {
                division.setLevelName(DivisionLevelEnum.valueOf(entity.getDivisionLevel()).description);
            }
            return division;
        });
    }
}

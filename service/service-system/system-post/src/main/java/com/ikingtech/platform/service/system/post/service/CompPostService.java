package com.ikingtech.platform.service.system.post.service;

import com.ikingtech.framework.sdk.component.model.ComponentPost;
import com.ikingtech.platform.service.system.post.service.repository.PostRepository;

import java.util.List;

/**
 * @author tie yan
 */
public class CompPostService extends AbstractCompPostService {

    public CompPostService(PostRepository service) {
        super(service);
    }

    @Override
    public List<ComponentPost> listByName(String name) {
        return super.listByName(name);
    }
}

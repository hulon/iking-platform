package com.ikingtech.platform.service.system.post.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.service.system.post.entity.PostDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author tie yan
 */
@Mapper
public interface PostMapper extends BaseMapper<PostDO> {
}

package com.ikingtech.platform.service.system.role.service.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.role.model.RoleQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.role.entity.RoleDO;
import com.ikingtech.platform.service.system.role.mapper.RoleMapper;

/**
 * @author tie yan
 */
public class RoleRepository extends ServiceImpl<RoleMapper, RoleDO> {

    public static LambdaQueryWrapper<RoleDO> createWrapper(RoleQueryParamDTO queryParam, String tenantCode) {
        return Wrappers.<RoleDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), RoleDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getDataScopeType()), RoleDO::getDataScopeType, queryParam.getDataScopeType())
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), RoleDO::getRemark, queryParam.getRemark())
                .eq(RoleDO::getTenantCode, tenantCode)
                .orderByDesc(RoleDO::getCreateTime);
    }
}

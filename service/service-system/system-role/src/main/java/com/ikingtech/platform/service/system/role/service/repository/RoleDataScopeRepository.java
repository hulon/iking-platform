package com.ikingtech.platform.service.system.role.service.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.service.system.role.entity.RoleDataScopeDO;
import com.ikingtech.platform.service.system.role.mapper.RoleDataScopeMapper;

/**
 * @author tie yan
 */
public class RoleDataScopeRepository extends ServiceImpl<RoleDataScopeMapper, RoleDataScopeDO> {
}

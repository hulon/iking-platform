package com.ikingtech.platform.service.system.role.service;

import com.ikingtech.framework.sdk.component.model.ComponentRole;
import com.ikingtech.platform.service.system.role.service.repository.RoleRepository;

import java.util.List;

/**
 * @author tie yan
 */
public class CompRoleService extends AbstractCompRoleService {

    public CompRoleService(RoleRepository service) {
        super(service);
    }

    @Override
    public List<ComponentRole> listByName(String name) {
        return super.listByName(name);
    }
}

package com.ikingtech.platform.service.system.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("role_menu")
public class RoleMenuDO extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 5045315474414225895L;

	@TableField(value = "role_id")
	private String roleId;

	@TableField("domain_code")
	private String domainCode;

	@TableField("tenant_code")
	private String tenantCode;

	@TableField("app_code")
	private String appCode;

	@TableField(value = "menu_id")
	private String menuId;
}

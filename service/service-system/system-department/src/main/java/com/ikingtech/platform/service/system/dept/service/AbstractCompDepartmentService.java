package com.ikingtech.platform.service.system.dept.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ikingtech.framework.sdk.component.api.CompDepartmentApi;
import com.ikingtech.framework.sdk.component.model.ComponentDepartment;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.department.model.DeptBasicDTO;
import com.ikingtech.framework.sdk.department.model.DeptDTO;
import com.ikingtech.framework.sdk.enums.component.PickerElementTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.service.system.dept.entity.DepartmentDO;
import com.ikingtech.platform.service.system.dept.exception.DeptExceptionInfo;
import com.ikingtech.platform.service.system.dept.service.repository.DeptRepository;
import com.ikingtech.platform.service.system.dept.service.repository.ModelConverter;
import lombok.RequiredArgsConstructor;

import java.util.*;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public abstract class AbstractCompDepartmentService implements CompDepartmentApi {

    private final DeptRepository repo;

    private final ModelConverter converter;

    @Override
    public List<ComponentDepartment> listByName(String name) {
        List<DeptDTO> departments = this.converter.modelConvert(
                this.repo.list(
                        Wrappers.<DepartmentDO>lambdaQuery()
                                .like(DepartmentDO::getName, name)
                                .eq(DepartmentDO::getTenantCode, Me.tenantCode())
                                .orderByDesc(DepartmentDO::getCreateTime)
                )
        );
        return this.convert(departments, this.extractDepartmentFullName(Tools.Coll.convertMap(departments, DeptDTO::getId, DeptDTO::getFullPath)));
    }

    @Override
    public List<ComponentDepartment> listByParentId(String parentId) {
        List<DeptDTO> departments = this.converter.modelConvert(
                this.repo.list(
                        Wrappers.<DepartmentDO>lambdaQuery()
                                .eq(DepartmentDO::getParentId, parentId)
                                .orderByDesc(DepartmentDO::getCreateTime)
                )
        );
        return this.convert(departments);
    }

    @Override
    public ComponentDepartment getParentDepartment(String parentId) {
        DepartmentDO entity = this.repo.getById(parentId);
        if (null == entity) {
            throw new FrameworkException(DeptExceptionInfo.DEPT_NOT_FOUND);
        }
        DeptBasicDTO rootDepartment = this.converter.modelInfoConvert(entity);
        ComponentDepartment result = new ComponentDepartment();
        result.setElementType(PickerElementTypeEnum.DEPT);
        result.setElementId(rootDepartment.getId());
        result.setElementName(rootDepartment.getName());
        return result;
    }

    private List<ComponentDepartment> convert(List<DeptDTO> departments) {
        return this.convert(departments, new HashMap<>());
    }

    private List<ComponentDepartment> convert(List<DeptDTO> departments, Map<String, String> departmentFullNameMap) {
        return Tools.Coll.convertList(departments, department -> {
            ComponentDepartment compDept = new ComponentDepartment();
            compDept.setElementId(department.getId());
            compDept.setElementName(department.getName());
            compDept.setElementType(PickerElementTypeEnum.DEPT);
            compDept.setManagerId(department.getManagerId());
            if (null != department.getManager()) {
                compDept.setManagerName(department.getManager().getUserName());
            }
            compDept.setUserCount(department.getUserCount());
            compDept.setFullPath(department.getFullPath());
            compDept.setFullName(departmentFullNameMap.getOrDefault(department.getId(), Tools.Str.EMPTY));
            return compDept;
        });
    }

    @Override
    public Map<String, String> extractDepartmentFullName(Map<String, String> deptFullPathMap) {
        List<String> ids = Tools.Coll.flatMap(new ArrayList<>(deptFullPathMap.values()), fullPath -> Tools.Str.split(fullPath, "@"), Collection::stream);
        if (Tools.Coll.isBlank(ids)) {
            return new HashMap<>();
        }
        Map<String, String> departmentNameMap = Tools.Coll.convertMap(this.converter.modelInfoConvert(this.repo.listByIds(ids)), DeptBasicDTO::getId, DeptBasicDTO::getName);

        Map<String, String> result = new HashMap<>(deptFullPathMap.size());
        deptFullPathMap.forEach((deptId, deptFullPath) -> result.put(deptId, Tools.Coll.join(Tools.Coll.convertList(Tools.Str.split(deptFullPath, "@"), departmentNameMap::get), "-")));
        return result;
    }
}

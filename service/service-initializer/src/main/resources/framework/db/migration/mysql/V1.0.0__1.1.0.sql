CREATE TABLE `application`
(
    `id`              varchar(32)  NOT NULL DEFAULT '' COMMENT '主键',
    `tenant_code`     varchar(64)  NULL     DEFAULT '' COMMENT '租户标识',
    `name`            varchar(64)  NULL     DEFAULT '' COMMENT '应用名称',
    `code`            varchar(64)  NULL     DEFAULT '' COMMENT '应用标识',
    `icon`            varchar(256) NULL     DEFAULT 'iksvg_kapian' COMMENT '应用图标',
    `icon_background` varchar(256) NULL     DEFAULT '#1e90ff' COMMENT '图标背景色',
    `remark`          varchar(500) NULL     DEFAULT '' COMMENT '备注',
    `index_path`      varchar(512) NULL     DEFAULT NULL COMMENT '应用首页地址',
    `type`            varchar(64)  NULL     DEFAULT NULL COMMENT '应用类型',
    `status`          varchar(64)  NULL     DEFAULT NULL COMMENT '应用状态',
    `create_time`     datetime     NULL     DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32)  NULL     DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64)  NULL     DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime     NULL     DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32)  NULL     DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64)  NULL     DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用';

CREATE TABLE `application_assign`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `app_code`    varchar(64) NULL DEFAULT '' COMMENT '应用标识',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `enabled`     tinyint     NULL DEFAULT 1 COMMENT '是否启用',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用分配';

CREATE TABLE `application_general_data`
(
    `id`            varchar(32) NOT NULL COMMENT '主键',
    `tenant_code`   varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `page_id`       varchar(32) NULL DEFAULT '' COMMENT '页面编号',
    `business_code` varchar(64) NULL DEFAULT '' COMMENT '业务标识',
    `data`          json        NULL COMMENT '业务数据',
    `create_time`   datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='页面模型';

CREATE TABLE `application_model`
(
    `id`                varchar(32)  NOT NULL COMMENT '主键',
    `app_code`          varchar(64)  NULL DEFAULT '' COMMENT '应用标识',
    `name`              varchar(64)  NULL DEFAULT '' COMMENT '模型名称',
    `code`              varchar(64)  NULL DEFAULT '' COMMENT '模型标识',
    `type`              varchar(64)  NULL DEFAULT '' COMMENT '模型类型',
    `status`            varchar(64)  NULL DEFAULT NULL COMMENT '模型状态',
    `creation_type`     varchar(64)  NULL DEFAULT NULL COMMENT '模型创建类型',
    `datasource_id`     varchar(32)  NULL DEFAULT '' COMMENT '数据源编号',
    `sync_table_name`   varchar(64)  NULL DEFAULT '' COMMENT '同步表名称',
    `base_package_name` varchar(256) NULL DEFAULT '' COMMENT '基础包名',
    `remark`            varchar(500) NULL DEFAULT '' COMMENT '说明',
    `create_time`       datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='业务模型';

CREATE TABLE `application_model_field`
(
    `id`                   char(32)         NOT NULL COMMENT '主键',
    `tenant_code`          varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `model_id`             char(32)         NULL DEFAULT '' COMMENT '所属模型编号',
    `name`                 varchar(128)     NULL DEFAULT '' COMMENT '模型字段名称',
    `type`                 varchar(64)      NULL DEFAULT '' COMMENT '模型字段类型',
    `generic_type`         varchar(64)      NULL DEFAULT '' COMMENT '模型字段泛型',
    `remark`               varchar(500)     NULL DEFAULT '' COMMENT '说明',
    `db_table_field`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否是数据库表字段',
    `length`               varchar(32)      NULL DEFAULT '' COMMENT '数据库表字段长度',
    `db_table_field_type`  varchar(64)      NULL DEFAULT '' COMMENT '数据库表字段类型',
    `nullable`             tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否可以为空',
    `query_field`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为查询字段',
    `compare_type`         varchar(64)      NULL DEFAULT '' COMMENT '查询比较类型',
    `sync_with_datasource` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否与数据源保持同步',
    `sort_order`           int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`          datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`            varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`          varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`          datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`            varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`          varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='业务模型字段';

CREATE TABLE `application_model_relation`
(
    `id`                   char(32)    NOT NULL COMMENT '主键',
    `tenant_code`          varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `model_id`             char(32)    NULL DEFAULT '' COMMENT '附属模型编号',
    `master_model_id`      char(32)    NULL DEFAULT '' COMMENT '主模型编号',
    `foreign_key_field_id` char(32)    NULL DEFAULT '' COMMENT '外键字段编号',
    `create_time`          datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`            varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`          varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`          datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`            varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`          varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='业务模型关系';

CREATE TABLE `application_page`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `app_id`      varchar(64)  NULL DEFAULT '' COMMENT '应用编号',
    `app_code`    varchar(64)  NULL DEFAULT '' COMMENT '应用标识',
    `tenant_code` varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `parent_id`   varchar(32)  NULL DEFAULT '' COMMENT '父页面编号',
    `name`        varchar(64)  NULL DEFAULT '' COMMENT '页面名称',
    `icon`        varchar(256) NULL DEFAULT '' COMMENT '页面图标',
    `type`        varchar(64)  NULL DEFAULT '' COMMENT '页面类型',
    `link`        varchar(256) NULL DEFAULT '' COMMENT '页面链接',
    `json`        json         NULL COMMENT '页面JSON',
    `component`   varchar(512) NULL DEFAULT '' COMMENT '页面组件路径',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用页面';

CREATE TABLE `application_page_model`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `page_id`     varchar(32) NULL DEFAULT '' COMMENT '页面编号',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `model_id`    varchar(32) NULL DEFAULT '' COMMENT '模型编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用页面关联模型';

CREATE TABLE `application_page_template`
(
    `id`                varchar(32)      NOT NULL COMMENT '主键',
    `title`             varchar(128)     NULL DEFAULT '' COMMENT '名称',
    `json_data`         json             NULL COMMENT '模板定义',
    `description`       varchar(1024)    NULL DEFAULT '' COMMENT '描述',
    `internal_template` tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否为内置模板',
    `create_time`       datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='应用页面模板信息';

CREATE TABLE `approve_form`
(
    `id`                  varchar(32)      NOT NULL COMMENT '主键',
    `name`                varchar(64)      NULL DEFAULT '' COMMENT '表单名称',
    `business_type`       varchar(64)      NULL DEFAULT '' COMMENT '表单业务类型',
    `type`                varchar(64)      NULL DEFAULT '' COMMENT '表单分类',
    `icon`                varchar(512)     NULL DEFAULT '' COMMENT '表单图标',
    `icon_background`     varchar(64)      NULL DEFAULT '' COMMENT '表单图标背景色',
    `group_id`            varchar(64)      NULL DEFAULT '' COMMENT '表单分组编号',
    `remark`              varchar(500)     NULL DEFAULT '' COMMENT '表单说明',
    `configured`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '表单是否已配置',
    `report`              tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否是注册表单',
    `customize_initiator` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '表单是否支持配置发起人',
    `status`              varchar(64)      NULL DEFAULT '' COMMENT '表单状态',
    `latest_version`      tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否最新版本',
    `version_no`          int UNSIGNED     NULL DEFAULT 1 COMMENT '表单版本号',
    `sort_order`          int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `visible`             tinyint UNSIGNED NULL DEFAULT 1 COMMENT '表单在审批中心是否可见',
    `create_time`         datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`           varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`         varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`         datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`           varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`         varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单';

CREATE TABLE `approve_form_advance_config`
(
    `id`                          varchar(32)   NOT NULL COMMENT '主键',
    `form_id`                     varchar(32)   NULL DEFAULT '' COMMENT '表单编号',
    `approve_auto_distinct`       tinyint       NULL DEFAULT 0 COMMENT '是否自动去重审批人',
    `approve_distinct_type`       varchar(32)   NULL DEFAULT '' COMMENT '审批人去重类型',
    `allow_add_approve`           tinyint       NULL DEFAULT NULL,
    `allow_revoke_pass_approve`   tinyint       NULL DEFAULT 0 COMMENT '是否允许撤销审批通过的审批单',
    `allow_revoke_approving`      tinyint       NULL DEFAULT 0 COMMENT '是否允许撤销审批中的审批单',
    `allow_modify_passed_approve` tinyint       NULL DEFAULT 0 COMMENT '是否允许修改审批通过的审批单',
    `approve_opinion_required`    tinyint       NULL DEFAULT 0 COMMENT '审批意见是否必填',
    `approve_opinion`             varchar(128)  NULL DEFAULT '' COMMENT '默认审批意见',
    `approve_comment_invisible`   tinyint       NULL DEFAULT 0 COMMENT '是否显示审批意见',
    `allow_submit_by_other`       tinyint       NULL DEFAULT 0 COMMENT '是否允许他人代提交',
    `title_template`              varchar(256)  NULL DEFAULT '' COMMENT '审批标题模板',
    `summary_field_names`         varchar(1024) NULL DEFAULT '' COMMENT '摘要字段名',
    `notify_day`                  datetime      NULL DEFAULT NULL COMMENT '审批提醒日期',
    `notify_time`                 time          NULL DEFAULT NULL COMMENT '审批提醒时间',
    `report_template_id`          varchar(64)   NULL DEFAULT '' COMMENT '报表模板编号',
    `create_time`                 datetime      NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                   varchar(32)   NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`                 varchar(64)   NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`                 datetime      NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                   varchar(32)   NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`                 varchar(64)   NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单高级配置';

CREATE TABLE `approve_form_group`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `name`        varchar(64)      NULL DEFAULT '' COMMENT '表单分组名称',
    `remark`      varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `renamable`   tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否可重命名',
    `deletable`   tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否可删除',
    `sortable`    tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否可排序',
    `sort_order`  int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单分组';

CREATE TABLE `approve_form_initiator`
(
    `id`             varchar(32) NOT NULL COMMENT '主键',
    `form_id`        varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `initiator_id`   varchar(32) NULL DEFAULT '' COMMENT '表单发起人编号',
    `initiator_type` varchar(64) NULL DEFAULT '' COMMENT '表单发起人类型',
    `create_time`    datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`      varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`    varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`    datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`      varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`    varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单发起人';

CREATE TABLE `approve_form_instance`
(
    `id`                varchar(32)      NOT NULL COMMENT '主键',
    `serial_no`         varchar(32)      NULL DEFAULT '' COMMENT '审批流水号',
    `form_id`           varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `form_visible`      tinyint          NULL DEFAULT 1 COMMENT '所属表单是否可见',
    `business_type`     varchar(64)      NULL DEFAULT '' COMMENT '表单业务类型',
    `business_data_id`  varchar(32)      NULL DEFAULT '' COMMENT '业务数据编号',
    `form_name`         varchar(128)     NULL DEFAULT '' COMMENT '表单名称',
    `form_type`         varchar(32)      NULL DEFAULT '' COMMENT '表单类型',
    `title`             varchar(128)     NULL DEFAULT '' COMMENT '表单标题',
    `initiator_id`      varchar(32)      NULL DEFAULT '' COMMENT '发起人编号',
    `initiator_time`    datetime         NULL DEFAULT NULL COMMENT '发起时间',
    `form_data`         json             NULL COMMENT '表单数据',
    `form_data_summary` text             NULL COMMENT '表单数据摘要',
    `process_status`    varchar(64)      NULL DEFAULT '' COMMENT '流程状态',
    `visible`           tinyint UNSIGNED NULL DEFAULT 1 COMMENT '表单在审批中心是否可见',
    `draft`             tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否草稿',
    `create_time`       datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单实例';

CREATE TABLE `approve_form_instance_share`
(
    `id`               varchar(32) NOT NULL COMMENT '主键',
    `form_instance_id` varchar(32) NULL DEFAULT '' COMMENT '表单实例编号',
    `user_id`          varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `create_time`      datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单实例分享';

CREATE TABLE `approve_form_manager`
(
    `id`           varchar(32)  NOT NULL COMMENT '主键',
    `form_id`      varchar(32)  NULL DEFAULT '' COMMENT '表单编号',
    `manager_id`   varchar(32)  NULL DEFAULT '' COMMENT '表单管理者编号',
    `manager_type` varchar(64)  NULL DEFAULT '' COMMENT '表单管理者类型',
    `sort_order`   int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time`  datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`    varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`  varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`  datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`    varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`  varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单管理者';

CREATE TABLE `approve_form_view`
(
    `id`              varchar(32) NOT NULL COMMENT '主键',
    `form_id`         varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_node_id` varchar(32) NULL DEFAULT NULL COMMENT '所属流程节点编号',
    `property`        json        NULL COMMENT '表单定义信息',
    `submit_label`    varchar(64) NULL DEFAULT '' COMMENT '提交按钮文字',
    `refuse_label`    varchar(64) NULL DEFAULT '' COMMENT '拒绝按钮文字',
    `create_time`     datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单页面配置';

CREATE TABLE `approve_form_widget`
(
    `id`              varchar(32)      NOT NULL COMMENT '主键',
    `form_id`         varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `process_node_id` varchar(32)      NULL DEFAULT NULL COMMENT '流程节点编号',
    `widget_name`     varchar(64)      NULL DEFAULT '' COMMENT '表单控件名称',
    `widget_label`    varchar(64)      NULL DEFAULT '' COMMENT '表单控件字段标签',
    `widget_type`     varchar(32)      NULL DEFAULT NULL COMMENT '表单控件字段类型',
    `required`        tinyint UNSIGNED NULL DEFAULT 0 COMMENT '字段是否必填',
    `remain`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为前一节点保留字段',
    `create_time`     datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单页面组件';

CREATE TABLE `approve_process`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `form_id`     varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `name`        varchar(64) NULL DEFAULT '' COMMENT '流程名称',
    `property`    json        NULL COMMENT '审批流定义信息',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程';

CREATE TABLE `approve_process_cond_comp`
(
    `id`                 varchar(32) NOT NULL COMMENT '主键',
    `form_id`            varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`         varchar(32) NULL DEFAULT '' COMMENT '流程编号',
    `node_id`            varchar(32) NULL DEFAULT '' COMMENT '流程节点编号',
    `group_id`           varchar(32) NULL DEFAULT '' COMMENT '流程条件组编号',
    `widget_name`        varchar(64) NULL DEFAULT '' COMMENT '表单控件名称',
    `widget_description` varchar(64) NULL DEFAULT '' COMMENT '表单控件描述',
    `comparator`         varchar(32) NULL DEFAULT '' COMMENT '条件比较式',
    `compare_to_values`  text        NULL COMMENT '条件比较值',
    `date_pattern`       varchar(64) NULL DEFAULT '' COMMENT '日期格式',
    `create_time`        datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`          varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`        varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`        datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`          varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`        varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程节点分支条件';

CREATE TABLE `approve_process_cond_group`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `form_id`     varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`  varchar(32) NULL DEFAULT '' COMMENT '流程编号',
    `node_id`     varchar(32) NULL DEFAULT '' COMMENT '流程节点编号',
    `name`        varchar(64) NULL DEFAULT '' COMMENT '分支条件组名称',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程节点分支条件组';

CREATE TABLE `approve_process_executor`
(
    `id`                varchar(32)  NOT NULL COMMENT '主键',
    `form_id`           varchar(32)  NULL DEFAULT '' COMMENT '表单编号',
    `process_id`        varchar(32)  NULL DEFAULT '' COMMENT '流程节编号',
    `node_id`           varchar(32)  NULL DEFAULT '' COMMENT '流程节节点编号',
    `executor_id`       varchar(32)  NULL DEFAULT '' COMMENT '流程节执行者编号',
    `executor_type`     varchar(64)  NULL DEFAULT '' COMMENT '流程节执行者类型',
    `executor_category` varchar(64)  NULL DEFAULT '' COMMENT '流程节执行者类别',
    `sort_order`        int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time`       datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程节点执行者配置信息';

CREATE TABLE `approve_process_instance`
(
    `id`               varchar(32) NOT NULL COMMENT '主键',
    `name`             varchar(64) NULL DEFAULT '' COMMENT '流程实例名称',
    `form_id`          varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`       varchar(32) NULL DEFAULT '' COMMENT '流程配置编号',
    `form_instance_id` varchar(32) NULL DEFAULT '' COMMENT '表单实例编号',
    `status`           varchar(64) NULL DEFAULT '' COMMENT '流程状态',
    `create_time`      datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程实例';

CREATE TABLE `approve_process_instance_node`
(
    `id`                      varchar(32)      NOT NULL COMMENT '主键',
    `form_id`                 varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `node_form_id`            varchar(32)      NULL DEFAULT NULL COMMENT '节点表单编号',
    `process_id`              varchar(32)      NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`                 varchar(32)      NULL DEFAULT '' COMMENT '流程配置节点编号',
    `form_instance_id`        varchar(32)      NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id`     varchar(32)      NULL DEFAULT '' COMMENT '流程实例编号',
    `name`                    varchar(64)      NULL DEFAULT '' COMMENT '流程节点名称',
    `type`                    varchar(64)      NULL DEFAULT '' COMMENT '流程节点类型',
    `approve_type`            varchar(64)      NULL DEFAULT '' COMMENT '审批方式',
    `approval_category`       varchar(64)      NULL DEFAULT '' COMMENT '流程节点执行者类型',
    `single_approval`         tinyint UNSIGNED NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人',
    `multi_executor_type`     varchar(64)      NULL DEFAULT '' COMMENT '多人审批方式',
    `re_submit_to_back`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '重新发起时是否有退回人直接审批',
    `back_to_initiator`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点',
    `executor_empty_strategy` varchar(64)      NULL DEFAULT '' COMMENT '节点执行对象为空时的策略',
    `auto_execute_comment`    varchar(500)     NULL DEFAULT '' COMMENT '节点自动执行时的审批意见',
    `status`                  varchar(64)      NULL DEFAULT '' COMMENT '节点执行状态',
    `form_data`               json             NULL COMMENT '节点表单数据',
    `sort_order`              int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`             datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`               varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`             varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`             datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`               varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`             varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程实例节点';

CREATE TABLE `approve_process_instance_user`
(
    `id`                       varchar(32)  NOT NULL COMMENT '主键',
    `form_id`                  varchar(32)  NULL DEFAULT '' COMMENT '表单编号',
    `process_id`               varchar(32)  NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`                  varchar(32)  NULL DEFAULT NULL COMMENT '流程配置节点编号',
    `form_instance_id`         varchar(32)  NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id`      varchar(32)  NULL DEFAULT '' COMMENT '流程实例编号',
    `process_instance_node_id` varchar(32)  NULL DEFAULT '' COMMENT '流程实例节点编号',
    `user_id`                  varchar(32)  NULL DEFAULT '' COMMENT '流程执行用户编号',
    `status`                   varchar(64)  NULL DEFAULT '' COMMENT '流程执行用户状态',
    `sort_order`               int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time`              datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`              varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`              datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`              varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程实例节点执行人';

CREATE TABLE `approve_process_node`
(
    `id`                             varchar(32)      NOT NULL COMMENT '主键',
    `form_id`                        varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `node_form_id`                   varchar(32)      NULL DEFAULT NULL COMMENT '节点表单编号',
    `process_id`                     varchar(32)      NULL DEFAULT '' COMMENT '流程配置编号',
    `parent_id`                      varchar(32)      NULL DEFAULT '' COMMENT '前一流程节点编号',
    `name`                           varchar(64)      NULL DEFAULT '' COMMENT '流程节点名称',
    `type`                           varchar(64)      NULL DEFAULT '' COMMENT '流程节点类型',
    `approve_type`                   varchar(64)      NULL DEFAULT '' COMMENT '流程节点审批方式',
    `approval_category`              varchar(64)      NULL DEFAULT '' COMMENT '流程节点执行者类型',
    `organization_widget_name`       varchar(64)      NULL DEFAULT '' COMMENT '组织选择控件名称',
    `dept_widget_name`               varchar(64)      NULL DEFAULT '' COMMENT '部门选择控件名称',
    `user_widget_name`               varchar(64)      NULL DEFAULT '' COMMENT '联系人控件名称',
    `initiator_specified_scope_type` varchar(64)      NULL DEFAULT '' COMMENT '发起人范围类型',
    `single_approval`                tinyint UNSIGNED NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人',
    `executor_empty_strategy`        varchar(64)      NULL DEFAULT '' COMMENT '执行对象为空时的策略',
    `auto_execute_comment`           varchar(500)     NULL DEFAULT '' COMMENT '自动执行时的审批意见',
    `multi_executor_type`            varchar(64)      NULL DEFAULT '' COMMENT '多人审批方式',
    `re_submit_to_back`              tinyint UNSIGNED NULL DEFAULT 0 COMMENT '重新发起时是否由退回人直接审批',
    `back_to_initiator`              tinyint UNSIGNED NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点',
    `initiator_specify_carbon_copy`  tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否允许申请人自选抄送人',
    `condition_order`                int UNSIGNED     NULL DEFAULT 1 COMMENT '条件节点优先级',
    `create_time`                    datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                      varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`                    varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`                    datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                      varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`                    varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程节点';

CREATE TABLE `approve_process_permission`
(
    `id`              varchar(32) NOT NULL COMMENT '主键',
    `form_id`         varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`      varchar(32) NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`         varchar(32) NULL DEFAULT '' COMMENT '流程配置节点编号',
    `widget_name`     varchar(32) NULL DEFAULT '' COMMENT '表单控件名称',
    `permission_type` varchar(64) NULL DEFAULT '' COMMENT '字段权限类型',
    `create_time`     datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='表单流程节点字段权限';

CREATE TABLE `approve_record`
(
    `id`                  varchar(32) NOT NULL COMMENT '主键',
    `form_id`             varchar(32) NULL DEFAULT '' COMMENT '表单编号',
    `process_id`          varchar(32) NULL DEFAULT '' COMMENT '流程配置编号',
    `form_instance_id`    varchar(32) NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id` varchar(32) NULL DEFAULT '' COMMENT '流程实例编号',
    `name`                varchar(64) NULL DEFAULT '' COMMENT '审批记录名称',
    `create_time`         datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`           varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`         varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`         datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`           varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`         varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='审批记录';

CREATE TABLE `approve_record_node`
(
    `id`                       varchar(32)      NOT NULL COMMENT '主键',
    `record_id`                varchar(32)      NULL DEFAULT '' COMMENT '审批记录编号',
    `form_id`                  varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `process_id`               varchar(32)      NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`                  varchar(32)      NULL DEFAULT '' COMMENT '流程配置节点编号',
    `form_instance_id`         varchar(32)      NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id`      varchar(32)      NULL DEFAULT '' COMMENT '流程实例编号',
    `process_instance_node_id` varchar(32)      NULL DEFAULT '' COMMENT '流程实例节点编号',
    `name`                     varchar(64)      NULL DEFAULT '' COMMENT '流程节点名称',
    `type`                     varchar(64)      NULL DEFAULT '' COMMENT '流程节点类型',
    `approve_type`             varchar(64)      NULL DEFAULT '' COMMENT '审批方式',
    `approval_category`        varchar(64)      NULL DEFAULT '' COMMENT '工作流节点执行者类型',
    `single_approval`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '发起人自选-是否为自选单人',
    `re_submit_to_back`        tinyint UNSIGNED NULL DEFAULT 0 COMMENT '重新发起时是否有退回人直接审批',
    `back_to_initiator`        tinyint UNSIGNED NULL DEFAULT 0 COMMENT '退回时是否直接退回到发起节点',
    `executor_empty_strategy`  varchar(64)      NULL DEFAULT '' COMMENT '节点执行对象为空时的策略',
    `multi_executor_type`      varchar(64)      NULL DEFAULT '' COMMENT '多人审批方式',
    `status`                   varchar(64)      NULL DEFAULT '' COMMENT '节点状态',
    `append_executor_user_id`  varchar(32)      NULL DEFAULT '' COMMENT '追加执行者用户编号',
    `back_to_record_node_id`   varchar(32)      NULL DEFAULT '' COMMENT '退回节点编号',
    `back_to_record_node_name` varchar(64)      NULL DEFAULT '' COMMENT '退回节点名称',
    `approve_comment`          varchar(500)     NULL DEFAULT '' COMMENT '审批意见',
    `form_instance_comment`    varchar(500)     NULL DEFAULT '' COMMENT '评论',
    `attachment`               varchar(512)     NULL DEFAULT '' COMMENT '文件附件',
    `images`                   varchar(512)     NULL DEFAULT '' COMMENT '图片附件',
    `sort_order`               int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`              datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`              varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`              datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`              varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='审批记录节点';

CREATE TABLE `approve_record_node_attachment`
(
    `id`                       varchar(32)  NOT NULL,
    `form_id`                  varchar(32)  NULL DEFAULT '' COMMENT '表单编号',
    `process_id`               varchar(32)  NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`                  varchar(32)  NULL DEFAULT '' COMMENT '流程配置节点编号',
    `form_instance_id`         varchar(32)  NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id`      varchar(32)  NULL DEFAULT '' COMMENT '流程实例编号',
    `process_instance_node_id` varchar(32)  NULL DEFAULT '' COMMENT '流程实例节点编号',
    `record_id`                varchar(32)  NULL DEFAULT '' COMMENT '审批记录编号',
    `record_node_id`           varchar(32)  NULL DEFAULT '' COMMENT '审批记录节点编号',
    `attachment_id`            varchar(32)  NULL DEFAULT '' COMMENT '附件编号',
    `attachment_name`          varchar(256) NULL DEFAULT '' COMMENT '附件名称',
    `attachment_size`          varchar(64)  NULL DEFAULT '' COMMENT '附件大小',
    `attachment_suffix`        varchar(32)  NULL DEFAULT '' COMMENT '附件后缀名',
    `sort_order`               int UNSIGNED NULL DEFAULT 1,
    `create_time`              datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`              varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`              datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`              varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE `approve_record_node_user`
(
    `id`                       varchar(32)      NOT NULL COMMENT '主键',
    `form_id`                  varchar(32)      NULL DEFAULT '' COMMENT '表单编号',
    `process_id`               varchar(32)      NULL DEFAULT '' COMMENT '流程配置编号',
    `node_id`                  varchar(32)      NULL DEFAULT '' COMMENT '流程配置节点编号',
    `form_instance_id`         varchar(32)      NULL DEFAULT '' COMMENT '表单实例编号',
    `process_instance_id`      varchar(32)      NULL DEFAULT '' COMMENT '流程实例编号',
    `process_instance_node_id` varchar(32)      NULL DEFAULT '' COMMENT '流程实例节点编号',
    `record_id`                varchar(32)      NULL DEFAULT '' COMMENT '审批记录编号',
    `record_node_id`           varchar(32)      NULL DEFAULT '' COMMENT '审批记录节点编号',
    `user_id`                  varchar(32)      NULL DEFAULT '' COMMENT '用户编号',
    `status`                   varchar(64)      NULL DEFAULT '' COMMENT '用户状态',
    `append_user`              tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为加签用户',
    `sort_order`               int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`              datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`                varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`              varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`              datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`                varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`              varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='审批记录节点执行人';

CREATE TABLE `auth_log`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `domain_code` varchar(64)      NULL DEFAULT '' COMMENT '域标识',
    `tenant_code` varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `app_code`    varchar(64)      NULL DEFAULT '' COMMENT '应用标识',
    `user_id`     char(32)         NULL DEFAULT '' COMMENT '用户编号',
    `username`    varchar(64)      NULL DEFAULT '' COMMENT '用户名',
    `type`        varchar(32)      NULL DEFAULT '' COMMENT '登录类型',
    `ip`          varchar(64)      NULL DEFAULT '' COMMENT '客户端ip',
    `location`    varchar(256)     NULL DEFAULT '' COMMENT '客户端地区',
    `browser`     varchar(256)     NULL DEFAULT '' COMMENT '浏览器',
    `os`          varchar(256)     NULL DEFAULT '' COMMENT '操作系统',
    `success`     tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否成功',
    `message`     text             NULL COMMENT '返回信息',
    `sign_time`   datetime         NULL DEFAULT NULL COMMENT '登录/登出时间',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='登录日志';

CREATE TABLE `cashier_supplier`
(
    `id`               varchar(32)  NOT NULL COMMENT '主键',
    `type`             varchar(32)  NOT NULL COMMENT '支付平台类型',
    `name`             varchar(64)  NULL DEFAULT '' COMMENT '支付平台名称',
    `custom_id`        varchar(256) NULL DEFAULT '' COMMENT '支付平台商户编号',
    `custom_name`      varchar(256) NULL DEFAULT '' COMMENT '支付平台商户名称',
    `custom_serial_no` varchar(256) NULL DEFAULT '' COMMENT '支付平台商户SerialNo',
    `app_id`           varchar(256) NULL DEFAULT '' COMMENT '支付平台AppId',
    `app_secret`       varchar(256) NULL DEFAULT '' COMMENT '短信平台AppSecret',
    `api_key`          varchar(256) NULL DEFAULT '' COMMENT '支付平台AppKey',
    `extra_api_key`    varchar(256) NULL DEFAULT '' COMMENT '支付平台旧版本AppKey',
    `private_key`      varchar(512) NULL DEFAULT '' COMMENT '支付平台私钥',
    `public_key`       varchar(512) NULL DEFAULT '' COMMENT '支付平台公钥',
    `cert_key`         varchar(512) NULL DEFAULT '' COMMENT '支付平台证书',
    `notify_url`       varchar(512) NULL DEFAULT '' COMMENT '支付结果通知地址',
    `create_time`      datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='支付平台';

CREATE TABLE `country`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `code`        varchar(64)  NULL DEFAULT '' COMMENT '国家编码',
    `full_code`   varchar(64)  NULL DEFAULT '' COMMENT '国家完整编码',
    `tenant_code` varchar(32)  NULL DEFAULT '' COMMENT '国家完整编码',
    `name`        varchar(128) NULL DEFAULT '' COMMENT '国家名称',
    `eu_name`     varchar(512) NULL DEFAULT '' COMMENT '国家英文名称',
    `continent`   varchar(32)  NULL DEFAULT '' COMMENT '国家所属大洲',
    `sort_order`  int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='国家';

CREATE TABLE `database_backup_record`
(
    `id`            char(32)         NOT NULL COMMENT '主键',
    `strategy_id`   char(32)         NULL DEFAULT '' COMMENT '数据库备份策略id',
    `record_code`   varchar(64)      NULL DEFAULT NULL COMMENT '编号',
    `sql_file_path` varchar(256)     NULL DEFAULT '' COMMENT 'sql文件路径',
    `log_file_path` varchar(256)     NULL DEFAULT NULL COMMENT 'log文件路劲',
    `success`       tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否执行成功',
    `expire_time`   datetime         NULL DEFAULT NULL COMMENT '备份文件到期时间',
    `create_time`   datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     char(32)         NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     char(32)         NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='数据库备份记录';

CREATE TABLE `database_backup_strategy`
(
    `id`            char(32)         NOT NULL COMMENT '主键',
    `name`          varchar(64)      NULL DEFAULT '' COMMENT '备份策略名称（字母下划线数字）',
    `datasource_id` char(32)         NULL DEFAULT '' COMMENT '数据源id',
    `schema_name`   varchar(64)      NULL DEFAULT '' COMMENT '数据库名称',
    `type`          tinyint UNSIGNED NULL DEFAULT 1 COMMENT '备份对象类型（0自定义选表；1全表)',
    `table_names`   varchar(1024)    NULL DEFAULT '' COMMENT '备份对象（全表为null,自定义为表名字符串）',
    `cron`          varchar(64)      NULL DEFAULT '' COMMENT 'cron表达式',
    `expire_type`   int UNSIGNED     NULL DEFAULT NULL COMMENT '过期策略',
    `parent_path`   varchar(256)     NULL DEFAULT '' COMMENT '备份根目录',
    `running`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '状态（开启/暂停）',
    `remark`        varchar(256)     NULL DEFAULT '' COMMENT '描述',
    `del_flag`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`   datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     char(32)         NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     char(32)         NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='数据库备份策略';

CREATE TABLE `datasource`
(
    `id`                 varchar(32)      NOT NULL COMMENT '主键',
    `tenant_code`        varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `name`               varchar(64)      NULL DEFAULT '' COMMENT '数据源名称',
    `type`               varchar(64)      NULL DEFAULT '' COMMENT '数据源类型',
    `ip`                 varchar(64)      NULL DEFAULT '' COMMENT '数据源IP地址',
    `port`               varchar(16)      NULL DEFAULT '' COMMENT '数据源端口',
    `jdbc_url`           varchar(512)     NULL DEFAULT '' COMMENT 'jdbc链接',
    `username`           varchar(64)      NULL DEFAULT '' COMMENT '用户名',
    `password`           varchar(64)      NULL DEFAULT '' COMMENT '密码',
    `driver_class_name`  varchar(256)     NULL DEFAULT '' COMMENT '驱动类名',
    `schema_name`        varchar(64)      NULL DEFAULT '' COMMENT '数据库名',
    `remark`             varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `default_datasource` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否默认',
    `create_time`        datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`          varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`        varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`        datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`          varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`        varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='数据源';

CREATE TABLE `department`
(
    `id`                varchar(32)      NOT NULL COMMENT '主键',
    `full_path`         varchar(600)     NOT NULL COMMENT '部门全路径',
    `parent_id`         varchar(32)      NULL DEFAULT '' COMMENT '父部门编号',
    `tenant_code`       varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `name`              varchar(64)      NULL DEFAULT '' COMMENT '部门名称',
    `grade`             int UNSIGNED     NULL DEFAULT 1 COMMENT '级别',
    `code`              varchar(64)      NULL DEFAULT '' COMMENT '编码',
    `short_name`        varchar(64)      NULL DEFAULT '' COMMENT '简称',
    `manager_id`        varchar(32)      NULL DEFAULT '' COMMENT '主管领导',
    `establish_date`    datetime         NULL DEFAULT NULL COMMENT '成立日期',
    `status`            varchar(64)      NULL DEFAULT '' COMMENT '状态',
    `equity_ratio`      varchar(32)      NULL DEFAULT NULL COMMENT '股权比例',
    `province_code`     varchar(64)      NULL DEFAULT '' COMMENT '省份代码',
    `province_name`     varchar(64)      NULL DEFAULT '' COMMENT '省份名称',
    `city_code`         varchar(64)      NULL DEFAULT '' COMMENT '市代码',
    `city_name`         varchar(64)      NULL DEFAULT '' COMMENT '市名称',
    `district_code`     varchar(64)      NULL DEFAULT '' COMMENT '区代码',
    `district_name`     varchar(64)      NULL DEFAULT '' COMMENT '区名称',
    `street_code`       varchar(64)      NULL DEFAULT '' COMMENT '街道代码',
    `street_name`       varchar(64)      NULL DEFAULT '' COMMENT '街道名称',
    `address`           varchar(500)     NULL DEFAULT '' COMMENT '详细住址',
    `website`           varchar(500)     NULL DEFAULT '' COMMENT '网址',
    `postal_code`       varchar(64)      NULL DEFAULT '' COMMENT '邮编',
    `phone`             varchar(64)      NULL DEFAULT '' COMMENT '联系电话',
    `fax`               varchar(256)     NULL DEFAULT '' COMMENT '传真号',
    `introduction_type` varchar(64)      NULL DEFAULT '' COMMENT '简介类型',
    `introduction`      longtext         NULL COMMENT '简介内容',
    `org_id`            varchar(32)      NULL DEFAULT '' COMMENT '所属组织编号',
    `default_flag`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否默认部门',
    `sort_order`        int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`       datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='部门';

CREATE TABLE `dict`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `name`        varchar(64)      NULL DEFAULT '' COMMENT '字典名称',
    `code`        varchar(64)      NULL DEFAULT '' COMMENT '字典标识',
    `tenant_code` varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `remark`      varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `type`        varchar(64)      NULL DEFAULT '' COMMENT '字典类型，system:系统字典 business:业务字典',
    `del_flag`    tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='字典';

CREATE TABLE `dict_item`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `parent_id`   varchar(32)      NULL DEFAULT '' COMMENT '父字典项编号',
    `full_path`   varchar(600)     NULL DEFAULT '' COMMENT '字典项全路径',
    `dict_id`     varchar(32)      NULL DEFAULT '' COMMENT '所属字典编号',
    `dict_code`   varchar(32)      NULL DEFAULT '' COMMENT '所属字典标识',
    `tenant_code` varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `label`       varchar(64)      NULL DEFAULT '' COMMENT '字典项名称',
    `value`       varchar(64)      NULL DEFAULT '' COMMENT '字典项数据',
    `remark`      varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `preset`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否预置字典项',
    `sort_order`  int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`    tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='字典项';

CREATE TABLE `division`
(
    `id`                 varchar(32)      NOT NULL COMMENT '主键',
    `full_path`          varchar(600)     NULL DEFAULT '' COMMENT '行政区划全路径',
    `parent_id`          varchar(32)      NULL DEFAULT '' COMMENT '父行政区划编号',
    `tenant_code`        varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `no`                 varchar(32)      NULL DEFAULT '' COMMENT '行政区划编码',
    `name`               varchar(64)      NULL DEFAULT '' COMMENT '行政区划名称',
    `parent_no`          varchar(32)      NULL DEFAULT '' COMMENT '父行政区划编码',
    `division_level`     varchar(32)      NULL DEFAULT NULL COMMENT '行政区划级别',
    `provincial_capital` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为省会城市',
    `sort_order`         int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`           tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`        datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`          varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`        varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`        datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`          varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`        varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='行政区划';

CREATE TABLE `jimu_dict`
(
    `id`          varchar(32)              NOT NULL,
    `dict_name`   varchar(100)             NULL DEFAULT NULL COMMENT '字典名称',
    `dict_code`   varchar(100)             NULL DEFAULT NULL COMMENT '字典编码',
    `description` varchar(256)             NULL DEFAULT NULL COMMENT '描述',
    `del_flag`    int                      NULL DEFAULT NULL COMMENT '删除状态',
    `create_by`   varchar(32)              NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime                 NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(32)              NULL DEFAULT NULL COMMENT '更新人',
    `update_time` datetime                 NULL DEFAULT NULL COMMENT '更新时间',
    `type`        int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '字典类型0为string,1为number',
    `tenant_id`   varchar(10)              NULL DEFAULT NULL COMMENT '多租户标识',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `uk_sd_dict_code` (`dict_code` ASC) USING BTREE
);

CREATE TABLE `jimu_dict_item`
(
    `id`          varchar(32)  NOT NULL,
    `dict_id`     varchar(32)  NULL DEFAULT NULL COMMENT '字典id',
    `item_text`   varchar(100) NULL DEFAULT NULL COMMENT '字典项文本',
    `item_value`  varchar(100) NULL DEFAULT NULL COMMENT '字典项值',
    `description` varchar(256) NULL DEFAULT NULL COMMENT '描述',
    `sort_order`  int          NULL DEFAULT NULL COMMENT '排序',
    `status`      int          NULL DEFAULT NULL COMMENT '状态（1启用 0不启用）',
    `create_by`   varchar(32)  NULL DEFAULT NULL,
    `create_time` datetime     NULL DEFAULT NULL,
    `update_by`   varchar(32)  NULL DEFAULT NULL,
    `update_time` datetime     NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_sdi_role_dict_id` (`dict_id` ASC) USING BTREE,
    INDEX `idx_sdi_role_sort_order` (`sort_order` ASC) USING BTREE,
    INDEX `idx_sdi_status` (`status` ASC) USING BTREE,
    INDEX `idx_sdi_dict_val` (`dict_id` ASC, `item_value` ASC) USING BTREE
);

CREATE TABLE `jimu_report`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `code`        varchar(50)  NULL DEFAULT NULL COMMENT '编码',
    `name`        varchar(50)  NULL DEFAULT NULL COMMENT '名称',
    `note`        varchar(256) NULL DEFAULT NULL COMMENT '说明',
    `status`      varchar(10)  NULL DEFAULT NULL COMMENT '状态',
    `type`        varchar(10)  NULL DEFAULT NULL COMMENT '类型',
    `json_str`    longtext     NULL COMMENT 'json字符串',
    `api_url`     varchar(256) NULL DEFAULT NULL COMMENT '请求地址',
    `thumb`       text         NULL COMMENT '缩略图',
    `create_by`   varchar(50)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(50)  NULL DEFAULT NULL COMMENT '修改人',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '修改时间',
    `del_flag`    tinyint(1)   NULL DEFAULT NULL COMMENT '删除标识0-正常,1-已删除',
    `api_method`  varchar(256) NULL DEFAULT NULL COMMENT '请求方法0-get,1-post',
    `api_code`    varchar(256) NULL DEFAULT NULL COMMENT '请求编码',
    `template`    tinyint(1)   NULL DEFAULT NULL COMMENT '是否是模板 0-是,1-不是',
    `view_count`  bigint       NULL DEFAULT 0 COMMENT '浏览次数',
    `css_str`     text         NULL COMMENT 'css增强',
    `js_str`      text         NULL COMMENT 'js增强',
    `tenant_id`   varchar(10)  NULL DEFAULT NULL COMMENT '多租户标识',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `uniq_jmreport_code` (`code` ASC) USING BTREE,
    INDEX `uniq_jmreport_createby` (`create_by` ASC) USING BTREE,
    INDEX `uniq_jmreport_delflag` (`del_flag` ASC) USING BTREE
);

CREATE TABLE `jimu_report_data_source`
(
    `id`            varchar(36)  NOT NULL,
    `name`          varchar(100) NULL DEFAULT NULL COMMENT '数据源名称',
    `report_id`     varchar(100) NULL DEFAULT NULL COMMENT '报表_id',
    `code`          varchar(100) NULL DEFAULT NULL COMMENT '编码',
    `remark`        varchar(200) NULL DEFAULT NULL COMMENT '备注',
    `db_type`       varchar(10)  NULL DEFAULT NULL COMMENT '数据库类型',
    `db_driver`     varchar(100) NULL DEFAULT NULL COMMENT '驱动类',
    `db_url`        varchar(500) NULL DEFAULT NULL COMMENT '数据源地址',
    `db_username`   varchar(100) NULL DEFAULT NULL COMMENT '用户名',
    `db_password`   varchar(100) NULL DEFAULT NULL COMMENT '密码',
    `create_by`     varchar(50)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time`   datetime     NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`     varchar(50)  NULL DEFAULT NULL COMMENT '更新人',
    `update_time`   datetime     NULL DEFAULT NULL COMMENT '更新日期',
    `connect_times` int UNSIGNED NULL DEFAULT 0 COMMENT '连接失败次数',
    `tenant_id`     varchar(10)  NULL DEFAULT NULL COMMENT '多租户标识',
    `type`          varchar(10)  NULL DEFAULT NULL COMMENT '类型(report:报表;drag:仪表盘)',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_jmdatasource_report_id` (`report_id` ASC) USING BTREE,
    INDEX `idx_jmdatasource_code` (`code` ASC) USING BTREE
);

CREATE TABLE `jimu_report_db`
(
    `id`               varchar(36)  NOT NULL COMMENT 'id',
    `jimu_report_id`   varchar(32)  NULL DEFAULT NULL COMMENT '主键字段',
    `create_by`        varchar(50)  NULL DEFAULT NULL COMMENT '创建人登录名称',
    `update_by`        varchar(50)  NULL DEFAULT NULL COMMENT '更新人登录名称',
    `create_time`      datetime     NULL DEFAULT NULL COMMENT '创建日期',
    `update_time`      datetime     NULL DEFAULT NULL COMMENT '更新日期',
    `db_code`          varchar(32)  NULL DEFAULT NULL COMMENT '数据集编码',
    `db_ch_name`       varchar(50)  NULL DEFAULT NULL COMMENT '数据集名字',
    `db_type`          varchar(32)  NULL DEFAULT NULL COMMENT '数据源类型',
    `db_table_name`    varchar(32)  NULL DEFAULT NULL COMMENT '数据库表名',
    `db_dyn_sql`       longtext     NULL COMMENT '动态查询sql',
    `db_key`           varchar(32)  NULL DEFAULT NULL COMMENT '数据源key',
    `tb_db_key`        varchar(32)  NULL DEFAULT NULL COMMENT '填报数据源',
    `tb_db_table_name` varchar(32)  NULL DEFAULT NULL COMMENT '填报数据表',
    `java_type`        varchar(32)  NULL DEFAULT NULL COMMENT 'java类数据集  类型（spring:springkey,class:java类名）',
    `java_value`       varchar(256) NULL DEFAULT NULL COMMENT 'java类数据源  数值（bean key/java类名）',
    `api_url`          varchar(256) NULL DEFAULT NULL COMMENT '请求地址',
    `api_method`       varchar(256) NULL DEFAULT NULL COMMENT '请求方法0-get,1-post',
    `is_list`          varchar(10)  NULL DEFAULT '0' COMMENT '是否是列表0否1是 默认0',
    `is_page`          varchar(10)  NULL DEFAULT NULL COMMENT '是否作为分页,0:不分页，1:分页',
    `db_source`        varchar(256) NULL DEFAULT NULL COMMENT '数据源',
    `db_source_type`   varchar(50)  NULL DEFAULT NULL COMMENT '数据库类型 mysql oracle sqlserver',
    `json_data`        text         NULL COMMENT 'json数据，直接解析json内容',
    `api_convert`      varchar(256) NULL DEFAULT NULL COMMENT 'api转换器',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_jmreportdb_db_key` (`db_key` ASC) USING BTREE,
    INDEX `idx_jimu_report_id` (`jimu_report_id` ASC) USING BTREE,
    INDEX `idx_db_source_id` (`db_source` ASC) USING BTREE
);

CREATE TABLE `jimu_report_db_field`
(
    `id`                varchar(36)  NOT NULL COMMENT 'id',
    `create_by`         varchar(50)  NULL DEFAULT NULL COMMENT '创建人登录名称',
    `create_time`       datetime     NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`         varchar(50)  NULL DEFAULT NULL COMMENT '更新人登录名称',
    `update_time`       datetime     NULL DEFAULT NULL COMMENT '更新日期',
    `jimu_report_db_id` varchar(32)  NULL DEFAULT NULL COMMENT '数据源id',
    `field_name`        varchar(80)  NULL DEFAULT NULL COMMENT '字段名',
    `field_text`        varchar(50)  NULL DEFAULT NULL COMMENT '字段文本',
    `widget_type`       varchar(50)  NULL DEFAULT NULL COMMENT '控件类型',
    `widget_width`      int          NULL DEFAULT NULL COMMENT '控件宽度',
    `order_num`         int          NULL DEFAULT NULL COMMENT '排序',
    `search_flag`       int          NULL DEFAULT 0 COMMENT '查询标识0否1是 默认0',
    `search_mode`       int          NULL DEFAULT NULL COMMENT '查询模式1简单2范围',
    `dict_code`         varchar(256) NULL DEFAULT NULL COMMENT '字典编码支持从表中取数据',
    `search_value`      varchar(100) NULL DEFAULT NULL COMMENT '查询默认值',
    `search_format`     varchar(50)  NULL DEFAULT NULL COMMENT '查询时间格式化表达式',
    `ext_json`          text         NULL COMMENT '参数配置',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_jrdf_jimu_report_db_id` (`jimu_report_db_id` ASC) USING BTREE,
    INDEX `idx_dbfield_order_num` (`order_num` ASC) USING BTREE
);

CREATE TABLE `jimu_report_db_param`
(
    `id`                  varchar(36)   NOT NULL,
    `jimu_report_head_id` varchar(36)   NOT NULL COMMENT '动态报表id',
    `param_name`          varchar(32)   NOT NULL COMMENT '参数字段',
    `param_txt`           varchar(32)   NULL DEFAULT NULL COMMENT '参数文本',
    `param_value`         varchar(1000) NULL DEFAULT NULL COMMENT '参数默认值',
    `order_num`           int           NULL DEFAULT NULL COMMENT '排序',
    `create_by`           varchar(50)   NULL DEFAULT NULL COMMENT '创建人登录名称',
    `create_time`         datetime      NULL DEFAULT NULL COMMENT '创建日期',
    `update_by`           varchar(50)   NULL DEFAULT NULL COMMENT '更新人登录名称',
    `update_time`         datetime      NULL DEFAULT NULL COMMENT '更新日期',
    `search_flag`         int           NULL DEFAULT NULL COMMENT '查询标识0否1是 默认0',
    `widget_type`         varchar(50)   NULL DEFAULT NULL COMMENT '查询控件类型',
    `search_mode`         int           NULL DEFAULT NULL COMMENT '查询模式1简单2范围',
    `dict_code`           varchar(256)  NULL DEFAULT NULL COMMENT '字典',
    `search_format`       varchar(50)   NULL DEFAULT NULL COMMENT '查询时间格式化表达式',
    `ext_json`            text          NULL COMMENT '参数配置',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_jmrheadid` (`jimu_report_head_id` ASC) USING BTREE,
    INDEX `idx_jrdp_jimu_report_head_id` (`jimu_report_head_id` ASC) USING BTREE
);

CREATE TABLE `jimu_report_link`
(
    `id`            varchar(32)   NOT NULL COMMENT '主键id',
    `report_id`     varchar(32)   NULL DEFAULT NULL COMMENT '积木设计器id',
    `parameter`     text          NULL COMMENT '参数',
    `eject_type`    varchar(1)    NULL DEFAULT NULL COMMENT '弹出方式（0 当前页面 1 新窗口）',
    `link_name`     varchar(256)  NULL DEFAULT NULL COMMENT '链接名称',
    `api_method`    varchar(1)    NULL DEFAULT NULL COMMENT '请求方法0-get,1-post',
    `link_type`     varchar(1)    NULL DEFAULT NULL COMMENT '链接方式(0 网络报表 1 网络连接 2 图表联动)',
    `api_url`       varchar(1000) NULL DEFAULT NULL COMMENT '外网api',
    `link_chart_id` varchar(50)   NULL DEFAULT NULL COMMENT '联动图表的id',
    `expression`    varchar(256)  NULL DEFAULT NULL COMMENT '表达式',
    `requirement`   varchar(256)  NULL DEFAULT NULL COMMENT '条件',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `uniq_link_reportid` (`report_id` ASC) USING BTREE
);

CREATE TABLE `jimu_report_map`
(
    `id`           varchar(64)  NOT NULL COMMENT '主键',
    `label`        varchar(125) NULL DEFAULT NULL COMMENT '地图名称',
    `name`         varchar(125) NULL DEFAULT NULL COMMENT '地图编码',
    `data`         longtext     NULL COMMENT '地图数据',
    `create_by`    varchar(32)  NULL DEFAULT NULL COMMENT '创建人',
    `create_time`  datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`    varchar(32)  NULL DEFAULT NULL COMMENT '修改人',
    `update_time`  datetime     NULL DEFAULT NULL COMMENT '修改时间',
    `del_flag`     varchar(1)   NULL DEFAULT NULL COMMENT '0表示未删除,1表示删除',
    `sys_org_code` varchar(64)  NULL DEFAULT NULL COMMENT '所属部门',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `uniq_jmreport_map_name` (`name` ASC) USING BTREE
);

CREATE TABLE `jimu_report_share`
(
    `id`               varchar(32)   NOT NULL COMMENT '主键',
    `report_id`        varchar(32)   NULL DEFAULT NULL COMMENT '在线excel设计器id',
    `preview_url`      varchar(1000) NULL DEFAULT NULL COMMENT '预览地址',
    `preview_lock`     varchar(4)    NULL DEFAULT NULL COMMENT '密码锁',
    `last_update_time` datetime      NULL DEFAULT NULL COMMENT '最后更新时间',
    `term_of_validity` varchar(1)    NULL DEFAULT NULL COMMENT '有效期(0:永久有效，1:1天，2:7天)',
    `status`           varchar(1)    NULL DEFAULT NULL COMMENT '是否过期(0未过期，1已过期)',
    PRIMARY KEY (`id`) USING BTREE
);

CREATE TABLE `job`
(
    `id`                 varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code`        varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `name`               varchar(256) NULL DEFAULT '' COMMENT '任务名称',
    `description`        varchar(256) NULL DEFAULT '' COMMENT '任务描述',
    `type`               varchar(64)  NULL DEFAULT '' COMMENT '任务类型',
    `executor_client_id` varchar(512) NULL DEFAULT '' COMMENT '执行器客户端标识',
    `executor_handler`   varchar(512) NULL DEFAULT '' COMMENT '执行器标识',
    `param`              text         NULL COMMENT '任务参数',
    `cron`               varchar(128) NULL DEFAULT '' COMMENT 'cron表达式',
    `next_time`          datetime     NULL DEFAULT NULL COMMENT '下一次任务执行时间',
    `status`             varchar(64)  NULL DEFAULT '' COMMENT '任务状态',
    `create_time`        datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`          varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`        varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`        datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`          varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`        varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='任务';

CREATE TABLE `label`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `label`       varchar(64) NULL DEFAULT '' COMMENT '标签名称',
    `value`       varchar(64) NULL DEFAULT '' COMMENT '标签值',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='标签';

CREATE TABLE `label_assign`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code` varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `label_id`    varchar(32)  NULL DEFAULT '' COMMENT '标签名称',
    `business_id` varchar(256) NULL DEFAULT '' COMMENT '业务数据编号',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='标签分配';

CREATE TABLE `menu`
(
    `id`              varchar(32)      NOT NULL COMMENT '主键',
    `full_path`       varchar(600)     NOT NULL COMMENT '菜单全路径',
    `parent_id`       varchar(32)      NULL DEFAULT '' COMMENT '父菜单编号',
    `name`            varchar(64)      NULL DEFAULT '' COMMENT '菜单名称',
    `domain_code`     varchar(64)      NULL DEFAULT '' COMMENT '域标识',
    `tenant_code`     varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `app_code`        varchar(64)      NULL DEFAULT '' COMMENT '应用标识',
    `page_path`       varchar(512)     NULL DEFAULT '' COMMENT '菜单图标',
    `component`       varchar(512)     NULL DEFAULT '' COMMENT '文件地址',
    `framework`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否为框架路由',
    `eu_name`         varchar(64)      NULL DEFAULT '' COMMENT '菜单英文名称',
    `permission_code` varchar(64)      NULL DEFAULT '' COMMENT '菜单权限码',
    `icon`            varchar(256)     NULL DEFAULT '' COMMENT '菜单图标',
    `active_icon`     varchar(256)     NULL DEFAULT '' COMMENT '菜单激活时显示的图标',
    `logo`            varchar(256)     NULL DEFAULT '' COMMENT '应用图标',
    `link`            varchar(256)     NULL DEFAULT '' COMMENT '外部网页跳转链接',
    `iframe`          varchar(256)     NULL DEFAULT '' COMMENT '内嵌网页链接',
    `default_opened`  tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否默认展开',
    `permanent`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否常驻标签页',
    `sidebar`         tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否在侧边栏导航中展示',
    `breadcrumb`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否在面包屑导航中展示',
    `active_menu`     varchar(256)     NULL DEFAULT '' COMMENT '侧边栏高亮路由完整地址',
    `cache`           varchar(256)     NULL DEFAULT '' COMMENT '页面缓存，内容为空时表示不缓存，内容为permanent时表示永久缓存，内容为其他时表示跳转到指定页面是缓存当前页面',
    `no_cache`        varchar(256)     NULL DEFAULT '' COMMENT '清除页面缓存',
    `badge`           varchar(256)     NULL DEFAULT '' COMMENT '导航徽标，内容为空时表示无徽标，内容为point时表示点形徽标，内容为其他时表示徽标文本内容，如果是数字字符串，则为0时不显示徽标',
    `copyright`       tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否显示版权信息',
    `padding_bottom`  varchar(256)     NULL DEFAULT '' COMMENT '页面和底部的距离',
    `whitelist`       varchar(256)     NULL DEFAULT '' COMMENT '路由白名单，白名单内的路由不受权限控制',
    `menu_view_type`  varchar(64)      NULL DEFAULT '' COMMENT '菜单可视类型',
    `menu_type`       varchar(64)      NULL DEFAULT '' COMMENT '菜单类型',
    `jump_type`       varchar(64)      NULL DEFAULT '' COMMENT '跳转方式',
    `keep_alive`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '路由缓冲',
    `visible`         tinyint UNSIGNED NULL DEFAULT 0 COMMENT '菜单是否可见',
    `auto_render`     tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否自动渲染页面',
    `remark`          varchar(500)     NULL DEFAULT '' COMMENT '菜单描述',
    `sort_order`      int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`     datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='菜单';

CREATE TABLE `message`
(
    `id`                   varchar(32)   NOT NULL COMMENT '主键',
    `tenant_code`          varchar(64)   NULL DEFAULT '' COMMENT '租户标识',
    `template_id`          varchar(32)   NULL DEFAULT '' COMMENT '消息模板编号',
    `channel`              varchar(32)   NULL DEFAULT '' COMMENT '发送渠道',
    `business_name`        varchar(64)   NULL DEFAULT '' COMMENT '业务类型名称',
    `business_key`         varchar(32)   NULL DEFAULT '' COMMENT '业务类型标识',
    `message_template_key` varchar(32)   NULL DEFAULT '' COMMENT '消息模板标识',
    `message_title`        varchar(256)  NULL DEFAULT '' COMMENT '消息标题',
    `message_content`      varchar(1024) NULL DEFAULT '' COMMENT '消息内容',
    `redirect_to`          text          NULL COMMENT '跳转地址',
    `deliver_status`       varchar(64)   NULL DEFAULT '' COMMENT '发送状态',
    `deliver_time`         datetime      NULL DEFAULT NULL COMMENT '发送时间',
    `receiver_id`          varchar(32)   NULL DEFAULT '' COMMENT '接收者编号',
    `read_status`          varchar(64)   NULL DEFAULT '' COMMENT '消息读取状态',
    `read_time`            datetime      NULL DEFAULT NULL COMMENT '消息读取时间',
    `cause`                varchar(1024) NULL DEFAULT '' COMMENT '发送失败原因',
    `create_time`          datetime      NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`            varchar(32)   NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`          varchar(64)   NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`          datetime      NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`            varchar(32)   NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`          varchar(64)   NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息';

CREATE TABLE `message_channel_definition`
(
    `id`                  varchar(32)      NOT NULL COMMENT '主键',
    `tenant_code`         varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `template_id`         varchar(32)      NULL DEFAULT '' COMMENT '消息模板编号',
    `channel`             varchar(32)      NULL DEFAULT '' COMMENT '消息发送渠道',
    `channel_description` varchar(64)      NULL DEFAULT '' COMMENT '消息发送渠道描述',
    `channel_id`          varchar(32)      NULL DEFAULT '' COMMENT '消息发送渠道标识',
    `channel_template_id` varchar(64)      NULL DEFAULT '' COMMENT '消息发送渠道模板编号',
    `content`             varchar(1024)    NULL DEFAULT '' COMMENT '消息模板内容',
    `voice`               tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否播放声音',
    `redirect_layout`     varchar(32)      NULL DEFAULT '' COMMENT '重定向链接样式',
    `status`              varchar(64)      NULL DEFAULT '' COMMENT '消息发送渠道状态',
    `support_template`    tinyint UNSIGNED NULL DEFAULT 1 COMMENT '消息渠道是否支持模板',
    `show_notification`   tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否展示消息提醒',
    `sort_order`          int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`         datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`           varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`         varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`         datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`           varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`         varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息发送渠道定义';

CREATE TABLE `message_param_definition`
(
    `id`                    varchar(32)      NOT NULL COMMENT '主键',
    `tenant_code`           varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `template_id`           varchar(32)      NULL DEFAULT '' COMMENT '消息模板编号',
    `channel_definition_id` varchar(32)      NULL DEFAULT '' COMMENT '消息发送渠道定义编号',
    `pre_definition`        tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否预定义参数',
    `param_name`            varchar(64)      NULL DEFAULT '' COMMENT '参数名称',
    `param_description`     varchar(64)      NULL DEFAULT '' COMMENT '参数描述',
    `mapped_param_name`     varchar(64)      NULL DEFAULT '' COMMENT '映射参数名称',
    `sort_order`            int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `create_time`           datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`             varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`           varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`           datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`             varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`           varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息参数定义';

CREATE TABLE `message_receiver_definition`
(
    `id`                  varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code`         varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `template_id`         varchar(32)  NULL DEFAULT '' COMMENT '消息模板编号',
    `receiver_type`       varchar(64)  NULL DEFAULT '' COMMENT '接收者类型',
    `receiver_param_name` varchar(64)  NULL DEFAULT '' COMMENT '接收者参数名称',
    `sort_order`          int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time`         datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`           varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`         varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`         datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`           varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`         varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息接收者定义';

CREATE TABLE `message_redirect_definition`
(
    `id`                    varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code`           varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `template_id`           varchar(32)  NULL DEFAULT '' COMMENT '消息模板编号',
    `channel_definition_id` varchar(32)  NULL DEFAULT '' COMMENT '消息发送渠道定义编号',
    `redirect_to`           varchar(256) NULL DEFAULT '' COMMENT '重定向地址',
    `redirect_name`         varchar(64)  NULL DEFAULT '' COMMENT '重定向名称',
    `sort_order`            int UNSIGNED NULL DEFAULT 1 COMMENT '排序值',
    `create_time`           datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`             varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`           varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`           datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`             varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`           varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息重定向定义';

CREATE TABLE `message_template`
(
    `id`                     varchar(32)      NOT NULL COMMENT '主键',
    `tenant_code`            varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `business_name`          varchar(64)      NULL DEFAULT '' COMMENT '业务名称',
    `business_key`           varchar(32)      NULL DEFAULT '' COMMENT '业务名称',
    `message_template_key`   varchar(32)      NULL DEFAULT '' COMMENT '消息标识',
    `message_template_title` varchar(256)     NULL DEFAULT '' COMMENT '消息标题',
    `configured`             tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已配置',
    `configurable`           tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否可配置',
    `sort_order`             int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`               tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`            datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`              varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`            varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`            datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`              varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`            varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='消息模板';

CREATE TABLE `operation_log`
(
    `id`               varchar(32)      NOT NULL COMMENT '主键',
    `domain_code`      varchar(64)      NULL DEFAULT '' COMMENT '域标识',
    `tenant_code`      varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `app_code`         varchar(64)      NULL DEFAULT '' COMMENT '应用标识',
    `module`           varchar(256)     NULL DEFAULT '' COMMENT '业务模块',
    `operation`        varchar(256)     NULL DEFAULT '' COMMENT '操作内容',
    `method`           varchar(256)     NULL DEFAULT '' COMMENT '方法名',
    `request_method`   varchar(64)      NULL DEFAULT '' COMMENT '请求方法',
    `request_url`      varchar(512)     NULL DEFAULT '' COMMENT '请求地址',
    `request_param`    text             NULL COMMENT '请求参数',
    `response_body`    text             NULL COMMENT '响应体',
    `operate_user_id`  varchar(32)      NULL DEFAULT '' COMMENT '操作用户编号',
    `operate_username` varchar(64)      NULL DEFAULT '' COMMENT '操作用户账户',
    `ip`               varchar(64)      NULL DEFAULT '' COMMENT '客户端ip',
    `location`         varchar(256)     NULL DEFAULT '' COMMENT '客户端地区',
    `success`          tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否成功',
    `message`          text             NULL COMMENT '返回信息',
    `operation_time`   datetime         NULL DEFAULT NULL COMMENT '操作时间',
    `create_time`      datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='操作日志';

CREATE TABLE `organization`
(
    `id`                varchar(32)      NOT NULL COMMENT '主键',
    `parent_id`         varchar(32)      NULL DEFAULT NULL COMMENT '父组织编号',
    `tenant_code`       varchar(32)      NULL DEFAULT '' COMMENT '租户标识',
    `name`              varchar(64)      NULL DEFAULT '' COMMENT '组织名称',
    `full_path`         varchar(600)     NULL DEFAULT '' COMMENT '组织全路径',
    `grade`             int UNSIGNED     NOT NULL COMMENT '级别',
    `code`              varchar(64)      NULL DEFAULT '' COMMENT '编码',
    `short_name`        varchar(64)      NULL DEFAULT '' COMMENT '简称',
    `manager_id`        varchar(32)      NULL DEFAULT '' COMMENT '主管领导',
    `establish_date`    datetime         NULL DEFAULT NULL COMMENT '成立日期',
    `status`            varchar(32)      NULL DEFAULT NULL COMMENT '状态',
    `equity_ratio`      varchar(32)      NULL DEFAULT NULL COMMENT '股权比例',
    `province_code`     varchar(64)      NULL DEFAULT '' COMMENT '省份代码',
    `province_name`     varchar(64)      NULL DEFAULT '' COMMENT '省份名称',
    `city_code`         varchar(64)      NULL DEFAULT '' COMMENT '市代码',
    `city_name`         varchar(64)      NULL DEFAULT '' COMMENT '市名称',
    `district_code`     varchar(64)      NULL DEFAULT '' COMMENT '区代码',
    `district_name`     varchar(64)      NULL DEFAULT '' COMMENT '区名称',
    `street_code`       varchar(64)      NULL DEFAULT '' COMMENT '街道代码',
    `street_name`       varchar(64)      NULL DEFAULT '' COMMENT '街道名称',
    `address`           varchar(500)     NULL DEFAULT '' COMMENT '详细住址',
    `website`           varchar(500)     NULL DEFAULT '' COMMENT '网址',
    `postal_code`       varchar(64)      NULL DEFAULT '' COMMENT '邮编',
    `phone`             varchar(64)      NULL DEFAULT '' COMMENT '联系电话',
    `fax`               varchar(256)     NULL DEFAULT '' COMMENT '传真号',
    `introduction_type` varchar(32)      NULL DEFAULT NULL COMMENT '简介类型',
    `introduction`      longtext         NULL COMMENT '简介内容',
    `default_flag`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否默认组织',
    `sort_order`        int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`          tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`       datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '组织';

CREATE TABLE `oss_file`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `domain_code` varchar(64)  NULL DEFAULT '' COMMENT '域标识',
    `tenant_code` varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `app_code`    varchar(64)  NULL DEFAULT '' COMMENT '应用标识',
    `origin_name` varchar(256) NOT NULL COMMENT '原始文件名称',
    `file_size`   varchar(64)  NULL DEFAULT '' COMMENT '文件大小',
    `suffix`      varchar(32)  NULL DEFAULT '' COMMENT '文件后缀',
    `url`         varchar(256) NULL DEFAULT '' COMMENT '文件访问相对路径',
    `dir_name`    varchar(64)  NULL DEFAULT '' COMMENT '文件服务器目录',
    `path`        varchar(256) NULL DEFAULT '' COMMENT '文件服务器路径',
    `md5`         varchar(256) NULL DEFAULT '' COMMENT '文件md5',
    `use_count`   varchar(64)  NULL DEFAULT '0' COMMENT '文件使用次数',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='文件';

CREATE TABLE `pay_record`
(
    `id`                varchar(32)  NOT NULL COMMENT '主键',
    `request_id`        varchar(64)  NULL DEFAULT '' COMMENT '请求编号',
    `amount`            double       NOT NULL COMMENT '支付金额',
    `supplier_id`       varchar(32)  NULL DEFAULT '' COMMENT '支付平台编号',
    `supplier_type`     varchar(64)  NULL DEFAULT '' COMMENT '支付平台类型',
    `pay_type`          varchar(64)  NULL DEFAULT '' COMMENT '支付类型',
    `supplier_trade_id` varchar(64)  NULL DEFAULT '' COMMENT '支付平台交易编号',
    `channel_trade_id`  varchar(64)  NULL DEFAULT '' COMMENT '支付渠道交易编号',
    `trade_time`        datetime     NULL DEFAULT NULL COMMENT '交易时间',
    `cause`             varchar(256) NULL DEFAULT '' COMMENT '交易详情',
    `status`            varchar(64)  NULL DEFAULT '' COMMENT '交易状态',
    `props`             varchar(512) NULL DEFAULT '' COMMENT '交易参数',
    `create_time`       datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`         varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`       varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`       datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`         varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`       varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='支付记录';

CREATE TABLE `post`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `name`        varchar(64)      NULL DEFAULT '' COMMENT '岗位名称',
    `tenant_code` varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `remark`      varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `sort_order`  int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`    tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='岗位';

CREATE TABLE `role`
(
    `id`              varchar(32)      NOT NULL COMMENT '主键',
    `name`            varchar(64)      NOT NULL COMMENT '角色名称',
    `domain_code`     varchar(64)      NULL DEFAULT '' COMMENT '域标识',
    `tenant_code`     varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `app_code`        varchar(64)      NULL DEFAULT '' COMMENT '应用标识',
    `data_scope_type` varchar(64)      NULL DEFAULT '' COMMENT '数据权限类型',
    `remark`          varchar(500)     NULL DEFAULT '' COMMENT '备注',
    `sort_order`      int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`        tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`     datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='角色';

CREATE TABLE `role_data_scope`
(
    `id`              varchar(32) NOT NULL COMMENT '编号',
    `role_id`         varchar(32) NULL DEFAULT '' COMMENT '角色编号',
    `tenant_code`     varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `data_scope_code` varchar(32) NULL DEFAULT '' COMMENT '权限码',
    `create_time`     datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='角色数据权限';

CREATE TABLE `role_menu`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `role_id`     varchar(32) NULL DEFAULT '' COMMENT '角色编号',
    `domain_code` varchar(64) NULL DEFAULT '' COMMENT '域标识',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `app_code`    varchar(64) NULL DEFAULT '' COMMENT '应用标识',
    `menu_id`     varchar(32) NULL DEFAULT '' COMMENT '菜单编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='角色菜单';

CREATE TABLE `schedule`
(
    `id`                  varchar(32)  NOT NULL COMMENT '主键',
    `business_id`         varchar(128) NULL DEFAULT '' COMMENT '业务编号',
    `title`               varchar(128) NULL DEFAULT '' COMMENT '标题',
    `type`                varchar(64)  NULL DEFAULT '' COMMENT '类型',
    `status`              varchar(64)  NULL DEFAULT '' COMMENT '状态',
    `remark`              varchar(500) NULL DEFAULT '' COMMENT '说明',
    `redirect_url`        varchar(512) NULL DEFAULT '' COMMENT '跳转地址',
    `all_day`             tinyint      NULL DEFAULT 0 COMMENT '是否全天',
    `estimate_end_time`   datetime     NULL DEFAULT NULL COMMENT '计划结束时间',
    `estimate_start_time` datetime     NULL DEFAULT NULL COMMENT '计划开始时间',
    `create_by`           char(32)     NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`         varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `create_time`         datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`           char(32)     NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`         varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    `update_time`         datetime     NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='日程';

CREATE TABLE `sms`
(
    `id`               varchar(32)      NOT NULL COMMENT '主键',
    `code`             varchar(64)      NULL DEFAULT '' COMMENT '短信平台标识',
    `name`             varchar(64)      NULL DEFAULT '' COMMENT '短信平台名称',
    `type`             varchar(64)      NULL DEFAULT '' COMMENT '短信平台类型',
    `app_key`          varchar(256)     NULL DEFAULT '' COMMENT '短信平台AppKey',
    `app_secret`       varchar(256)     NULL DEFAULT '' COMMENT '短信平台AppSecret',
    `extend_code`      varchar(256)     NULL DEFAULT '' COMMENT '短信平台ExtendCode',
    `endpoint`         varchar(256)     NULL DEFAULT '' COMMENT '短信平台地址',
    `signature`        varchar(256)     NULL DEFAULT '' COMMENT '短信签名',
    `support_template` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否支持模板',
    `create_time`      datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`        varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`      varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`      datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`        varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`      varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='短信平台';

CREATE TABLE `sql_api_definition`
(
    `file_path`    varchar(512) NOT NULL,
    `file_content` mediumtext   NULL,
    PRIMARY KEY (`file_path`) USING BTREE
);

CREATE TABLE `sql_api_definition_backup`
(
    `id`          varchar(32) NOT NULL COMMENT '原对象id',
    `create_date` bigint      NOT NULL COMMENT '备份时间',
    `tag`         varchar(32) NULL DEFAULT NULL COMMENT '标签',
    `type`        varchar(32) NULL DEFAULT NULL COMMENT '类型',
    `name`        varchar(64) NULL DEFAULT NULL COMMENT '原名称',
    `content`     blob        NULL COMMENT '备份内容',
    `create_by`   varchar(64) NULL DEFAULT NULL COMMENT '操作人',
    PRIMARY KEY (`id`, `create_date`) USING BTREE
);

CREATE TABLE `sys_user`
(
    `id`                   varchar(32)      NOT NULL COMMENT '主键',
    `username`             varchar(64)      NULL DEFAULT '' COMMENT '账户',
    `password`             varchar(128)     NULL DEFAULT '' COMMENT '账户密码',
    `password_modify_time` datetime         NULL DEFAULT NULL COMMENT '密码修改时间',
    `nickname`             varchar(64)      NULL DEFAULT '' COMMENT '昵称',
    `name`                 varchar(64)      NULL DEFAULT '' COMMENT '名称',
    `avatar`               varchar(256)     NULL DEFAULT '' COMMENT '头像',
    `birth_day`            date             NULL DEFAULT NULL COMMENT '出生日期',
    `phone`                varchar(64)      NULL DEFAULT '' COMMENT '联系电话',
    `email`                varchar(64)      NULL DEFAULT '' COMMENT '邮件地址',
    `identity_no`          varchar(64)      NULL DEFAULT '' COMMENT '身份证号',
    `sex`                  varchar(64)      NULL DEFAULT 'MALE' COMMENT '性别',
    `locked`               tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否被锁定',
    `lock_type`            varchar(64)      NULL DEFAULT 'NO_LOCK' COMMENT '锁定类型',
    `admin_user`           tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否管理员',
    `platform_user`        tinyint UNSIGNED NULL DEFAULT 1 COMMENT '是否平台用户',
    `del_flag`             tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`          datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`            varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`          varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`          datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`            varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`          varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户';

CREATE TABLE `system_log`
(
    `id`              varchar(32)  NOT NULL COMMENT '主键',
    `tenant_code`     varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `trace_id`        varchar(32)  NULL DEFAULT '' COMMENT '追踪编号',
    `package_name`    varchar(256) NULL DEFAULT '' COMMENT '包名',
    `method`          varchar(256) NULL DEFAULT '' COMMENT '方法名',
    `message`         text         NULL COMMENT '返回信息',
    `exception_stack` text         NULL COMMENT '异常堆栈',
    `execute_time`    datetime     NULL DEFAULT NULL COMMENT '执行时间',
    `create_time`     datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`       varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`     varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`     datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`       varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`     varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='系统日志';

CREATE TABLE `tenant`
(
    `id`            varchar(32)      NOT NULL COMMENT '编号/租户编号',
    `name`          varchar(64)      NULL DEFAULT '' COMMENT '租户名称',
    `code`          varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `type`          varchar(64)      NULL DEFAULT '' COMMENT '租户类型',
    `tenant_domain` varchar(256)     NULL DEFAULT '' COMMENT '租户域名',
    `logo`          varchar(256)     NULL DEFAULT '' COMMENT '租户logo',
    `icon`          varchar(256)     NULL DEFAULT '' COMMENT '租户icon',
    `element`       varchar(64)      NULL DEFAULT 'icon_name' COMMENT '元素',
    `arrange`       varchar(64)      NULL DEFAULT 'left_icon_right_name' COMMENT '排列方式',
    `alignment`     varchar(64)      NULL DEFAULT 'left' COMMENT '水平对齐',
    `icon_open`     varchar(512)     NULL DEFAULT '' COMMENT '图标（左侧菜单展开）',
    `icon_close`    varchar(512)     NULL DEFAULT '' COMMENT '图标（左侧菜单收起）',
    `menu_name`     varchar(64)      NULL DEFAULT '' COMMENT '名称（默认为租户名称，可修改，建议名称在8个字内）',
    `favicon`       varchar(512)     NULL DEFAULT '' COMMENT '浏览器页签图标',
    `favicon_name`  varchar(64)      NULL DEFAULT '' COMMENT '浏览器页签图标名称',
    `start_date`    date             NULL DEFAULT NULL COMMENT '租户有效期开始日期',
    `end_date`      date             NULL DEFAULT NULL COMMENT '租户有效期结束日期',
    `status`        varchar(64)      NULL DEFAULT '' COMMENT '租户状态',
    `remark`        varchar(500)     NULL DEFAULT '' COMMENT '租户描述',
    `sort_order`    int UNSIGNED     NULL DEFAULT 1 COMMENT '排序值',
    `del_flag`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否已删除',
    `create_time`   datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='租户';

CREATE TABLE `todo_task`
(
    `id`            char(32)      NOT NULL COMMENT '主键',
    `domain_code`   varchar(64)   NULL DEFAULT '' COMMENT '域标识',
    `tenant_code`   varchar(64)   NULL DEFAULT '' COMMENT '租户标识',
    `app_code`      varchar(64)   NULL DEFAULT '' COMMENT '应用标识',
    `business_name` varchar(128)  NULL DEFAULT '' COMMENT '业务名称',
    `business_id`   varchar(128)  NULL DEFAULT '' COMMENT '业务数据编号',
    `summary`       varchar(1024) NULL DEFAULT '' COMMENT '摘要',
    `user_id`       char(32)      NULL DEFAULT '' COMMENT '用户编号',
    `user_name`     varchar(64)   NULL DEFAULT '' COMMENT '用户名',
    `status`        varchar(64)   NULL DEFAULT '' COMMENT '业务状态',
    `redirect`      varchar(256)  NULL DEFAULT '' COMMENT '跳转链接',
    `create_time`   datetime      NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     char(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64)   NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime      NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     char(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64)   NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='待办任务表';

CREATE TABLE `user_category`
(
    `id`            varchar(32) NOT NULL COMMENT '主键',
    `user_id`       varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `category_code` varchar(64) NULL DEFAULT '' COMMENT '身份标识',
    `create_time`   datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`     varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`   varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`   datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`     varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`   varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户身份';

CREATE TABLE `user_config`
(
    `id`          varchar(32)      NOT NULL COMMENT '主键',
    `tenant_code` varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `user_id`     varchar(32)      NULL DEFAULT '' COMMENT '用户id',
    `type`        varchar(32)      NULL DEFAULT '' COMMENT '配置类型',
    `value`       longtext         NULL COMMENT '配置信息',
    `global`      tinyint UNSIGNED NULL DEFAULT 0 COMMENT '是否全局配置',
    `create_time` datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户配置';

CREATE TABLE `user_dept`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `user_id`     varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `dept_id`     varchar(32) NULL DEFAULT '' COMMENT '部门编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户部门';

CREATE TABLE `user_post`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `user_id`     varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `post_id`     varchar(32) NULL DEFAULT '' COMMENT '岗位编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户岗位';

CREATE TABLE `user_role`
(
    `id`          varchar(32) NOT NULL COMMENT '主键',
    `domain_code` varchar(64) NULL DEFAULT '' COMMENT '域标识',
    `tenant_code` varchar(64) NULL DEFAULT '' COMMENT '租户标识',
    `app_code`    varchar(64) NULL DEFAULT '' COMMENT '应用标识',
    `user_id`     varchar(32) NULL DEFAULT '' COMMENT '用户编号',
    `role_id`     varchar(32) NULL DEFAULT '' COMMENT '角色编号',
    `create_time` datetime    NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32) NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64) NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime    NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32) NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64) NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户角色';

CREATE TABLE `user_social`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `user_id`     varchar(32)  NULL DEFAULT NULL COMMENT '用户编号',
    `social_id`   varchar(256) NULL DEFAULT '' COMMENT '第三方平台编号',
    `social_type` varchar(64)  NULL DEFAULT '' COMMENT '第三方平台类型',
    `social_no`   varchar(128) NULL DEFAULT '' COMMENT '第三方平台身份标识',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户第三方平台身份标识';

CREATE TABLE `user_tenant`
(
    `id`           varchar(32)      NOT NULL COMMENT '主键',
    `user_id`      varchar(32)      NULL DEFAULT '' COMMENT '用户编号',
    `tenant_code`  varchar(64)      NULL DEFAULT '' COMMENT '租户标识',
    `recent_login` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '是否最近登录',
    `create_time`  datetime         NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`    varchar(32)      NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`  varchar(64)      NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`  datetime         NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`    varchar(32)      NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`  varchar(64)      NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='用户租户';

CREATE TABLE `variable`
(
    `id`             varchar(32)  NOT NULL COMMENT '编号/参数编号',
    `name`           varchar(64)  NULL DEFAULT '' COMMENT '参数名称',
    `tenant_code`    varchar(64)  NULL DEFAULT '' COMMENT '租户标识',
    `variable_key`   varchar(128) NULL DEFAULT '' COMMENT '参数键名',
    `variable_value` varchar(128) NULL DEFAULT '' COMMENT '参数值',
    `type`           varchar(64)  NULL DEFAULT '' COMMENT '参数类型',
    `create_time`    datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`      varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name`    varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time`    datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`      varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name`    varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='系统参数';
CREATE TABLE `wechat_mini`
(
    `id`          varchar(32)  NOT NULL COMMENT '主键',
    `code`        varchar(64)  NULL DEFAULT '' COMMENT '微信小程序标识',
    `name`        varchar(64)  NULL DEFAULT '' COMMENT '微信小程序名称',
    `app_id`      varchar(256) NULL DEFAULT '' COMMENT '微信小程序appid',
    `app_secret`  varchar(256) NULL DEFAULT '' COMMENT '微信小程序appsecret',
    `create_time` datetime     NULL DEFAULT NULL COMMENT '创建时间',
    `create_by`   varchar(32)  NULL DEFAULT '' COMMENT '创建人编号',
    `create_name` varchar(64)  NULL DEFAULT '' COMMENT '创建人姓名',
    `update_time` datetime     NULL DEFAULT NULL COMMENT '更新时间',
    `update_by`   varchar(32)  NULL DEFAULT '' COMMENT '更新人编号',
    `update_name` varchar(64)  NULL DEFAULT '' COMMENT '更新人姓名',
    PRIMARY KEY (`id`) USING BTREE
) COMMENT ='微信小程序';
SET FOREIGN_KEY_CHECKS = 1;

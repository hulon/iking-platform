package com.ikingtech.platform.service.push.sms;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.sms.api.SmsApi;
import com.ikingtech.framework.sdk.sms.model.SmsSendMessageParamDTO;
import com.ikingtech.platform.service.push.common.Deliver;
import com.ikingtech.platform.service.push.common.DeliverResult;
import com.ikingtech.platform.service.push.common.DeliverTypeEnum;
import com.ikingtech.platform.service.push.common.PushRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 短信发送工具
 * </p>
 *
 * @author DPJ
 * @since 2023-05-24
 */
@Component
@RequiredArgsConstructor
public class SmsDeliver implements Deliver {

    private final SmsApi smsApi;

    @Override
    public DeliverResult send(PushRequest pushRequest, String receiverId) {
        SmsSendMessageParamDTO param = new SmsSendMessageParamDTO();
        param.setSmsId(pushRequest.getAppConfigId());
        param.setTemplateId(pushRequest.getTemplateId());
        param.setPhone(receiverId);
        param.setContent(pushRequest.getBody().getText());
        param.setParams(pushRequest.getTemplateParams());
        R<Object> result = this.smsApi.send(param);
        return new DeliverResult(result.isSuccess(), result.getMsg());
    }

    @Override
    public DeliverTypeEnum type() {
        return DeliverTypeEnum.SMS;
    }
}

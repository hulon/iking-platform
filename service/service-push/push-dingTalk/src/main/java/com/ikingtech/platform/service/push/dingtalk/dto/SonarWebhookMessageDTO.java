package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@NoArgsConstructor
@Data
public class SonarWebhookMessageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -3749771855127985542L;

    private String serverUrl;

    private String taskId;

    private String status;

    private String analysedAt;

    private String revision;

    private String changedAt;

    private ProjectDTO project;

    private BranchDTO branch;

    @Data
    public static class ProjectDTO implements Serializable {

        @Serial
        private static final long serialVersionUID = -6989534605625079547L;

        private String key;

        private String name;

        private String url;
    }

    @Data
    public static class BranchDTO implements Serializable {

        @Serial
        private static final long serialVersionUID = -4889058079313316273L;

        private String name;

        private String type;

        private Boolean isMain;

        private String url;
    }
}

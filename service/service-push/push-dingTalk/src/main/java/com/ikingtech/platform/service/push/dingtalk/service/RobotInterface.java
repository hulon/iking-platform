package com.ikingtech.platform.service.push.dingtalk.service;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.platform.service.push.dingtalk.dto.DingTalkSendMessageDTO;


/**
 * created on 2023-05-25 16:42
 *
 * @author wub
 */

public interface RobotInterface {

    DingTalkSendMessageDTO analysis(String payLoad) throws FrameworkException;

    String getType();
}

package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * created on 2023-05-24 16:57
 * <p>
 * 消息卡片按钮
 *
 * @author wub
 */

@Data
public class ButtonDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2788706625893883969L;

    /**
     * 按钮标题
     */
    private String title;

    /**
     * 按钮跳转的地址
     */
    private String url;
}

package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * DingTalk机器人消息实体
 *
 * @author tie yan
 */
@Data
public class DingTalkRobotMessageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1758526312533688645L;

    private String msgtype;

    private DingTalkRobotMarkdownMessageDTO markdown;

    private DingTalkRobotActionCardMessageDTO actionCard;
}

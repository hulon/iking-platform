package com.ikingtech.platform.service.push.dingtalk.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * created on 2023-05-24 16:47
 * <p>
 * 钉钉机器人消息内容
 *
 * @author wub
 */

@Data
public class DingTalkSendMessageDTO implements Serializable {


    @Serial
    private static final long serialVersionUID = -4368172793083729363L;

    /**
     * 机器人webhook
     */
    private String webhook;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 按钮排列方向,默认横向
     * 0：竖直排列
     * 1：横向排列
     */
    private String btnOrientation;
    /**
     * 按钮集合,为空则不展示
     */
    private List<ButtonDTO> buttonDTOList;
    /**
     * 加签密钥,传参则使用加签方式,不传则不使用
     */
    private String secret;

    public DingTalkSendMessageDTO(String webhook, String title, String content, String btnOrientation, List<ButtonDTO> buttonDTOList) {
        this.webhook = webhook;
        this.title = title;
        this.content = content;
        this.btnOrientation = btnOrientation;
        this.buttonDTOList = buttonDTOList;
    }

}

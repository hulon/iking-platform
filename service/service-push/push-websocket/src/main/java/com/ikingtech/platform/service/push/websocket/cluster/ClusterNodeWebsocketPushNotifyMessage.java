package com.ikingtech.platform.service.push.websocket.cluster;

import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeMessage;
import com.ikingtech.platform.service.push.common.MessageBody;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ClusterNodeWebsocketPushNotifyMessage extends ClusterNodeMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = -371496708566850956L;

    private MessageBody messageBody;

    private String receiverId;
}

package com.ikingtech.platform.service.push.websocket.configuration;

import com.ikingtech.framework.sdk.cluster.client.ClusterClient;
import com.ikingtech.platform.service.push.common.Deliver;
import com.ikingtech.platform.service.push.websocket.WebsocketClusterDeliver;
import com.ikingtech.platform.service.push.websocket.WebsocketSingletonDeliver;
import com.ikingtech.platform.service.push.websocket.handler.WebsocketHandler;
import com.ikingtech.platform.service.push.websocket.handler.WebsocketMessageAnnouncer;
import com.ikingtech.platform.service.push.websocket.interceptor.WebsocketInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Configuration
@EnableWebSocket
@RequiredArgsConstructor
public class WebsocketConfiguration {

    @Bean
    public WebsocketHandler websocketHandler(List<WebsocketMessageAnnouncer> announcers) {
        return new WebsocketHandler(announcers);
    }

    @Bean
    public WebsocketInterceptor websocketInterceptor() {
        return new WebsocketInterceptor();
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "mode", havingValue = "singleton", matchIfMissing = true)
    public Deliver websocketSingletonDeliver() {
        return new WebsocketSingletonDeliver();
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "mode", havingValue = "cluster")
    public Deliver websocketClusterDeliver(ClusterClient clusterClient) {
        return new WebsocketClusterDeliver(clusterClient);
    }
}

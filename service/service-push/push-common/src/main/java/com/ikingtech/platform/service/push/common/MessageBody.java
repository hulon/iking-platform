package com.ikingtech.platform.service.push.common;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageBody implements Serializable {

    @Serial
    private static final long serialVersionUID = 1092372830468946137L;

    /**
     * 业务类型
     */
    private String businessKey;

    /**
     * 是否展示消息提醒
     */
    private Boolean showNotification;

    /**
     * 消息点击后的跳转链接
     */
    private List<MessageRedirect> redirectTo;

    /**
     * 消息文本
     */
    private String text;
}

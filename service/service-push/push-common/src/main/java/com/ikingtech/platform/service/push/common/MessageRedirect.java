package com.ikingtech.platform.service.push.common;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageRedirect implements Serializable {

    @Serial
    private static final long serialVersionUID = -6792448954037636376L;

    /**
     * 消息点击后的跳转链接
     */
    private String redirectLink;

    /**
     * 跳转链接名称
     */
    private String redirectName;
}

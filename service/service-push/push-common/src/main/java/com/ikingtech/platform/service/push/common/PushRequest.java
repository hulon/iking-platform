package com.ikingtech.platform.service.push.common;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class PushRequest {

    /**
     * 应用配置编号
     * 应用源为平台时，该字段指定平台内配置的编号
     * 应用源为配置文件时，该字段指定配置文件内的appId
     */
    private String appConfigId;

    /**
     * 消息模板编号
     * 应用源为配置文件时，该字段指定配置文件内的templateId，如未指定则默认使用应用下的第一个消息模板
     * 应用源为配置平台时时，该字段由调用方指定，必填
     */
    private String templateId;

    /**
     * 消息体
     */
    private MessageBody body;

    /**
     * 消息内容
     * 若消息接收方以模板形式接受消息，则此字段指定消息接收方的模板参数，与templateId字段配合使用
     */
    private Map<String, Object> templateParams;
}

package com.ikingtech.platform.service.authorization;

import com.ikingtech.framework.sdk.enums.authorization.ClientTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class Authorization implements Serializable {

    @Serial
    private static final long serialVersionUID = -7894695748951554566L;

    /**
     * 客户端ID
     */
    private String clientId;

    /**
     * 客户端类型
     */
    private ClientTypeEnum clientType;

    /**
     * 资源列表
     */
    private List<Resource> resources;

}

package com.ikingtech.platform;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tie yan
 */
@MapperScan(basePackages = "**.**.mapper")
@SpringBootApplication
public class Lite {

    public static void main(String[] args) {
        SpringApplication.run(Lite.class, args);
    }
}
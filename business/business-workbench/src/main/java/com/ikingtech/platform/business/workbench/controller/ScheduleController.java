package com.ikingtech.platform.business.workbench.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.workbench.api.ScheduleApi;
import com.ikingtech.framework.sdk.workbench.model.ScheduleDTO;
import com.ikingtech.framework.sdk.workbench.model.ScheduleQueryParamDTO;
import com.ikingtech.platform.business.workbench.entity.ScheduleDO;
import com.ikingtech.platform.business.workbench.service.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
@ApiController(value = "/schedule", name = "工作台-日程管理", description = "工作台-日程管理")
public class ScheduleController implements ScheduleApi {

    private final ScheduleRepository repo;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<String> add(ScheduleDTO schedule) {
        ScheduleDO entity = Tools.Bean.copy(schedule, ScheduleDO.class);
        entity.setId(Tools.Id.uuid());
        this.repo.save(entity);
        return R.ok(entity.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Object> delete(String id) {
        this.repo.removeById(id);
        return R.ok();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Boolean> update(ScheduleDTO schedule) {
        if (!this.repo.exists(Wrappers.<ScheduleDO>lambdaQuery().eq(ScheduleDO::getId, schedule.getId()))) {
            throw new FrameworkException("对象不存在");
        }
        ScheduleDO entity = Tools.Bean.copy(schedule, ScheduleDO.class);
        this.repo.updateById(entity);
        return R.ok();
    }

    @Override
    public R<ScheduleDTO> detail(String id) {
        ScheduleDO entity = this.repo.getById(id);
        if (null == entity) {
            throw new FrameworkException("对象不存在");
        }
        ScheduleDTO schedule = Tools.Bean.copy(entity, ScheduleDTO.class);
        return R.ok(schedule);
    }

    @Override
    public R<List<ScheduleDTO>> page(ScheduleQueryParamDTO queryParam) {
        return R.ok(PageResult.build(this.repo.page(new Page<>(queryParam.getPage(), queryParam.getRows()), ScheduleRepository.createWrapper(queryParam))).convert(entity -> Tools.Bean.copy(entity, ScheduleDTO.class)));
    }

    @Override
    public R<List<ScheduleDTO>> all() {
        return R.ok(Tools.Coll.convertList(this.repo.list(), entity -> Tools.Bean.copy(entity, ScheduleDTO.class)));
    }

    @Override
    public R<Map<String, Integer>> countByDate(ScheduleQueryParamDTO queryParam) {
        List<ScheduleDO> entities = this.repo.list(Wrappers.<ScheduleDO>lambdaQuery()
                .le(null != queryParam.getStartTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getStartTime()))
                .ge(null != queryParam.getEndTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getEndTime()))
                .orderByDesc(ScheduleDO::getCreateTime));
        Map<String, Integer> result = new HashMap<>();
        entities.forEach(entity -> {
            String dateStr = Tools.DateTime.Formatter.simpleDate(entity.getEstimateStartTime().toLocalDate());
            Integer count = result.computeIfAbsent(dateStr, k -> 0);
            result.put(dateStr, count + 1);
        });
        return R.ok(result);
    }
}
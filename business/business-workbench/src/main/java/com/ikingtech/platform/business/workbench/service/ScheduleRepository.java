package com.ikingtech.platform.business.workbench.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.workbench.model.ScheduleQueryParamDTO;
import com.ikingtech.platform.business.workbench.entity.ScheduleDO;
import com.ikingtech.platform.business.workbench.mapper.ScheduleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class ScheduleRepository extends ServiceImpl<ScheduleMapper, ScheduleDO> {

    public static LambdaQueryWrapper<ScheduleDO> createWrapper(ScheduleQueryParamDTO queryParam) {
        return Wrappers.<ScheduleDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(queryParam.getIds()), ScheduleDO::getId, queryParam.getIds())
                .eq(Tools.Str.isNotBlank(queryParam.getStatus()), ScheduleDO::getStatus, queryParam.getStatus())
                .like(Tools.Str.isNotBlank(queryParam.getTitle()), ScheduleDO::getTitle, queryParam.getTitle())
                .eq(Tools.Str.isNotBlank(queryParam.getType()), ScheduleDO::getType, queryParam.getType())
                .le(null != queryParam.getEndTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getEndTime()))
                .like(Tools.Str.isNotBlank(queryParam.getRemark()), ScheduleDO::getRemark, queryParam.getRemark())
                .ge(null != queryParam.getStartTime(), ScheduleDO::getEstimateStartTime, Tools.DateTime.Formatter.simple(queryParam.getStartTime()))
                .orderByDesc(ScheduleDO::getCreateTime);
    }
}

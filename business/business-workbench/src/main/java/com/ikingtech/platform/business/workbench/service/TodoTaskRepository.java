package com.ikingtech.platform.business.workbench.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.workbench.entity.TodoTaskDO;
import com.ikingtech.platform.business.workbench.mapper.TodoTaskMapper;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
public class TodoTaskRepository extends ServiceImpl<TodoTaskMapper, TodoTaskDO> {
}

package com.ikingtech.platform.business.workbench.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("todo_task")
public class TodoTaskDO extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 5617444476397729445L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("app_code")
    private String appCode;

    @TableField("business_name")
    private String businessName;

    @TableField("business_id")
    private String businessId;

    @TableField("summary")
    private String summary;

    @TableField("user_id")
    private String userId;

    @TableField("user_name")
    private String userName;

    @TableField("status")
    private String status;

    @TableField("redirect")
    private String redirect;
}

package com.ikingtech.platform.business.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("message_receiver_definition")
public class MessageReceiverDefinitionDO extends SortEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6874964096562027644L;

    @TableField("tenant_code")
    private String tenantCode;

    @TableField("template_id")
    private String templateId;

    @TableField("receiver_type")
    private String receiverType;

    @TableField("receiver_param_name")
    private String receiverParamName;
}

package com.ikingtech.platform.business.message.service.router;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@AllArgsConstructor
public class RouteResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -966289793721489055L;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 重定向地址
     */
    private List<String> redirectTo;

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 失败原因
     */
    private String cause;
}

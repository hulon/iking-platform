package com.ikingtech.platform.business.message.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageChannelDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageChannelDefinitionMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public class MessageChannelDefinitionService extends ServiceImpl<MessageChannelDefinitionMapper, MessageChannelDefinitionDO> {

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().eq(MessageChannelDefinitionDO::getId, id));
    }

    public void removeByTemplateId(String templateId) {
        this.remove(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().eq(MessageChannelDefinitionDO::getTemplateId, templateId));
    }

    public List<MessageChannelDefinitionDO> listByTemplateId(String templateId) {
        return this.list(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().eq(MessageChannelDefinitionDO::getTemplateId, templateId));
    }

    public List<MessageChannelDefinitionDO> listByTemplateIds(List<String> templateIds) {
        return this.list(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().in(MessageChannelDefinitionDO::getTemplateId, templateIds));
    }

    public List<String> listTemplateIdByChannelType(String channel) {
        return this.listObjs(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().select(MessageChannelDefinitionDO::getTemplateId).eq(MessageChannelDefinitionDO::getChannel, channel), String.class::cast);
    }

    public void removeByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().in(MessageChannelDefinitionDO::getTemplateId, templateIds));
    }

    public boolean channelExist(String templateId, String channel) {
        return this.baseMapper.exists(Wrappers.<MessageChannelDefinitionDO>lambdaQuery().eq(MessageChannelDefinitionDO::getTemplateId, templateId).eq(MessageChannelDefinitionDO::getChannel, channel));
    }
}

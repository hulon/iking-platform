package com.ikingtech.platform.business.message.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.platform.business.message.entity.MessageReceiverDefinitionDO;
import com.ikingtech.platform.business.message.mapper.MessageReceiverDefinitionMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public class MessageReceiverDefinitionService extends ServiceImpl<MessageReceiverDefinitionMapper, MessageReceiverDefinitionDO> {

    public void removeByTemplateIds(List<String> templateIds) {
        this.remove(Wrappers.<MessageReceiverDefinitionDO>lambdaQuery().in(MessageReceiverDefinitionDO::getTemplateId, templateIds));
    }

    public List<MessageReceiverDefinitionDO> listByTemplateId(String templateId) {
        return this.list(Wrappers.<MessageReceiverDefinitionDO>lambdaQuery().eq(MessageReceiverDefinitionDO::getTemplateId, templateId));
    }
}

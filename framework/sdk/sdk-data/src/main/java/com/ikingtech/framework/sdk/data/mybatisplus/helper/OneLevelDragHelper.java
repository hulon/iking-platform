package com.ikingtech.framework.sdk.data.mybatisplus.helper;

import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import com.ikingtech.framework.sdk.utils.Tools;

import java.util.List;

/**
 * @author tie yan
 */
public class OneLevelDragHelper<T extends SortEntity> extends AbstractDragHelper<T>{

    @Override
    public List<T> drag() {
        T currentNode = this.currentNode.get();
        T targetNode = this.targetNode.get();
        boolean isUp = currentNode.getSortOrder() > targetNode.getSortOrder();
        int startNodeOrder = isUp ? targetNode.getSortOrder() : currentNode.getSortOrder();
        int endNodeOrder = isUp ? currentNode.getSortOrder() : targetNode.getSortOrder();
        int currentNodeOrder = isUp ? targetNode.getSortOrder() + 1 : targetNode.getSortOrder();
        if (Boolean.TRUE.equals(this.beforeTarget)) {
            endNodeOrder--;
            currentNodeOrder--;
        } else {
            startNodeOrder++;
        }
        List<T> nodeBetweenCurrentAndTarget = this.nodeBetweenCurrentAndTarget.apply(startNodeOrder, endNodeOrder);
        currentNode.setSortOrder(currentNodeOrder);
        List<T> sortedEntities = Tools.Coll.traverse(nodeBetweenCurrentAndTarget, entity -> {
            entity.setSortOrder(isUp ?
                    entity.getSortOrder() + 1 :
                    entity.getSortOrder() - 1);
            return entity;
        });
        sortedEntities.add(currentNode);
        return sortedEntities;
    }
}

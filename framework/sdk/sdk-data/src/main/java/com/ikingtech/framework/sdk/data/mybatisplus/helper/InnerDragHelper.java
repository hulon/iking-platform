package com.ikingtech.framework.sdk.data.mybatisplus.helper;

import com.ikingtech.framework.sdk.data.mybatisplus.entity.SortEntity;
import com.ikingtech.framework.sdk.utils.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tie yan
 */
public class InnerDragHelper<T extends SortEntity> extends AbstractDragHelper<T> {

    @Override
    public List<T> drag() {
        T currentNode = this.currentNode.get();
        List<T> sortedEntities = new ArrayList<>();
        currentNode.setParentId(this.targetNode.get().getId());
        currentNode.setSortOrder(this.maxSortOrder.getAsInt() + 1);
        sortedEntities.addAll(Tools.Coll.traverse(this.nodeAfterCurrent.get(), entity -> {
            entity.setSortOrder(entity.getSortOrder() - 1);
            return entity;
        }));
        sortedEntities.add(currentNode);
        return sortedEntities;
    }
}

package com.ikingtech.framework.sdk.sms.rpc.api;

import com.ikingtech.framework.sdk.sms.api.SmsApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "SmsRpcApi", path = "/sms")
public interface SmsRpcApi extends SmsApi {
}

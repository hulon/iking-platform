package com.ikingtech.framework.sdk.sms.embedded;

/**
 * @author tie yan
 */
public abstract class AbstractSmsRequest implements SmsRequest {

    protected SmsConfig config;

    @Override
    public SmsRequest config(SmsConfig config) {
        this.config = config;
        return this;
    }
}

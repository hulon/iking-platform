package com.ikingtech.framework.sdk.sms.embedded;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class SmsSendArgs {

    public SmsSendArgs(Builder builder) {
        this.content = builder.content;
        this.phone = builder.phone;
        this.params = builder.params;
        this.templateId = builder.templateId;
        this.signature = builder.signature;
    }

    private String content;

    private String phone;

    private String templateId;

    private Map<String, Object> params;

    private String signature;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String content;

        private String phone;

        private String templateId;

        private Map<String, Object> params;

        private String signature;

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder templateId(String templateId) {
            this.templateId = templateId;
            return this;
        }

        public Builder params(Map<String, Object> params) {
            this.params = params;
            return this;
        }

        public Builder signature(String signature) {
            this.signature = signature;
            return this;
        }

        public SmsSendArgs build() {
            return new SmsSendArgs(this);
        }
    }
}

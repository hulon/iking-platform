package com.ikingtech.framework.sdk.sms.embedded;

import com.ikingtech.framework.sdk.enums.sms.SmsPlatformTypeEnum;

import java.util.List;

/**
 * @author tie yan
 */
public interface SmsRequest {

    void send(SmsSendArgs args);

    List<SmsTemplateInfo> listTemplate();

    SmsRequest config(SmsConfig config);

    SmsPlatformTypeEnum platform();
}

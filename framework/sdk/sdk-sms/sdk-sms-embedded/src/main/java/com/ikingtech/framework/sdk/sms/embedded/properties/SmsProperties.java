package com.ikingtech.framework.sdk.sms.embedded.properties;

import com.ikingtech.framework.sdk.enums.sms.SmsPlatformTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".sms")
public class SmsProperties implements Serializable{

    @Serial
    private static final long serialVersionUID = 5368231654238334344L;

    private List<Platform> platform;

    @Data
    public static class Platform implements Serializable {

        @Serial
    private static final long serialVersionUID = -7196746620623148328L;

        private String id;

        private String name;

        private SmsPlatformTypeEnum type;

        private String appKey;

        private String appSecret;

        private String appId;

        private String extendCode;

        private String endpoint;

        private String signature;

        private String templateId;

        private Boolean supportTemplate = true;
    }
}

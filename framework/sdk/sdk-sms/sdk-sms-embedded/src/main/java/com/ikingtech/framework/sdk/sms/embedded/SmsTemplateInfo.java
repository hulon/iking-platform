package com.ikingtech.framework.sdk.sms.embedded;

import com.ikingtech.framework.sdk.enums.sms.SmsTemplateStatusEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class SmsTemplateInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = -2497252653710326563L;

    private String templateId;

    private String templateName;

    private String content;

    private SmsTemplateStatusEnum status;

    private List<String> params;
}

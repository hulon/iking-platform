package com.ikingtech.framework.sdk.sms.embedded.core.sanyihengxin.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
public class SanYiHengXinSendMessageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 816467278418141177L;

    private String userName;

    private Long timestamp;

    private String sign;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime sendTime;
}

package com.ikingtech.framework.sdk.sms.embedded.core.sanyihengxin.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SanYiHengXinSendMessageOneToOneParam extends SanYiHengXinSendMessageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 4020266920045580674L;

    private List<Message> messageList;

    @Data
    public static class Message implements Serializable{

        @Serial
    private static final long serialVersionUID = 2005841466872716289L;

        private String phone;

        private String content;

        private String extCode;

        private String callData;
    }
}

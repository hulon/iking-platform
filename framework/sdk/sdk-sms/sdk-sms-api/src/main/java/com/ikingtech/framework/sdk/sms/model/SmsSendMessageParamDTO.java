package com.ikingtech.framework.sdk.sms.model;

import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
public class SmsSendMessageParamDTO {

    private String smsId;

    private String templateId;

    private String phone;

    private String content;

    private Map<String, Object> params;
}

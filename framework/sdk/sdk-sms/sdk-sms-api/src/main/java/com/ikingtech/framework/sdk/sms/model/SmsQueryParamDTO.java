package com.ikingtech.framework.sdk.sms.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "SmsQueryParamDTO", description = "短信信息查询参数")
public class SmsQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 3509642635800838407L;

    @Schema(name = "code", description = "短信标识")
    private String code;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "appId", description = "短信AppId")
    private String appId;

    @Schema(name = "type", description = "短信平台")
    private String type;

    @Schema(name = "supportTemplate", description = "是否支持模板配置")
    private Boolean supportTemplate;
}

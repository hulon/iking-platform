package com.ikingtech.framework.sdk.attachment.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.attachment.AttachmentTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
public class AttachmentFileDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3199111133824789311L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "ossFileId", description = "文件编号")
    private String ossFileId;

    @Schema(name = "type", description = "附件类型")
    private AttachmentTypeEnum type;

    @Schema(name = "typeName", description = "附件类型名称")
    private String typeName;

    @Schema(name = "originName", description = "原始文件名称")
    private String originName;

    @Schema(name = "fileSize", description = "文件大小")
    private Long fileSize;

    @Schema(name = "suffix", description = "文件后缀")
    private String suffix;

    @Schema(name = "url", description = "文件访问相对路径")
    private String url;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

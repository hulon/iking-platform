package com.ikingtech.framework.sdk.role.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "RoleMenuUpdateParamDTO", description = "更新角色菜单参数")
public class RoleMenuUpdateParamDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = -4376997976920569359L;

	@Schema(name = "roleId", description = "角色编号")
	private String roleId;

	@Schema(name = "menuIds", description = "菜单编号集合")
	private List<String> menuIds;
}

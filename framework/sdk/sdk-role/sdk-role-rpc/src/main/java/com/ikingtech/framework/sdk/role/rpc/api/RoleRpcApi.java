package com.ikingtech.framework.sdk.role.rpc.api;

import com.ikingtech.framework.sdk.role.api.RoleApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "RoleRpcApi", path = "/system/role")
public interface RoleRpcApi extends RoleApi {
}

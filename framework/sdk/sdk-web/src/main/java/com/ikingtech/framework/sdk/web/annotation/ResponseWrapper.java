package com.ikingtech.framework.sdk.web.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseWrapper {
}

package com.ikingtech.framework.sdk.web.support.server;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class FrameworkServerProxy implements Serializable {

    @Serial
    private static final long serialVersionUID = -673507442496947217L;

    private static FrameworkServer server;

    public static void accept(FrameworkServer server) {
        FrameworkServerProxy.server = server;
    }

    public static FrameworkServer server() {
        return server;
    }
}

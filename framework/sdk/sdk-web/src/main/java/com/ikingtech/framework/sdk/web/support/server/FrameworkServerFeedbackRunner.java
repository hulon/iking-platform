package com.ikingtech.framework.sdk.web.support.server;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;

/**
 * @author tie yan
 */
public interface FrameworkServerFeedbackRunner {

    /**
     * 服务端回调客户端
     *
     * @param data 请求数据
     */
    R<Object> run(Object data);

    /**
     * 获取服务端回调类型
     *
     * @return 服务端回调类型
     */
    FrameworkServerFeedbackTypeEnum type();
}

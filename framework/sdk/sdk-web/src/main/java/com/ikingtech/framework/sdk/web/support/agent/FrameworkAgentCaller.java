package com.ikingtech.framework.sdk.web.support.agent;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;

/**
 * @author tie yan
 */
public interface FrameworkAgentCaller {

    /**
     * 客户端发起远程调用请求
     *
     * @param data 数据
     * @return 执行结果
     */
    R<Object> call(Object data);

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    FrameworkAgentTypeEnum type();

    /**
     * 服务提供方名称
     *
     * @return 客户端类型
     */
    String provider();
}

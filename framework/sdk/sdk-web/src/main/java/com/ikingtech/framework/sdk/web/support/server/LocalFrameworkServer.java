package com.ikingtech.framework.sdk.web.support.server;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class LocalFrameworkServer implements FrameworkServer {

    private final List<FrameworkServerFeedbackRunner> runners;

    private Map<FrameworkServerFeedbackTypeEnum, FrameworkServerFeedbackRunner> runnerMap;

    @Override
    public R<Object> feedback(FrameworkServerFeedbackTypeEnum agentType, Object data) {
        if (!this.runnerMap.containsKey(agentType)) {
            return R.failed("serverFeedbackRunnerNotFound");
        }
        return this.runnerMap.get(agentType).run(data);
    }

    public void init() {
        this.runnerMap = Tools.Coll.convertMap(this.runners, FrameworkServerFeedbackRunner::type);
        FrameworkServerProxy.accept(this);
    }
}

package com.ikingtech.framework.sdk.web.support.server;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class LoadBalanceFrameworkServer implements FrameworkServer {

    private final List<FrameworkServerFeedbackCaller> callers;

    private Map<FrameworkServerFeedbackTypeEnum, FrameworkServerFeedbackCaller> callerMap;

    @Override
    public R<Object> feedback(FrameworkServerFeedbackTypeEnum feedbackType, Object data) {
        if (!this.callerMap.containsKey(feedbackType)) {
            return R.failed("serverFeedbackCallerNotFound");
        }
        return this.callerMap.get(feedbackType).call(data);
    }

    public void init() {
        this.callerMap = Tools.Coll.convertMap(this.callers, FrameworkServerFeedbackCaller::type);
        FrameworkServerProxy.accept(this);
    }
}

package com.ikingtech.framework.sdk.web.annotation;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
@RestController
@Tag(name = "")
@ApiSupport
public @interface ApiController {

    /**
     * Alias for {@link RequestMapping#value}.
     */
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};

    /**
     * Alias for {@link Tag#name}.
     */
    @AliasFor(annotation = Tag.class)
    String name() default "";

    /**
     * Alias for {@link Tag#description}.
     */
    @AliasFor(annotation = Tag.class)
    String description() default "";

    @AliasFor(annotation = ApiSupport.class)
    int order() default Integer.MAX_VALUE;
}

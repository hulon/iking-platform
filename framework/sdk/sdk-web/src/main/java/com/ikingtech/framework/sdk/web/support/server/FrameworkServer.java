package com.ikingtech.framework.sdk.web.support.server;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkServerFeedbackTypeEnum;

/**
 * @author tie yan
 */
public interface FrameworkServer {

    R<Object> feedback(FrameworkServerFeedbackTypeEnum agentType, Object data);
}

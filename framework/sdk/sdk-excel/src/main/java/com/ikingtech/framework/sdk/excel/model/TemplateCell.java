package com.ikingtech.framework.sdk.excel.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class TemplateCell implements Serializable {

    @Serial
    private static final long serialVersionUID = 4715464387113258426L;

    private Integer startColumn;

    private Integer startLine;

    private Integer endColumn;

    private Integer endLine;

    private String content;

    private Integer height;

    private Integer width;

    private TemplateCellLayout layout;

    public static TemplateCellBuilder builder() {
        return new TemplateCellBuilder();
    }
}

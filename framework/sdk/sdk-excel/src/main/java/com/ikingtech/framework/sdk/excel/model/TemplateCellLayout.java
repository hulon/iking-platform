package com.ikingtech.framework.sdk.excel.model;

import com.ikingtech.framework.sdk.excel.enums.WorkCellBorderStyleEnum;
import com.ikingtech.framework.sdk.excel.enums.WorkCellFontPositionEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class TemplateCellLayout implements Serializable {

    @Serial
    private static final long serialVersionUID = -8327282721668856257L;

    private Integer fontSize;

    private String fontStyle;

    private Integer[] fontColour;

    private Boolean bold;

    private Boolean underline;

    private Boolean italic;

    private WorkCellFontPositionEnum fontPosition;

    private Integer[] backgroundColour;

    private WorkCellBorderStyleEnum border;
}

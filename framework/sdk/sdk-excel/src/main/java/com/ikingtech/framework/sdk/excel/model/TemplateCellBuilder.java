package com.ikingtech.framework.sdk.excel.model;

import com.ikingtech.framework.sdk.excel.enums.WorkCellBorderStyleEnum;
import com.ikingtech.framework.sdk.excel.enums.WorkCellFontPositionEnum;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
public class TemplateCellBuilder implements Serializable {

    @Serial
    private static final long serialVersionUID = 6883589532533363915L;

    private final TemplateCell cell;

    public TemplateCellBuilder() {
        this.cell = new TemplateCell();
        TemplateCellLayout layout = new TemplateCellLayout();
        this.cell.setHeight(20);
        layout.setFontStyle("宋体");
        layout.setFontSize(13);
        layout.setBorder(WorkCellBorderStyleEnum.ALL);
        this.cell.setLayout(layout);
    }

    public TemplateCellBuilder startRow(Integer rowNo) {
        this.cell.setStartLine(rowNo);
        return this;
    }

    public TemplateCellBuilder endRow(Integer rowNo) {
        this.cell.setEndLine(rowNo);
        return this;
    }

    public TemplateCellBuilder startColumn(Integer columnNo) {
        this.cell.setStartColumn(columnNo);
        return this;
    }

    public TemplateCellBuilder endColumn(Integer columnNo) {
        this.cell.setEndColumn(columnNo);
        return this;
    }

    public TemplateCellBuilder height(Integer height) {
        this.cell.setHeight(height);
        return this;
    }

    public TemplateCellBuilder width(Integer width) {
        this.cell.setWidth(width);
        return this;
    }

    public TemplateCellBuilder fontStyle(String style) {
        this.cell.getLayout().setFontStyle(style);
        return this;
    }

    public TemplateCellBuilder fontSize(Integer size) {
        this.cell.getLayout().setFontSize(size);
        return this;
    }

    public TemplateCellBuilder fontColour(String colour) {
        this.cell.getLayout().setFontColour(this.convertColour(colour));
        return this;
    }

    public TemplateCellBuilder bold() {
        this.cell.getLayout().setBold(true);
        return this;
    }

    public TemplateCellBuilder underline() {
        this.cell.getLayout().setUnderline(true);
        return this;
    }

    public TemplateCellBuilder italic() {
        this.cell.getLayout().setItalic(true);
        return this;
    }

    public TemplateCellBuilder fontPosition(WorkCellFontPositionEnum position) {
        this.cell.getLayout().setFontPosition(position);
        return this;
    }

    public TemplateCellBuilder backgroundColour(String colour) {
        this.cell.getLayout().setBackgroundColour(this.convertColour(colour));
        return this;
    }

    public TemplateCellBuilder border(WorkCellBorderStyleEnum style) {
        this.cell.getLayout().setBorder(style);
        return this;
    }

    public TemplateCellBuilder content(String content) {
        this.cell.setContent(content);
        return this;
    }

    public TemplateCell build() {
        if (this.cell.getEndColumn() == null) {
            this.cell.setEndColumn(this.cell.getStartColumn());
        }
        if (this.cell.getEndLine() == null) {
            this.cell.setEndLine(this.cell.getStartLine());
        }
        return this.cell;
    }

    private Integer[] convertColour(String colour) {
        if (colour.length() == 6) {
            colour += "00";
        }
        Integer[] rgb = new Integer[4];
        for (int i = 0; i < colour.length() - 1; i += 2) {
            rgb[i / 2] = Integer.parseUnsignedInt(colour.substring(i, i + 2), 16);
        }
        return rgb;
    }
}

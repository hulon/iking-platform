package com.ikingtech.framework.sdk.authenticate.extension;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.cache.constants.CacheConstants.IDENTITY_EXTENSION_REPORTER;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class IdentityExtensionReporterCollector implements ApplicationRunner {

    private final StringRedisTemplate redisTemplate;

    private final List<IdentityExtensionLoader> loaders;

    @Value("${spring.application.name}")
    private String serverName;

    @Override
    public void run(ApplicationArguments args) {
        if ("auth".equals(serverName)) {
            return;
        }
        if (Tools.Coll.isBlank(this.loaders)) {
            this.redisTemplate.opsForSet().remove(IDENTITY_EXTENSION_REPORTER, this.serverName);
        }
        this.redisTemplate.opsForSet().add(IDENTITY_EXTENSION_REPORTER, this.serverName);
    }
}

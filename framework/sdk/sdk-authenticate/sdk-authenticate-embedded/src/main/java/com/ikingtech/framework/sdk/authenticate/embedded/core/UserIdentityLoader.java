package com.ikingtech.framework.sdk.authenticate.embedded.core;

import com.ikingtech.framework.sdk.context.security.Identity;

/**
 * @author tie yan
 */
public interface UserIdentityLoader {

    Identity loadByCredential(String credentialName);

    Identity loadBySocial(String socialId, String socialNo);
}

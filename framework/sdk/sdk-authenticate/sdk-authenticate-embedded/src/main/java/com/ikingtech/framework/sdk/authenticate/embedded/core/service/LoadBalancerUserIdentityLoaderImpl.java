package com.ikingtech.framework.sdk.authenticate.embedded.core.service;

import com.ikingtech.framework.sdk.authenticate.embedded.core.UserIdentityLoader;
import com.ikingtech.framework.sdk.context.security.Identity;
import com.ikingtech.framework.sdk.user.model.UserSocialQueryParamDTO;
import com.ikingtech.framework.sdk.user.rpc.api.UserRpcApi;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author tie yan
 */
@Component
@RequiredArgsConstructor
public class LoadBalancerUserIdentityLoaderImpl implements UserIdentityLoader {

    private final UserRpcApi rpcApi;

    @Override
    public Identity loadByCredential(String credentialName) {
        return Tools.Bean.copy(this.rpcApi.getInfoByCredential(credentialName).getData(), Identity.class);
    }

    @Override
    public Identity loadBySocial(String socialId, String socialNo) {
        UserSocialQueryParamDTO param = new UserSocialQueryParamDTO();
        param.setSocialId(socialId);
        param.setSocialNo(socialNo);
        return  Tools.Bean.copy(this.rpcApi.getInfoBySocial(param).getData(), Identity.class);
    }
}

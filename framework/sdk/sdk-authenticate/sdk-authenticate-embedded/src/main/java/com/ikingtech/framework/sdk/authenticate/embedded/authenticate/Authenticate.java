package com.ikingtech.framework.sdk.authenticate.embedded.authenticate;

import com.ikingtech.framework.sdk.authenticate.embedded.core.Credential;
import com.ikingtech.framework.sdk.context.security.Identity;

/**
 * @author tie yan
 */
public interface Authenticate {

	/**
	 * 执行身份认证
	 * @param credential 身份凭证
	 * @return 用户信息
	 */
	Identity verify(Credential credential);

	/**
	 * 根据参数返回符合要求的认证处理方法
	 * @param credential 身份凭证
	 * @return 是否符合要求
	 */
	Boolean support(Credential credential);

}

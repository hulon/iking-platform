package com.ikingtech.framework.sdk.workbench.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Redirect {

    String link() default "";

    String param() default "";
}

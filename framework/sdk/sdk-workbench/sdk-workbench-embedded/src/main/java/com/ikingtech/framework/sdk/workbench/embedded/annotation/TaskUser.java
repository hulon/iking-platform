package com.ikingtech.framework.sdk.workbench.embedded.annotation;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TaskUser {

    boolean loginUser() default false;

    String userId() default "";
}

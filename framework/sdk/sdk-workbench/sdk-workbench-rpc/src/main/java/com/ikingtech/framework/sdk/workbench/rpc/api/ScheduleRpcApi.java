package com.ikingtech.framework.sdk.workbench.rpc.api;

import com.ikingtech.framework.sdk.workbench.api.ScheduleApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "ScheduleRpcApi", path = "/schedule")
public interface ScheduleRpcApi extends ScheduleApi {
}

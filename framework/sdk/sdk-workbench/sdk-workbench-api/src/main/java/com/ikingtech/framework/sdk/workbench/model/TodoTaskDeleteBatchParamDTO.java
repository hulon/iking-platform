package com.ikingtech.framework.sdk.workbench.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "TodoTaskDeleteBatchParamDTO", description = "待办任务批量删除参数")
public class TodoTaskDeleteBatchParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3128842238999342899L;

    @Schema(name = "businessId", description = "业务数据编号")
    private String businessId;

    @Schema(name = "userIds", description = "用户编号")
    private List<String> userIds;
}

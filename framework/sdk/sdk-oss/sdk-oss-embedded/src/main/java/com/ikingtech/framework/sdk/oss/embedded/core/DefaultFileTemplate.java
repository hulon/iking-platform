package com.ikingtech.framework.sdk.oss.embedded.core;

import com.ikingtech.framework.sdk.oss.embedded.exception.OssEmbeddedException;

import java.io.InputStream;
import java.util.Map;

/**
 * @author tie yan
 */
public class DefaultFileTemplate implements FileTemplate {

    private static final String FILE_SERVER_NOT_ENABLED = "[N/A]file server not enabled.";

    /**
     * 创建桶
     *
     * @param bucketName 桶名称
     */
    @Override
    public void createBucket(String bucketName) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    @Override
    public void removeBucket(String bucketName) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);

    }

    /**
     * 上传文件
     *
     * @param path        对象路径
     * @param objectName  对象名称
     * @param stream      文件流
     * @param contentType 媒体类型
     * @return 执行结果
     */
    @Override
    public String putObject(String path, String objectName, InputStream stream, String contentType) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    @Override
    public String putObject(String path, String dir, String objectName, InputStream stream, String contentType) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    /**
     * 分片上传文件
     *
     * @param path        对象路径
     * @param objectName  对象名称
     * @param contentType 文件传输类型
     * @return 执行结果
     */
    @Override
    public OssInitMultiUploadResponse initMultiUpload(String path, String objectName, String contentType) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    /**
     * 分片上传文件
     *
     * @param path           对象路径
     * @param partObjectName 分片文件名
     * @param uploadId       分片上传编号
     * @param partNo         分片序号
     * @param stream         文件流
     * @return 分片ETag
     */
    @Override
    public String putObjectSlice(String path, String partObjectName, String uploadId, Integer partNo, InputStream stream) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    /**
     * 完成分片上传文件
     *
     * @param path       对象路径
     * @param initResponse 分片上传任务创建结果
     * @param parts      分片信息，key为分片序号，value为分片上传返回的ETag
     * @return 执行结果
     */
    @Override
    public String completeMultiUpload(String path, OssInitMultiUploadResponse initResponse, Map<Integer, String> parts) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    /**
     * 下载文件
     *
     * @param bucketName 桶名称
     * @param objectName 对象名称
     * @return 文件流
     */
    @Override
    public OssResponse getObject(String bucketName, String objectName) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }

    /**
     * 删除桶
     *
     * @param bucketName 桶名称
     * @param objectName 对象名称
     */
    @Override
    public void removeObject(String bucketName, String objectName) {
        throw new OssEmbeddedException(FILE_SERVER_NOT_ENABLED);
    }
}

package com.ikingtech.framework.sdk.oss.embedded.core;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author tie yan
 */
public abstract class AbstractFileTemplate implements FileTemplate {

    public String objectNameFormat(String objectName) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")) + "/" + objectName;
    }


}

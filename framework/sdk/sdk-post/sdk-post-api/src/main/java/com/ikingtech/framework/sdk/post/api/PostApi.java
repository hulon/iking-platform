package com.ikingtech.framework.sdk.post.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.post.model.PostDTO;
import com.ikingtech.framework.sdk.post.model.PostQueryParamDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
public interface PostApi {

    /**
     * 添加岗位信息
     *
     * @param post 岗位信息
     * @return 返回添加结果
     */
    @PostRequest(order = 1, value = "/add", summary = "添加岗位信息", description = "添加岗位信息")
    R<String> add(@Parameter(name = "post", description = "岗位信息")
                  @RequestBody PostDTO post);

    /**
     * 删除岗位信息
     *
     * @param id 编号
     * @return 返回删除结果
     */
    @PostRequest(order = 2, value = "/delete", summary = "删除岗位信息", description = "删除岗位信息")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新岗位信息
     *
     * @param post 岗位信息
     * @return 返回更新结果
     */
    @PostRequest(order = 3, value = "/update", summary = "更新岗位信息", description = "更新岗位信息")
    R<Object> update(@Parameter(name = "post", description = "岗位信息")
                     @RequestBody PostDTO post);

    /**
     * 分页查询岗位信息
     *
     * @param queryParam 查询条件
     * @return 返回分页结果
     */
    @PostRequest(order = 4, value = "/list/page", summary = "分页查询岗位信息", description = "分页查询岗位信息")
    R<List<PostDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                          @RequestBody PostQueryParamDTO queryParam);

    /**
     * 查询所有岗位信息
     *
     * @return 返回所有岗位信息
     */
    @PostRequest(order = 5, value = "/list/all", summary = "查询所有岗位信息", description = "查询所有岗位信息")
    R<List<PostDTO>> all();

    /**
     * 查询指定编号的岗位信息
     *
     * @param id 编号
     * @return 返回指定编号的岗位信息
     */
    @PostRequest(order = 6, value = "/detail/id", summary = "查询指定编号的岗位信息", description = "查询指定编号的岗位信息")
    R<PostDTO> detail(@Parameter(name = "id", description = "编号")
                      @RequestBody String id);

    /**
     * 根据编号集合查询岗位信息
     *
     * @param ids 编号集合
     * @return 返回根据编号集合查询的岗位信息
     */
    @PostRequest(order = 7, value = "/map/ids", summary = "根据编号集合查询岗位信息", description = "根据编号集合查询岗位信息")
    R<Map<String, PostDTO>> mapByIds(@Parameter(name = "ids", description = "编号集合")
                                     @RequestBody BatchParam<String> ids);
}

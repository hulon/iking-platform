package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveFormWidgetTypeEnum {

    INPUT("单行输入"),

    TIME("时间"),

    NUMBER("数字"),

    CHECKBOX("复选框"),

    RADIO("单选项"),

    DATE("日期"),

    SELECT("下拉选项"),

    DIVIDER("分割线"),

    DEPT_PICKER("部门选择组件"),

    ORGANIZATION_PICKER("组织选择组件"),

    USER_PICKER("用户选择组件"),

    ROLE_PICKER("角色选择组件"),

    POST_PICKER("岗位选择组件"),

    TEXTAREA("文本域"),

    DATE_RANGE("日期范围"),

    TIME_RANGE("时间范围"),

    SWITCH("开关"),

    RATE("评分"),

    COLOUR_PICKER("颜色选择"),

    SLIDER("滑块"),

    STATIC_TEXT("静态文字"),

    HTML("Html"),

    BUTTON("按钮"),

    IK_UPLOAD_FILE("文件上传"),

    IK_UPLOAD_IMAGE("图片上传"),

    TREE_SELECT("树型选择");

    public final String description;
}

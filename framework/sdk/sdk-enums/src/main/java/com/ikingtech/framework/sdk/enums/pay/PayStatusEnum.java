package com.ikingtech.framework.sdk.enums.pay;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum PayStatusEnum {

    PAYING("支付中"),

    PAY_TIMEOUT("支付超时"),

    PAY_SUCCESS("支付成功"),

    PAY_FAILED("支付失败"),

    PAY_CANCEL("支付已取消"),

    PAY_CANCEL_FAIL("支付取消失败"),

    REFUNDED("已退款"),

    REFUNDED_FAIL("退款失败"),

    RED_PACK_SENDING("红包发放中"),

    RED_PACK_SUCCESS("红包发放成功"),

    RED_PACK_FAIL("红包发放失败");

    public final String description;
}

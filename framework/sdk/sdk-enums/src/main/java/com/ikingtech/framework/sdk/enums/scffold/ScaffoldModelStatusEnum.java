package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldModelStatusEnum {

    UNPUBLISHED("未发布"),

    PUBLISHING("发布中"),

    PUBLISHED("已发布");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveExecutorCategoryEnum {

    /**
     * 审批人
     */
    APPROVAL("审批人"),

    /**
     * 审批人为空时的审批人
     */
    RESERVE_APPROVAL("审批人为空时的审批人"),

    /**
     * 部门选择控件
     */
    DEPT_SELECT_COMPONENT("部门选择控件"),

    /**
     * 用户选择控件
     */
    USER_SELECT_COMPONENT("用户选择控件");

    public final String description;
}

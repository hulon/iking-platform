package com.ikingtech.framework.sdk.enums.component;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum PickerElementTypeEnum {

    USER("用户"),

    ROLE("角色"),

    ORGANIZATION("组织"),

    DEPT("部门"),

    POST("岗位");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.system.user;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum UserConfigTypeEnum {

    FRONT_GRAY_SCALE("页面灰度"),

    THEME("主题"),

    LANGUAGE("语言");

    public final String description;
}

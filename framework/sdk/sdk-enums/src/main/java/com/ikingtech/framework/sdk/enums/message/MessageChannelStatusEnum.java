package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum MessageChannelStatusEnum {

    ENABLED("已启用"),

    DISABLED("已停用");

    public final String description;
}

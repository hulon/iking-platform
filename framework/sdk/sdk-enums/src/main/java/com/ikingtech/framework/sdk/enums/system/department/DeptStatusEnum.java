package com.ikingtech.framework.sdk.enums.system.department;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DeptStatusEnum {

    ENABLE("已启用"),

    DISABLE("已停用");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.application;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApplicationModelTypeEnum {

    MASTER("主模型"),

    SLAVE("子模型"),

    MAPPING("映射模型");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveInitiatorSpecifiedScopeTypeEnum {

    /**
     * 全公司
     */
    ALL("全公司"),

    /**
     * 按角色指定
     */
    BY_ROLE("按角色指定"),

    /**
     * 按成员指定
     */
    BY_USER("按成员指定");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.system.menu;

import com.ikingtech.framework.sdk.enums.domain.DomainEnum;

import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
public interface MenuDefinition extends Serializable {

    String name();

    String appCode();

    String parent();

    String description();

    String euName();

    String permissionCode();

    String fullPath();

    Integer sortOrder();

    String component();

    Boolean framework();

    Boolean sidebar();

    Boolean breadcrumb();

    String icon();

    String activeIcon();

    String logo();

    String iframe();

    String link();

    MenuTypeEnum type();

    MenuJumpTypeEnum jumpType();

    Boolean keepAlive();

    List<DomainEnum> initDomains();
}

package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum FrameworkServerFeedbackTypeEnum {

    /**
     * 审批回调
     */
    APPROVE_PROCESS_CALLBACK_FEEDBACK("approve-process-callback-feedback", "审批回调"),

    /**
     * 定时任务回调
     */
    JOB_FEEDBACK("job-feedback", "定时任务回调"),

    /**
     * 登录事件回调
     */
    SIGN_EVENT_FEEDBACK("sign-event", "登录事件"),

    /**
     * 用户信息回调
     */
    USER_INFO_FEEDBACK("user-info", "用户信息回调");

    public final String serverAction;

    public final String description;
}

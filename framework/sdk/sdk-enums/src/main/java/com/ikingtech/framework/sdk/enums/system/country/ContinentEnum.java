package com.ikingtech.framework.sdk.enums.system.country;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ContinentEnum {

    /**
     * 亚洲
     */
    ASIA("亚洲"),

    /**
     * 欧洲
     */
    EUROPE("欧洲"),

    /**
     * 北美洲
     */
    NORTH_AMERICA("北美洲"),

    /**
     * 南美洲
     */
    SOUTH_AMERICA("南美洲"),

    /**
     * 大洋洲
     */
    OCEANIA("大洋洲"),

    /**
     * 非洲
     */
    AFRICA("非洲"),

    /**
     * 南极洲
     */
    ANTARCTIC("南极洲");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.system.dictionary;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DictTypeEnum {

    /**
     * 系统类
     */
    SYSTEM("系统类"),

    /**
     * 业务类
     */
    BUSINESS("业务类");

    /**
     * 描述
     */
    public final String description;
}

package com.ikingtech.framework.sdk.enums.attachment;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum AttachmentTypeEnum {

    FILE("文件"),

    IMAGE("图片"),

    VIDEO("视频"),

    AUDIO("音频"),

    WORD("Word文档"),

    EXCEL("Excel文档"),

    PPT("PPT文档"),

    ZIP("压缩包");

    public final String description;
}

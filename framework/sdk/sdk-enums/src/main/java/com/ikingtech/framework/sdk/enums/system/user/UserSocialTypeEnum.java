package com.ikingtech.framework.sdk.enums.system.user;

import com.ikingtech.framework.sdk.enums.authenticate.SignEndpointTypeEnum;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum UserSocialTypeEnum {

    WECHAT_MINI_OPEN_ID("微信小程序OpenId", SignEndpointTypeEnum.WECHAT_MINI),

    WECHAT_MINI_UNION_ID("微信小程序UnionId", SignEndpointTypeEnum.WECHAT_MINI);

    public final String description;

    public final SignEndpointTypeEnum signEndpointType;
}

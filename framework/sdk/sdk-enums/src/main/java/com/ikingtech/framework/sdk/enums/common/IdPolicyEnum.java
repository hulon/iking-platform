package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum IdPolicyEnum {

    /**
     * 自动
     */
    AUTO("自动"),

    /**
     * 手动
     */
    MANUAL("手动");

    public final String description;
}

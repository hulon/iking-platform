package com.ikingtech.framework.sdk.enums.system.department;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum DeptEquityRatioEnum {

    WHOLLY_OWNED("全资"),

    PARTICIPATION("参股"),

    HOLDING("控股");

    public final String description;
}

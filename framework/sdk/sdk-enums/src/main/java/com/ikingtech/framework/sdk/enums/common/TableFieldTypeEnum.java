package com.ikingtech.framework.sdk.enums.common;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum TableFieldTypeEnum {

    VARCHAR("字符串(varchar)", "varchar", "  {} varchar({}) COMMENT '{}',\n", JavaFieldTypeEnum.STRING),

    CHAR("字符串(char)", "char", "  {} char({}) COMMENT '{}',\n", JavaFieldTypeEnum.STRING),


    TEXT("字符串(text)", "text", "  {} text COMMENT '{}',\n", JavaFieldTypeEnum.STRING),


    MEDIUM_TEXT("字符串(mediumtext)", "mediumtext", "  {} mediumtext COMMENT '{}',\n", JavaFieldTypeEnum.STRING),


    LONG_TEXT("字符串(longText)", "longtext", "  {} longtext COMMENT '{}',\n", JavaFieldTypeEnum.STRING),


    BLOB("字符串(blob)", "blob", "  {} blob COMMENT '{}',\n", JavaFieldTypeEnum.STRING),

    JSON("字符串(json)", "json", "  {} json COMMENT '{}',\n", JavaFieldTypeEnum.STRING),


    DATETIME("日期(datetime)", "datetime", "  {} datetime COMMENT '{}',\n", JavaFieldTypeEnum.LOCAL_DATE_TIME),


    DATE("日期(date)", "date", "  {} date COMMENT '{}',\n", JavaFieldTypeEnum.LOCAL_DATE),


    TIME("时间(time)", "time", "  {} time COMMENT '{}',\n", JavaFieldTypeEnum.LOCAL_TIME),


    TIMESTAMP("时间戳(timestamp)", "timestamp", "  {} timestamp COMMENT '{}',\n", JavaFieldTypeEnum.LOCAL_DATE_TIME),


    INT("数字(int)", "int", "  {} int COMMENT '{}',\n", JavaFieldTypeEnum.INT),


    BIG_INT("数字(bigint)", "bigint", "  {} bigint COMMENT '{}',\n", JavaFieldTypeEnum.LONG),


    TINYINT("数字(tinyint)", "tinyint", "  {} tinyint COMMENT '{}',\n", JavaFieldTypeEnum.BOOLEAN),


    DOUBLE("数字(double)", "double", "  {} double COMMENT '{}',\n", JavaFieldTypeEnum.DOUBLE),

    
    FLOAT("数字(float)", "float", "  {} float COMMENT '{}',\n", JavaFieldTypeEnum.FLOAT);

    public final String description;

    public final String dbType;

    public final String sqlTemplate;

    public final JavaFieldTypeEnum javaType;

    public String sqlFormat(String fieldName, String fieldLength, String fieldRemark) {
        return VARCHAR.equals(this) ? Tools.Str.format(this.sqlTemplate, fieldName, fieldLength, fieldRemark) : Tools.Str.format(this.sqlTemplate, fieldName, fieldRemark);
    }

    public static TableFieldTypeEnum valueOfDbType(String dbTypeStr) {
        for (TableFieldTypeEnum value : TableFieldTypeEnum.values()) {
            if (value.dbType.equals(dbTypeStr)) {
                return value;
            }
        }
        return null;
    }
}

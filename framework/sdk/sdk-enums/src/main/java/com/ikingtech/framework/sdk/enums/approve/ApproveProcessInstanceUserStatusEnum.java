package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveProcessInstanceUserStatusEnum {

    /**
     * 待执行
     */
    WAITING("待执行"),

    /**
     * 审批中
     */
    RUNNING("执行中"),

    /**
     * 已通过
     */
    END("完成");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveAppendTypeEnum {

    /**
     * 在我之前
     */
    BEFORE("在我之前"),

    /**
     * 在我之后
     */
    AFTER("在我之后");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveFormInstancePreviewTypeEnum {

    BY_FORM_CONFIG("表单配置"),

    BY_DRAFT("草稿");

    public final String description;
}

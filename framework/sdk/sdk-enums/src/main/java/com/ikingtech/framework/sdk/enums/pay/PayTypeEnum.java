package com.ikingtech.framework.sdk.enums.pay;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum PayTypeEnum {

    ALI_PAY_SCAN("支付宝扫码支付"),

    ALI_PAY_JS("支付宝JS支付"),

    ALI_PAY_APP("支付宝APP支付"),

    WECHAT_H5("微信H5支付"),

    WECHAT_APP("微信APP支付"),

    WECHAT_JS("微信JS支付"),

    WECHAT_SCAN("微信扫码支付"),

    WECHAT_MINI("微信小程序支付"),

    WECHAT_RED_PACK("微信红包");

    public final String description;
}

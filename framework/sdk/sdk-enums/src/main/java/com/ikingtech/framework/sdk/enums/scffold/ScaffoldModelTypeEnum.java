package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldModelTypeEnum {

    MASTER("主模型"),

    SLAVE("子模型"),

    MAPPING("映射模型");

    public final String description;
}

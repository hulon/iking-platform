package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveTypeEnum {

    /**
     * 人工审批
     */
    MANUALLY("人工审批"),

    /**
     * 自动执行
     */
    AUTO_PASS("自动通过"),

    /**
     * 自动拒绝
     */
    AUTO_REJECT("自动拒绝");

    public final String description;
}

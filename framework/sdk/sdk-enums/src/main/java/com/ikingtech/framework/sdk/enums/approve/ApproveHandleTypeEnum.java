package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum ApproveHandleTypeEnum {

    SUBMIT("提交"),

    REVOKE("撤销"),

    PASS("通过"),

    REJECT("拒绝"),

    BACK("退回");

    public final String description;
}

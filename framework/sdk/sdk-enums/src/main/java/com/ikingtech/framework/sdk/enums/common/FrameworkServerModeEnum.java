package com.ikingtech.framework.sdk.enums.common;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum FrameworkServerModeEnum {

    /**
     * 单节点
     */
    SINGLETON("单节点"),

    /**
     * 集群
     */
    CLUSTER("集群");

    public final String description;
}

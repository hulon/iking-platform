package com.ikingtech.framework.sdk.enums.system.tenant;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum TenantTypeEnum {

    /**
     * 主租户
     */
    MASTER("主租户"),

    /**
     * 普通租户
     */
    NORMAL("普通租户");

    public final String description;
}

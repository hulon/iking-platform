package com.ikingtech.framework.sdk.enums.message;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public enum MessageSendChannelEnum {

    /**
     * 系统消息
     */
    SYSTEM("系统消息"),

    /**
     * 微信小程序-订阅消息
     */
    WECHAT_MINI_SUBSCRIBE("微信小程序-订阅消息"),

    /**
     * 短信
     */
    SMS("短信"),

    /**
     * 钉钉
     */
    DING_TALK_ROBOT("钉钉群机器人消息");

    public final String description;
}

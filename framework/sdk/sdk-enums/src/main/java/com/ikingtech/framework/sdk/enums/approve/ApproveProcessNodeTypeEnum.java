package com.ikingtech.framework.sdk.enums.approve;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ApproveProcessNodeTypeEnum {

    /**
     * 发起人节点
     */
    INITIATOR("发起人节点"),

    /**
     * 审批
     */
    APPROVE("审批节点"),

    /**
     * 抄送
     */
    CARBON_COPY("抄送节点"),

    /**
     * 办理
     */
    DISPOSE("办理节点"),

    /**
     * 分支节点
     */
    BRANCH("分支节点"),

    /**
     * 条件节点
     */
    CONDITION("条件节点"),

    /**
     * 结束节点
     */
    END("结束节点");

    public final String description;
}

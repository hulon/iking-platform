package com.ikingtech.framework.sdk.enums.system.menu;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * 菜单显示类型
 *
 * @author tie yan
 */
@AllArgsConstructor
public enum MenuViewTypeEnum {

	/**
	 * 业务端
	 */
	BUSINESS("业务端", 1),

	/**
	 * 管理端
	 */
	MANAGE("管理端", 2);

	public final String description;

	public final Integer sortOrder;
}

package com.ikingtech.framework.sdk.enums.scffold;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum ScaffoldModelRelationTypeEnum {

    ONE_TO_ONE("一对一"),

    ONE_TO_MANY("一对多");

    public final String description;
}

package com.ikingtech.framework.sdk.enums.system.variable;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum AllowMultiSignInEnum {

    /**
     * 允许同时登录
     */
    ALLOW("允许"),

    /**
     * 不允许同时登录
     */
    NOT_ALLOW("不允许");

    public final String description;
}

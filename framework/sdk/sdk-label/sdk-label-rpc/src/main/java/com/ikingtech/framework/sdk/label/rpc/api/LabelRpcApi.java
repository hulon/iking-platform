package com.ikingtech.framework.sdk.label.rpc.api;

import com.ikingtech.framework.sdk.label.api.LabelApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "LabelRpcApi", path = "/system/label")
public interface LabelRpcApi extends LabelApi {
}

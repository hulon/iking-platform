package com.ikingtech.framework.sdk.call.huawei;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HuaweiPrivateCallResponse extends HuaweiResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 1224293674786718229L;

    private String subscriptionId;
}

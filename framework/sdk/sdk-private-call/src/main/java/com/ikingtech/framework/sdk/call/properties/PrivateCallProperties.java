package com.ikingtech.framework.sdk.call.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(GLOBAL_CONFIG_PREFIX + ".private-call")
public class PrivateCallProperties {

    private String platform;

    private String appKey;

    private String secretKey;

    private String appUrl;

    private Huawei huawei;

    @Data
    public static class Huawei implements Serializable {

        @Serial
    private static final long serialVersionUID = -6678352430483557940L;

        private String areaCode;

        private Integer bindDuration;

        private Integer callDuration;

        private Boolean recordFlag;

        private Integer direction;

        private String recordHint;

        private Boolean sms;
    }
}

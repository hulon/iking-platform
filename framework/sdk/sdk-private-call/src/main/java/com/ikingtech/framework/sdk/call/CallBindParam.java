package com.ikingtech.framework.sdk.call;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class CallBindParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -5485947105079423034L;

    /**
     * 拨打方
     */
    private String callerPhone;

    /**
     * 被拨打方
     */
    private String calleePhone;

    /**
     * 隐私关系编号
     */
    private String relationNumber;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private final CallBindParam bindParam;

        public Builder() {
            this.bindParam = new CallBindParam();
        }

        public Builder callerPhone(String callerPhone) {
            this.bindParam.setCallerPhone(callerPhone);
            return this;
        }

        public Builder calleePhone(String calleePhone) {
            this.bindParam.setCalleePhone(calleePhone);
            return this;
        }

        public Builder relationNumber(String relationNumber) {
            this.bindParam.setRelationNumber(relationNumber);
            return this;
        }

        public CallBindParam build() {
            return this.bindParam;
        }
    }
}

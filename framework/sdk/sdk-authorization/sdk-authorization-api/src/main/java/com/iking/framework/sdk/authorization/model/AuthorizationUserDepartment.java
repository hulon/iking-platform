package com.iking.framework.sdk.authorization.model;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class AuthorizationUserDepartment implements Serializable {

    @Serial
    private static final long serialVersionUID = -1904761159605843529L;

    private String userId;

    private String deptId;
}

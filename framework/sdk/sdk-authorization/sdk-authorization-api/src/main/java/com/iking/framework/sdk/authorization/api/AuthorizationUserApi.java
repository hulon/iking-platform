package com.iking.framework.sdk.authorization.api;

import com.iking.framework.sdk.authorization.model.AuthorizationUser;

/**
 * @author tie yan
 */
public interface AuthorizationUserApi {

    AuthorizationUser loadUser(String userId);
}

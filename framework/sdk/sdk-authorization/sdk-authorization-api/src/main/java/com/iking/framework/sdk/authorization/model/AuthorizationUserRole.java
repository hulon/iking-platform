package com.iking.framework.sdk.authorization.model;

import com.ikingtech.framework.sdk.enums.system.role.DataScopeTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class AuthorizationUserRole implements Serializable {

    @Serial
    private static final long serialVersionUID = 7119034896311328064L;

    private String userId;

    private String roleId;

    private DataScopeTypeEnum dataScopeType;

    private List<String> dataScopeCodes;
}

package com.ikingtech.framework.sdk.log.embedded.runner;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.log.api.LogApi;
import com.ikingtech.framework.sdk.log.model.rpc.AuthLogReportParam;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class AuthLogReportAgentRunner implements FrameworkAgentRunner {

    private final LogApi api;

    /**
     * 执行客户端请求
     *
     * @param data 请求数据
     * @return 执行结果
     */
    @Override
    public R<Object> run(Object data) {
        try {
            return this.api.reportAuthLog((AuthLogReportParam) data);
        } catch (Exception e) {
            return R.failed();
        }
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.AUTH_LOG;
    }
}

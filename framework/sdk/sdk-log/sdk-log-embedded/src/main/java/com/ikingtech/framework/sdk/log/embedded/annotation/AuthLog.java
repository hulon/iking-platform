package com.ikingtech.framework.sdk.log.embedded.annotation;

import com.ikingtech.framework.sdk.enums.log.SignTypeEnum;

import java.lang.annotation.*;

/**
 * 登录日志注解
 *
 * @author zhangqiang
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthLog {

    SignTypeEnum type();
}

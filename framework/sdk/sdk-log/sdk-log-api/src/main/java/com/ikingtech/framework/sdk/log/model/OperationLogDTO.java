package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志信息
 *
 * @author wangbo
 */
@Data
@Schema(name = "OperationLogDTO", description = "操作日志信息")
public class OperationLogDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -9175130347752782987L;

    @Schema(name = "id", description = "日志主键")
    private String id;

    @Schema(name = "module", description = "业务模块")
    private String module;

    @Schema(name = "operation", description = "操作内容")
    private String operation;

    @Schema(name = "method", description = "方法名称")
    private String method;

    @Schema(name = "requestMethod", description = "请求方式")
    private String requestMethod;

    @Schema(name = "requestUrl", description = "请求地址")
    private String requestUrl;

    @Schema(name = "requestParam", description = "请求参数")
    private String requestParam;

    @Schema(name = "responseBody", description = "响应体")
    private String responseBody;

    @Schema(name = "operateUserId", description = "操作用户名id")
    private String operateUserId;

    @Schema(name = "operateUsername", description = "操作用户名")
    private String operateUsername;

    @Schema(name = "ip", description = "客户端IP地址")
    private String ip;

    @Schema(name = "location", description = "客户端IP归属地")
    private String location;

    @Schema(name = "success", description = "是否执行成功")
    private Boolean success;

    @Schema(name = "message", description = "返回消息")
    private String message;

    @Schema(name = "operationTime", description = "操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime operationTime;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;
}

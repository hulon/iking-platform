package com.ikingtech.framework.sdk.log.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.log.SignTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 登录日志信息
 * 
 * @author tie yan
 */
@Data
@Schema(name = "AuthLogDTO", description = "登录日志信息")
public class AuthLogDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2721389741870556855L;

    @Schema(name = "id", description = "日志主键")
    private String id;

    @Schema(name = "userId", description = "用户id")
    private String userId;

    @Schema(name = "username", description = "用户账号")
    private String username;

    @Schema(name = "type", description ="登录类型")
    private SignTypeEnum type;

    @Schema(name = "typeName", description ="登录类型名称")
    private String typeName;

    @Schema(name = "ip", description = "登录IP地址")
    private String ip;

    @Schema(name = "location", description = "登录IP归属地")
    private String location;

    @Schema(name = "browser", description = "浏览器")
    private String browser;

    @Schema(name = "os", description = "操作系统")
    private String os;

    @Schema(name = "success", description = "是否执行成功")
    private Boolean success;

    @Schema(name = "message", description = "返回信息")
    private String message;

    @Schema(name = "signTime", description = "登录/登出时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime signTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

package com.ikingtech.framework.sdk.log.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class SystemLogBatchReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 1634040993120611103L;

    /**
     * 操作日志信息集合
     */
    private List<SystemLogReportParam> logReportParams;
}

package com.ikingtech.framework.sdk.menu.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "RoleMenuAssignDTO", description = "角色菜单分配信息")
public class RoleMenuAssignDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = -4376997976920569359L;

	@Schema(name = "roleId", description = "角色编号")
	private String roleId;

	@Schema(name = "menus", description = "菜单集合")
	private List<MenuDTO> menus;
}

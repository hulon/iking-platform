package com.ikingtech.framework.sdk.menu.api;

import java.util.Collection;
import java.util.List;

/**
 * @author tie yan
 */
public interface MenuRoleApi {

    void removeRoleMenu(Collection<String> menuIds);

    List<String> loadMenuId(String roleId);

    List<String> loadMenuId(List<String> roleIds);
}

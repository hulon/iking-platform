package com.ikingtech.framework.sdk.department.model;

import com.ikingtech.framework.sdk.label.model.LabelDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "DeptDTO", description = "部门信息")
public class DeptDTO extends DeptBasicDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = 5256915461880042297L;

	@Schema(name = "userCount", description = "部门下用户数量")
	private Integer userCount;

	@Schema(name = "managerId", description = "主管用户编号")
	private String managerId;

	@Schema(name = "manager", description = "主管用户基本信息")
	private DeptManagerDTO manager;

	@Schema(name = "labelIds", description = "组织标签编号")
	private List<String> labelIds;

	@Schema(name = "labels", description = "组织标签")
	private List<LabelDTO> labels;
}

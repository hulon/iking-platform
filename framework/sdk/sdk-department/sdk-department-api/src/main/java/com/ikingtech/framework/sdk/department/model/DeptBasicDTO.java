package com.ikingtech.framework.sdk.department.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.department.DeptEquityRatioEnum;
import com.ikingtech.framework.sdk.enums.system.department.DeptIntroductionTypeEnum;
import com.ikingtech.framework.sdk.enums.system.department.DeptStatusEnum;
import com.ikingtech.framework.sdk.enums.system.department.DeptTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DeptBasicDTO", description = "组织架构基本信息")
public class DeptBasicDTO implements Serializable {

	@Serial
    private static final long serialVersionUID = 4190647170062682412L;

	@Schema(name = "id", description = "主键")
	private String id;

	@Schema(name = "parentId", description = "父级组织架构编号")
	private String parentId;

	@Schema(name = "tenantCode", description = "租户标识")
	private String tenantCode;

	@Schema(name = "name", description = "组织架构名称")
	private String name;

	@Schema(name = "fullPath", description = "组织架构全路径，@分割")
	private String fullPath;

	@Schema(name = "type", description = "组织架构类型")
	private DeptTypeEnum type;

	@Schema(name = "fullPath", description = "组织架构类型名称")
	private String typeName;

	@Schema(name = "status", description = "状态")
	private DeptStatusEnum status;

	@Schema(name = "status", description = "状态")
	private String statusName;

	@Schema(name = "grade", description = "组织架构级别")
	private Integer grade;

	@Schema(name = "code", description = "编码")
	private String code;

	@Schema(name = "shortName", description = "组织简称")
	private String shortName;

	@Schema(name = "equityRatio", description = "股权形式")
	private DeptEquityRatioEnum equityRatio;

	@Schema(name = "equityRatioName", description = "股权形式名称")
	private String equityRatioName;

	@Schema(name = "phone", description = "联系电话")
	private String phone;

	@Schema(name = "fax", description = "传真号")
	private String fax;

	@Schema(name = "establishDate", description = "成立日期")
	private LocalDate establishDate;

	@Schema(name = "provinceCode", description = "省级编码")
	private String provinceCode;

	@Schema(name = "provinceName", description = "省级名称")
	private String provinceName;

	@Schema(name = "cityCode", description = "市级编码")
	private String cityCode;

	@Schema(name = "cityName", description = "市级名称")
	private String cityName;

	@Schema(name = "districtCode", description = "区级编码")
	private String districtCode;

	@Schema(name = "districtName", description = "区级名称")
	private String districtName;

	@Schema(name = "streetCode", description = "街道编码")
	private String streetCode;

	@Schema(name = "streetName", description = "街道名称")
	private String streetName;

	@Schema(name = "address", description = "详细地址")
	private String address;

	@Schema(name = "homePage", description = "组织首页")
	private String homePage;

	@Schema(name = "introductionType", description = "简介类型")
	private DeptIntroductionTypeEnum introductionType;

	@Schema(name = "introductionTypeName", description = "简介类型名称")
	private String introductionTypeName;

	@Schema(name = "introduction", description = "简介")
	private String introduction;

	@Schema(name = "sortOrder", description = "排序值")
	private Integer sortOrder;

	@Schema(name = "defaultFlag", description = "是否默认组织架构")
	private Boolean defaultFlag;

	@Schema(name = "del_flag", description = "是否删除")
	private Boolean delFlag;

	@Schema(name = "inDataScope", description = "存在于数据权限中")
	private Boolean inDataScope;

	@Schema(name = "createBy", description = "创建人编号")
	private String createBy;

	@Schema(name = "createName", description = "创建人姓名")
	private String createName;

	@Schema(name = "updateBy", description = "更新人编号")
	private String updateBy;

	@Schema(name = "updateName", description = "更新人姓名")
	private String updateName;

	@Schema(name = "createTime", description = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

	@Schema(name = "updateTime", description = "更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime updateTime;
}

package com.ikingtech.framework.sdk.rpc.decoder;

import feign.Response;
import feign.Util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author tie yan
 */
public class DefaultFeignReason implements FeignErrorReason {

    @Override
    public String is(String methodKey, Response response) {
        try {
            return null == response.body() ?
                    response.reason() :
                    Util.toString(response.body().asReader(StandardCharsets.UTF_8));
        } catch (IOException e) {
            return "未知错误";
        }
    }
}

package com.ikingtech.framework.sdk.plugin.configuration;

import com.ikingtech.framework.sdk.plugin.core.CrudPluginRegistrar;
import com.ikingtech.framework.sdk.plugin.core.DelegatePluginRegistrar;
import com.ikingtech.framework.sdk.plugin.core.PluginRegistrar;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author tie yan
 */
public class PluginConfiguration {

    @Bean
    public PluginRegistrar crudPluginRegistrar() {
        return new CrudPluginRegistrar();
    }

    @Bean
    public DelegatePluginRegistrar delegatePluginRegistrar(List<PluginRegistrar> registrars) {
        return new DelegatePluginRegistrar(registrars);
    }
}

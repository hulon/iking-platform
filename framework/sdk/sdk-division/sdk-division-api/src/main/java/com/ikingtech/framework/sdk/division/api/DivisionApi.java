package com.ikingtech.framework.sdk.division.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.division.model.DivisionDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;

import java.util.List;

/**
 * @author tie yan
 */
public interface DivisionApi {

    /**
     * 获取所有行政区域信息
     *
     * @return 返回部门列表
     */
    @PostRequest(order = 1, value = "/list/all", summary = "获取所有行政区域信息", description = "获取所有行政区域信息")
    R<List<DivisionDTO>> all();
}

package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class WechatRedPackParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -7181765711220540330L;

    @JsonProperty(value = "nonce_str")
    private String nonceStr;

    private String sign;

    @JsonProperty(value = "mch_billno")
    private String mchBillNo;

    @JsonProperty(value = "mch_id")
    private String mchId;

    @JsonProperty(value = "wxappid")
    private String wxAppId;

    @JsonProperty(value = "send_name")
    private String sendName;

    @JsonProperty(value = "re_openid")
    private String reOpenid;

    @JsonProperty(value = "total_amount")
    private Integer totalAmount;

    @JsonProperty(value = "total_num")
    private Integer totalNum;

    private String wishing;

    @JsonProperty(value = "client_ip")
    private String clientIp;

    @JsonProperty(value = "act_name")
    private String actName;

    private String remark;

    @JsonProperty(value = "appid")
    private String appId;

    @JsonProperty(value = "bill_type")
    private String billType;
}

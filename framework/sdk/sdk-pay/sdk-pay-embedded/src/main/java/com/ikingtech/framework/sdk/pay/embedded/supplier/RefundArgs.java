package com.ikingtech.framework.sdk.pay.embedded.supplier;

import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class RefundArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = 5198365497599467109L;

    public RefundArgs(Builder builder) {
        this.requestId = builder.requestId;
        this.totalAmount = builder.totalAmount;
        this.refundAmount = builder.refundAmount;
        this.payType = builder.payType;
    }

    private String requestId;

    private Double totalAmount;

    private Double refundAmount;

    private PayTypeEnum payType;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String requestId;

        private Double totalAmount;

        private Double refundAmount;

        private PayTypeEnum payType;

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder refundAmount(Double refundAmount) {
            this.refundAmount = refundAmount;
            return this;
        }

        public Builder totalAmount(Double totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public Builder payType(PayTypeEnum payType) {
            this.payType = payType;
            return this;
        }

        public RefundArgs build() {
            return new RefundArgs(this);
        }
    }
}

package com.ikingtech.framework.sdk.pay.embedded.properties;

import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@Data
@ConfigurationProperties(prefix = GLOBAL_CONFIG_PREFIX + ".pay")
public class PayProperties {

    private List<Supplier> suppliers;

    @Data
    public static class Supplier implements Serializable {

        @Serial
    private static final long serialVersionUID = 1509782860033570520L;

        private String id;

        private CashierSupplierEnum type;

        private String customId;

        private String customName;

        private String customSerialNo;

        private String appId;

        private String apiKey;

        private String extraApiKey;

        private String privateKey;

        private String certKey;

        private String publicKey;

        private String notifyUrl;
    }
}

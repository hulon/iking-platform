package com.ikingtech.framework.sdk.pay.embedded;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.pay.embedded.supplier.CashierSupplierConfig;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class CashierBuilder {

    private final List<Cashier> cashiers;

    public Cashier build(CashierSupplierConfig cashierSupplierConfig) {
        for (Cashier cashier : this.cashiers) {
            if (cashierSupplierConfig.getType().equals(cashier.supplier())) {
                return cashier.config(cashierSupplierConfig);
            }
        }
        throw new FrameworkException("不支持的支付平台");
    }
}

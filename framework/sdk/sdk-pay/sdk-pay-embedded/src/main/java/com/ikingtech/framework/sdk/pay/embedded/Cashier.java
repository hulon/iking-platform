package com.ikingtech.framework.sdk.pay.embedded;

import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import com.ikingtech.framework.sdk.pay.embedded.supplier.*;
import com.ikingtech.framework.sdk.pay.embedded.supplier.*;

import java.util.Map;

/**
 * @author tie yan
 */
public interface Cashier {

    PayOrder pay(PayArgs args);

    CancelResult cancel(CancelArgs args);

    RefundResult refund(RefundArgs args);

    RedPackResult redPack(RedPackArgs args);

    RedPackResult readPackConfirm(RedPackArgs args);

    PayResult validate(String resultParam, Map<String, Object> resultParamMap);

    Cashier config(CashierSupplierConfig config);

    CashierSupplierEnum supplier();

    PayResult getPayResult(GetPayResultArgs args);
}

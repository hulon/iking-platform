package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ikingtech.framework.sdk.enums.pay.CashierSupplierEnum;
import com.ikingtech.framework.sdk.pay.embedded.supplier.CashierSupplierConfig;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayArgs;
import com.ikingtech.framework.sdk.pay.embedded.supplier.PayOrder;
import com.ikingtech.framework.sdk.utils.Tools;
import com.wechat.pay.java.core.RSAAutoCertificateConfig;
import com.wechat.pay.java.service.payments.jsapi.JsapiService;
import com.wechat.pay.java.service.payments.jsapi.JsapiServiceExtension;
import com.wechat.pay.java.service.payments.jsapi.model.Amount;
import com.wechat.pay.java.service.payments.jsapi.model.CloseOrderRequest;
import com.wechat.pay.java.service.payments.jsapi.model.Payer;
import com.wechat.pay.java.service.payments.jsapi.model.PrepayRequest;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WechatJsapiPayCashier {

    public static PayOrder prepay(PayArgs payArgs, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        PrepayRequest request = new PrepayRequest();
        Amount amount = new Amount();
        amount.setTotal((int) (payArgs.getAmount() * 100));
        request.setAmount(amount);
        request.setMchid(config.getCustomId());
        request.setAppid(config.getAppId());
        request.setDescription(payArgs.getSubject());
        request.setNotifyUrl(config.getNotifyUrl() + "/" + CashierSupplierEnum.WECHAT_PAY.name() + "/" + payArgs.getRequestId());
        request.setOutTradeNo(payArgs.getRequestId());
        Payer payer = new Payer();
        payer.setOpenid(payArgs.getChannelUserId());
        request.setPayer(payer);
        JsapiServiceExtension payService = new JsapiServiceExtension.Builder().config(rsaConfig).build();
        PayOrder payOrder = new PayOrder();
        payOrder.setRequestId(payArgs.getRequestId());
        payOrder.setPayInfo(Tools.Json.toJsonStr(payService.prepayWithRequestPayment(request)));
        return payOrder;
    }

    public static void closeOrder(String requestId, CashierSupplierConfig config, RSAAutoCertificateConfig rsaConfig) {
        CloseOrderRequest request = new CloseOrderRequest();
        request.setMchid(config.getCustomId());
        request.setOutTradeNo(requestId);
        JsapiService payService = new JsapiService.Builder().config(rsaConfig).build();
        payService.closeOrder(request);
    }

    @Data
    public static class WechatJsapiWakeUpParam implements Serializable {

        @Serial
    private static final long serialVersionUID = 6909007463371589922L;

        private String appId;

        private String timeStamp;

        private String nonceStr;

        @JsonProperty(value = "package")
        private String prePayId;

        private String signType;

        private String paySign;
    }
}

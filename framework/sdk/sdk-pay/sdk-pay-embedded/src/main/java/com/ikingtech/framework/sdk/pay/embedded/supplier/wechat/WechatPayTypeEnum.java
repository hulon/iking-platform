package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */

@RequiredArgsConstructor
public enum WechatPayTypeEnum {

    JSAPI("微信JSAPI支付"),

    APP("微信App支付"),

    H5("微信H5支付"),

    NATIVE("微信扫码支付"),

    MINI("微信小程序支付");

    public final String description;
}

package com.ikingtech.framework.sdk.pay.embedded.supplier.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Administrator
 */
@Data
public class WechatRedPackResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -7181765711220540330L;

    @JsonProperty(value = "return_code")
    private String returnCode;

    @JsonProperty(value = "return_msg")
    private String returnMsg;

    @JsonProperty(value = "result_code")
    private String resultCode;

    @JsonProperty(value = "err_code")
    private String errorCode;

    @JsonProperty(value = "err_code_des")
    private String errorCodeDesc;

    @JsonProperty(value = "mch_billno")
    private String mchBillNo;

    @JsonProperty(value = "mch_id")
    private String mchId;

    @JsonProperty(value = "wxappid")
    private String wxAppId;

    @JsonProperty(value = "re_openid")
    private String reOpenid;

    @JsonProperty(value = "total_amount")
    private Integer totalAmount;

    @JsonProperty(value = "send_listid")
    private String sendListId;

    @JsonProperty(value = "detail_id")
    private String detailId;

    @JsonProperty(value = "status")
    private String status;

    @JsonProperty(value = "send_type")
    private String sendType;

    @JsonProperty(value = "hb_type")
    private String redPackType;

    @JsonProperty(value = "total_num")
    private Integer totalNum;

    private String reason;

    @JsonProperty(value = "send_time")
    private String sendTime;

    @JsonProperty(value = "refund_time")
    private String refundTime;

    @JsonProperty(value = "refund_amount")
    private Integer refundAmount;

    public boolean fail() {
        return !"SUCCESS".equals(this.getReturnCode()) ||
                !"SUCCESS".equals(this.getResultCode());
    }
}

package com.ikingtech.framework.sdk.pay.embedded.supplier.allinpay;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 通联支付-收银宝-网上收银统一支付接口参数
 *
 * @author tie yan
 */
@Data
public class AllInPaySybPayResult implements Serializable {

    @Serial
    private static final long serialVersionUID = 5802575916769328057L;

    /**
     * 返回码
     * 此字段是通信标识，非交易结果，交易是否成功需要查看trxstatus来判断
     */
    private String retcode;

    /**
     * 返回码说明
     */
    private String retmsg;

    /**
     * 商户号
     * 平台分配的商户号
     */
    private String cusid;

    /**
     * 应用ID
     * 平台分配的APPID
     */
    private String appid;

    /**
     * 交易单号
     * 收银宝平台的交易流水号
     */
    private String trxid;

    /**
     * 渠道平台交易单号
     * 例如微信,支付宝平台的交易单号
     */
    private String chnltrxid;

    /**
     * 商户的交易订单号
     */
    private String reqsn;

    /**
     * 随机生成的字符串
     */
    private String randomstr;

    /**
     * 交易的状态,
     * 对于刷卡支付，该状态表示实际的支付结果，其他为下单状态
     */
    private String trxstatus;

    /**
     * 交易完成时间
     * yyyyMMddHHmmss
     * 对于微信刷卡支付有效
     */
    private String fintime;

    /**
     * 错误原因
     * 失败的原因说明
     */
    private String errmsg;

    /**
     * 支付串
     * 扫码支付则返回二维码串，js支付则返回json字符串
     * QQ钱包及云闪付的JS支付返回支付的链接,商户只需跳转到此链接即可完成支付
     * 支付宝App支付返回支付信息串
     */
    private String payinfo;

    /**
     * 签名
     */
    private String sign;

    public boolean fail() {
        return !"SUCCESS".equals(this.getRetcode());
    }
}

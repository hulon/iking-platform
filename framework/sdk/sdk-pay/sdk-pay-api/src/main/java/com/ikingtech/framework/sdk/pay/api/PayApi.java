package com.ikingtech.framework.sdk.pay.api;

import com.ikingtech.framework.sdk.base.model.BatchParam;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.pay.model.*;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface PayApi {

    /**
     * 支付下单
     *
     * @param payment 支付信息
     * @return PayOrderDTO
     */
    @PostRequest(order = 1, value = "/order", summary = "支付下单", description = "支付下单")
    R<PayOrderDTO> order(@Parameter(name = "payment", description = "支付信息")
                         @RequestBody PaymentDTO payment);

    /**
     * 退款
     *
     * @param refundParam 退款参数
     * @return Object
     */
    @PostRequest(order = 2, value = "/refund", summary = "退款", description = "退款")
    R<Object> refund(@Parameter(name = "refundParam", description = "退款参数")
                     @RequestBody PayRefundParamDTO refundParam);

    /**
     * 取消支付
     *
     * @param cancelParam 取消支付参数
     * @return Object
     */
    @PostRequest(order = 3, value = "/order/cancel", summary = "取消支付", description = "取消支付")
    R<Object> cancel(@Parameter(name = "cancelParam", description = "取消支付参数")
                     @RequestBody PaymentCancelParamDTO cancelParam);

    /**
     * 发送红包
     *
     * @param redPack 红包
     * @return Object
     */
    @PostRequest(order = 4, value = "/order/red-pack", summary = "发送红包", description = "发送红包")
    R<Object> redPack(@Parameter(name = "redPack", description = "红包")
                      @RequestBody PayRedPackDTO redPack);

    /**
     * 根据请求编号获取支付结果
     *
     * @param requestId 请求编号
     * @return PayRecordDTO
     */
    @PostRequest(order = 5, value = "/result/request-id", summary = "根据请求编号获取支付结果", description = "根据请求编号获取支付结果")
    R<PayRecordDTO> getPayResultByRequestId(@Parameter(name = "requestId", description = "请求编号")
                                            @RequestBody String requestId);

    /**
     * 根据请求编号集合获取支付结果列表
     *
     * @param requestIds 请求编号集合，用于查询对应的支付结果
     * @return 返回一个支付结果列表，列表中每个元素都是一个PayRecordDTO对象，包含了支付的详细信息
     */
    @PostRequest(order = 6, value = "/result/list/request-ids", summary = "根据请求编号集合获取支付结果列表", description = "根据请求编号集合获取支付结果列表")
    R<List<PayRecordDTO>> listPayResultByRequestIds(@Parameter(name = "requestIds", description = "请求编号集合")
                                                    @RequestBody BatchParam<String> requestIds);
}

package com.ikingtech.framework.sdk.pay.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PayOrderDTO", description = "支付订单信息")
public class PayOrderDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8043618403225675762L;

    @Schema(name = "supplierTradeId", description = "支付平台交易编号")
    private String supplierTradeId;

    @Schema(name = "requestId", description = "请求编号")
    private String requestId;

    @Schema(name = "channelTradeId", description = "支付平台渠道交易编号")
    private String channelTradeId;

    @Schema(name = "payInfo", description = "交易详情")
    private String payInfo;
}

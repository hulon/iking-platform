package com.ikingtech.framework.sdk.pay.model;

import com.ikingtech.framework.sdk.enums.pay.PayTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "PayRedPackDTO", description = "红包参数")
public class PayRedPackDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6366333689003257079L;

    @Schema(name = "merchantId", description = "商户编号")
    private String merchantId;

    @Schema(name = "supplierId", description = "支付平台编号")
    private String supplierId;

    @Schema(name = "payType", description = "支付方式")
    private PayTypeEnum payType;

    @Schema(name = "amount", description = "红包金额")
    private Double amount;

    @Schema(name = "requestId", description = "支付流水号")
    private String requestId;

    @Schema(name = "title", description = "订单标题")
    private String title;

    @Schema(name = "channelUserId", description = "支付渠道用户标识，用户的微信openid/用户user_id/用户小程序的openid等")
    private String channelUserId;

    @Schema(name = "receiverCount", description = "接收红包人数")
    private Integer receiverCount;

    @Schema(name = "wishingWords", description = "红包祝福语")
    private String wishingWords;

    @Schema(name = "remark", description = "红包备注")
    private String remark;
}

package com.ikingtech.framework.sdk.dict.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class DictionaryBeanDefinitionReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -7702836956623708484L;

    /**
     * 字典定义信息集合
     */
    private List<DictionaryBeanDefinition> beanDefinitions;
}

package com.ikingtech.framework.sdk.dict.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class DictionaryBeanDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = -5620315981768501974L;

    /**
     * 主键
     */
    private String id;

    /**
     * 租户标识
     */
    private String tenantCode;

    /**
     * 字典名称
     */
    private String name;

    /**
     * 字典描述
     */
    private String remark;

    /**
     * 字典项集合
     */
    private List<DictionaryItemBeanDefinition> itemBeanDefinitions;
}

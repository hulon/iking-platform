package com.ikingtech.framework.sdk.dict.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "DictItemQueryParamDTO", description = "查询参数")
public class DictItemQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -2086147277202213929L;

    @Schema(name = "dictId", description = "字典编号")
    private String dictId;

    @Schema(name = "value", description = "字典项值")
    private String value;

    @Schema(name = "label", description = "字典项名称")
    private String label;

    @Schema(name = "preset", description = "是否为预置项")
    private Boolean preset;
}

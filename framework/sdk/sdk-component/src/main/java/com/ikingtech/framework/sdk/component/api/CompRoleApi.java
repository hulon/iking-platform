package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentRole;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public interface CompRoleApi {

    List<ComponentRole> listByName(String name);
}

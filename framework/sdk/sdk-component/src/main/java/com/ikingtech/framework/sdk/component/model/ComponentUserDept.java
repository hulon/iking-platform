package com.ikingtech.framework.sdk.component.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ComponentUserDept", description = "业务组件-用户部门信息")
public class ComponentUserDept implements Serializable {

    @Serial
    private static final long serialVersionUID = -335079907050285925L;

    @Schema(name = "id;", description = "主键")
    private String id;

    @Schema(name = "userId", description = "用户编号")
    private String userId;

    @Schema(name = "deptId", description = "部门编号")
    private String deptId;
}

package com.ikingtech.framework.sdk.component.api;

import com.ikingtech.framework.sdk.component.model.ComponentPost;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tie yan
 */
@Service
public interface CompPostApi {

    List<ComponentPost> listByName(String name);
}

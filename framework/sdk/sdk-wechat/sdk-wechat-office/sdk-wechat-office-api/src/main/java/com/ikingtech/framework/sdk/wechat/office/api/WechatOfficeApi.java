package com.ikingtech.framework.sdk.wechat.office.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.framework.sdk.wechat.office.model.*;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author tie yan
 */
public interface WechatOfficeApi {

    /**
     * 新增<br />
     * 新增成功后返回编号
     *
     * @param wechatOffice 微信公众号信息
     * @return 编号
     */
    @PostRequest(order = 1,value = "/add", summary = "", description = "")
    R<String> add(@Parameter(name = "wechatOffice", description = "微信公众号信息")
                  @RequestBody WechatOfficeDTO wechatOffice);

    /**
     * 删除
     *
     * @param id 编号
     * @return 执行结果
     */
    @PostRequest(order = 2,value = "/delete", summary = "", description = "")
    R<Object> delete(@Parameter(name = "id", description = "编号")
                     @RequestBody String id);

    /**
     * 更新
     *
     * @param wechatOffice 微信公众号信息
     * @return 执行结果
     */
    @PostRequest(order = 3,value = "/update", summary = "", description = "")
    R<Object> update(@Parameter(name = "wechatOffice", description = "微信公众号信息")
                     @RequestBody WechatOfficeDTO wechatOffice);

    /**
     * 分页查询<br />
     * 没有查询结果时返回空列表
     *
     * @param queryParam 查询条件
     * @return 全量列表
     */
    @PostRequest(order = 4,value = "/list/page", summary = "", description = "")
    R<List<WechatOfficeDTO>> page(@Parameter(name = "queryParam", description = "查询条件")
                                @RequestBody WechatOfficeQueryParamDTO queryParam);

    /**
     * 全量查询<br />
     * 没有查询结果时返回空列表
     *
     * @return 全量列表
     */
    @PostRequest(order = 5,value = "/list/all", summary = "", description = "")
    R<List<WechatOfficeDTO>> all();

    /**
     * 查询详情<br />
     * 数据不存在时仍然返回成功,数据为null
     *
     * @param id 编号
     * @return 详情
     */
    @PostRequest(order = 6,value = "/detail/id", summary = "", description = "")
    R<WechatOfficeDTO> detail(@Parameter(name = "id", description = "编号")
                            @RequestBody String id);

    @PostRequest(order = 7,value = "/message/send", summary = "", description = "")
    R<Object> sendSubscribeMessage(@RequestBody WechatOfficeSendMessageParamDTO sendMessageParam);

    @PostRequest(order = 8,value = "/exchange-code", summary = "", description = "")
    R<WechatOfficeUserInfoDTO> exchangeCode(@RequestBody WechatOfficeExchangeCodeParamDTO exchangeCodeParam);
}

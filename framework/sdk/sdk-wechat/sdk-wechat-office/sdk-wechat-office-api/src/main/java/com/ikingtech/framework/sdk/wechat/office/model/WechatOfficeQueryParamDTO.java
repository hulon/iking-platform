package com.ikingtech.framework.sdk.wechat.office.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "WechatOfficeQueryParamDTO", description = "微信公众号信息查询参数")
public class WechatOfficeQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -6401342522041531934L;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "appId", description = "AppId")
    private String appId;

    @Schema(name = "appSecret", description = "AppSecret")
    private String appSecret;
}

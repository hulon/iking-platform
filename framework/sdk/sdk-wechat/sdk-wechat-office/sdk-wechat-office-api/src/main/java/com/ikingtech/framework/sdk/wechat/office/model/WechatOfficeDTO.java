package com.ikingtech.framework.sdk.wechat.office.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniDTO", description = "微信公众号信息")
public class WechatOfficeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 404386084900166458L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "名称")
    private String name;

    @Schema(name = "appId", description = "AppId")
    private String appId;

    @Schema(name = "appSecret", description = "AppSecret")
    private String appSecret;

    @Schema(name = "templates", description = "公众号消息模板集合")
    private List<WechatOfficeMessageTemplateDTO> templates;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

package com.ikingtech.framework.sdk.wechat.mini.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniSendMessageParamDTO", description = "微信小程序订阅消息发送参数")
public class WechatMiniSendMessageParamDTO {

    @Schema(name = "wechatMiniId", description = "微信小程序标识")
    private String wechatMiniId;

    @Schema(name = "messageTemplateId", description = "微信小程序订阅消息模板编号")
    private String templateId;

    @Schema(name = "openId", description = "微信小程序用户OpenId")
    private String openId;

    @Schema(name = "redirectTo", description = "微信小程序跳转链接")
    private String redirectTo;

    @Schema(name = "param", description = "微信小程序订阅消息模板参数")
    private Map<String, Object> param;
}

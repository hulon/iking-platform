package com.ikingtech.framework.sdk.wechat.mini.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniQrcodeGenerateParamDTO", description = "生成小程序二维码参数")
public class WechatMiniQrcodeGenerateParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -979548124784505553L;

    @Schema(name = "wechatMiniId", description = "小程序编号")
    private String wechatMiniId;

    @Schema(name = "page", description = "小程序路由")
    private String page;

    @Schema(name = "param", description = "参数")
    private String param;

    @Schema(name = "width", description = "宽度")
    private Integer width;

    @Schema(name = "version", description = "小程序版本（开发版/体验版/正式版）")
    private String version;
}

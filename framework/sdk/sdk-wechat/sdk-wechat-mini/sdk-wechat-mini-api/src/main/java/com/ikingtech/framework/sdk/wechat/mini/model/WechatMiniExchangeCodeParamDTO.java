package com.ikingtech.framework.sdk.wechat.mini.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "WechatMiniExchangeCodeParamDTO", description = "微信小程序获取用户OpenId及UnionId参数")
public class WechatMiniExchangeCodeParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -977060126737891594L;

    @Schema(name = "wechatMiniId", description = "微信小程序标识")
    private String wechatMiniId;

    @Schema(name = "code", description = "授权码")
    private String code;
}

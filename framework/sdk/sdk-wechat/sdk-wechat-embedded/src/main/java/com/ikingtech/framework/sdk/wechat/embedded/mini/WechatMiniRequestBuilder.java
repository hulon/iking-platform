package com.ikingtech.framework.sdk.wechat.embedded.mini;

import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.wechat.embedded.properties.WechatProperties;
import lombok.RequiredArgsConstructor;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class WechatMiniRequestBuilder {

    private final WechatProperties properties;

    private final WechatMiniRequest request;

    public WechatMiniRequest build(String wechatMiniId) {
        for (WechatProperties.WechatMini wechatMini : this.properties.getMini()) {
            if (Tools.Str.equals(wechatMiniId, wechatMini.getId())) {
                return this.build(Tools.Bean.copy(wechatMini, WechatMiniConfig.class));
            }
        }
        throw new FrameworkException("不支持的短信渠道");
    }

    public WechatMiniRequest build(WechatMiniConfig config) {
        return request.config(config);
    }

}

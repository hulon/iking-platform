package com.ikingtech.framework.sdk.wechat.embedded.mini;

import com.ikingtech.framework.sdk.enums.wechat.WechatMiniSubscribeMessageTemplateTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class WechatMiniSubscribeMessageTemplateInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = -2497252653710326563L;

    private String templateId;

    private String templateTitle;

    private String content;

    private WechatMiniSubscribeMessageTemplateTypeEnum type;

    private List<String> params;
}

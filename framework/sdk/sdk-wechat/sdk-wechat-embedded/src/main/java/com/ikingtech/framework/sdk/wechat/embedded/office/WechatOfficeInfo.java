package com.ikingtech.framework.sdk.wechat.embedded.office;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatOfficeInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 8522718600960253305L;

    private String wechatOfficeId;

    private String wechatOfficeAppId;

    private String wechatOfficeAppSecret;

    private String wechatOfficeName;
}

package com.ikingtech.framework.sdk.wechat.embedded.mini;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatMiniConfig implements Serializable {

    @Serial
    private static final long serialVersionUID = -7373429060684329451L;

    /**
     * 小程序AppId
     */
    private String appId;

    /**
     * 小程序AppSecret
     */
    private String appSecret;
}

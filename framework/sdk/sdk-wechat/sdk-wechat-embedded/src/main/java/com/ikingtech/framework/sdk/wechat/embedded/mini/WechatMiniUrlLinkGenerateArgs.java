package com.ikingtech.framework.sdk.wechat.embedded.mini;

import lombok.Data;

/**
 * @author tie yan
 */
@Data
public class WechatMiniUrlLinkGenerateArgs {

    public WechatMiniUrlLinkGenerateArgs(Builder builder) {
        this.page = builder.page;
        this.param = builder.param;
    }

    String page;

    private String param;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        String page;

        private String param;

        public Builder page(String page) {
            this.page = page;
            return this;
        }

        public Builder param(String param) {
            this.param = param;
            return this;
        }

        public WechatMiniUrlLinkGenerateArgs build() {
            return new WechatMiniUrlLinkGenerateArgs(this);
        }
    }
}

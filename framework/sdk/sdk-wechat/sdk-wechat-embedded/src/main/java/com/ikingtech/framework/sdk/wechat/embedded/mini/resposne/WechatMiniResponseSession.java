package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WechatMiniResponseSession extends WechatMiniResponse<Object> implements Serializable {

    @Serial
    private static final long serialVersionUID = -31749476297340133L;

    /**
     * 用户在开放平台的唯一标识符
     */
    private String unionid;

    /**
     * 用户唯一标识
     */
    private String openid;

    /**
     * 会话密钥
     */
    @JsonProperty(value = "session_key")
    private String sessionKey;
}

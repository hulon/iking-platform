package com.ikingtech.framework.sdk.wechat.embedded.mini;

import lombok.Data;

/**
 * @author tie yan
 */
@Data
public class WechatMiniQrcodeGenerateArgs {

    public WechatMiniQrcodeGenerateArgs(Builder builder) {
        this.page = builder.page;
        this.param = builder.param;
        this.width = builder.width;
        this.version = builder.version;
    }

    String page;

    private String param;

    private Integer width;

    private String version;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        String page;

        private String param;

        private Integer width;

        private String version;

        public Builder page(String page) {
            this.page = page;
            return this;
        }

        public Builder param(String param) {
            this.param = param;
            return this;
        }

        public Builder width(Integer width) {
            this.width = width;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public WechatMiniQrcodeGenerateArgs build() {
            return new WechatMiniQrcodeGenerateArgs(this);
        }
    }
}

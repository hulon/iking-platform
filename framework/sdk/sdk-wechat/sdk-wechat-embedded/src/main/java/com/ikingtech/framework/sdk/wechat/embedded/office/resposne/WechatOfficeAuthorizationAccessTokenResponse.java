package com.ikingtech.framework.sdk.wechat.embedded.office.resposne;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class WechatOfficeAuthorizationAccessTokenResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 4848469418067703061L;

    /**
     * 授权接口accessToken
     */
    private String access_token;

    /**
     * 用户在开放平台的唯一标识符
     */
    private String unionid;

    /**
     * 用户公众号唯一标识
     */
    private String openid;
}

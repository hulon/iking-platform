package com.ikingtech.framework.sdk.wechat.embedded.mini.resposne;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * WechatMiniResponseSubscribeMessageTemplate is a class that represents a template for the response of a WeChat mini program subscribe message.
 * It implements the Serializable interface to support object serialization.
 * This template contains properties such as priTmplId, title, content, example, type, and keywordEnumValueList.
 * The KeywordEnum class is a nested class within WechatMiniResponseSubscribeMessageTemplate and represents a keyword enumeration.
 * It contains properties keywordCode and enumValueList.
 * @author tie yan
 */
@Data
public class WechatMiniResponseSubscribeMessageTemplate implements Serializable {
    @Serial
    private static final long serialVersionUID = -7687363791489503793L;

    private String priTmplId;

    private String title;

    private String content;

    private String example;

    private Integer type;

    private List<KeywordEnum> keywordEnumValueList;

    /**
     * KeywordEnum is a nested class within WechatMiniResponseSubscribeMessageTemplate that represents a keyword enumeration.
     * It implements the Serializable interface to support object serialization.
     * This class contains properties keywordCode and enumValueList.
     */
    @Data
    public static class KeywordEnum implements Serializable {

        @Serial
    private static final long serialVersionUID = 475273579301782289L;

        private String keywordCode;

        private String enumValueList;
    }
}

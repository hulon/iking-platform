package com.ikingtech.framework.sdk.cluster.configuration;

import com.ikingtech.framework.sdk.cluster.client.ClusterClient;
import com.ikingtech.framework.sdk.cluster.node.ClusterNodeManager;
import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeHeartBeatMessageHandler;
import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeJoinMessageHandler;
import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeMessageHandler;
import com.ikingtech.framework.sdk.cluster.node.handler.ClusterNodeMessageHandlerProxy;
import com.ikingtech.framework.sdk.cluster.propertities.ClusterProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
@EnableConfigurationProperties({ClusterProperties.class})
@ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".cluster", name = "enable", havingValue = "true")
public class ClusterConfiguration {

    @Bean
    public ClusterNodeManager clusterNodeManager() {
        return new ClusterNodeManager();
    }

    @Bean
    public ClusterClient clusterClient(ClusterNodeManager nodeManager) {
        return new ClusterClient(nodeManager);
    }

    @Bean
    public ClusterNodeMessageHandler clusterNodeHeartBeatMessageHandler(ClusterClient clusterClient, ClusterNodeManager nodeManager) {
        return new ClusterNodeHeartBeatMessageHandler(nodeManager, clusterClient);
    }

    @Bean
    public ClusterNodeMessageHandler clusterNodeJoinMessageHandler(ClusterClient clusterClient, ClusterNodeManager nodeManager) {
        return new ClusterNodeJoinMessageHandler(nodeManager, clusterClient);
    }

    @Bean
    public ClusterNodeMessageHandlerProxy clusterNodeMessageHandlerProxy(List<ClusterNodeMessageHandler> nodeMessageHandlers) {
        return new ClusterNodeMessageHandlerProxy(nodeMessageHandlers);
    }
}

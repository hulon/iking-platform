package com.ikingtech.framework.sdk.cluster.node.handler;

import com.ikingtech.framework.sdk.utils.Tools;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Component
@RequiredArgsConstructor
public class ClusterNodeMessageHandlerProxy {

    private final List<ClusterNodeMessageHandler> nodeMessageHandlers;

    private final Map<Class<?>, ClusterNodeMessageHandler> nodeMessageHandlerMap = new HashMap<>();

    public ClusterNodeMessageHandler invoke(Object message) {
        return this.nodeMessageHandlerMap.get(message.getClass());
    }

    public void init() {
        this.nodeMessageHandlerMap.putAll(Tools.Coll.convertMap(this.nodeMessageHandlers, ClusterNodeMessageHandler::getClass));
    }
}

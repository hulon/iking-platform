package com.ikingtech.framework.sdk.cluster.node.handler;

/**
 * @author tie yan
 */
public interface ClusterNodeMessageHandler {

    void handle(Object message);
}

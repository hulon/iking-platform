package com.ikingtech.framework.sdk.limit.embedded.configuration;

import com.ikingtech.framework.sdk.limit.embedded.RateLimiterService;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;

/**
 * @author zhangqiang
 */
public class RateLimitEmbeddedConfiguration {

    @Bean
    public DefaultRedisScript<Long> limitScript() {
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/limit.lua"));
        redisScript.setResultType(Long.class);
        return redisScript;
    }

    @Bean
    public RateLimiterService rateLimiter(RedisTemplate<String, Object> redisTemplate,
                                          RedisScript<Long> redisScript) {
        return new RateLimiterService(redisTemplate, redisScript);
    }

}

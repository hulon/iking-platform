local key = string.gsub(KEYS[1], '"', '')
local count = tonumber(ARGV[1])
local time = tonumber(ARGV[2])
local current = tonumber(redis.call('get', key))
if current == nil then
    redis.call('incr', key)
    redis.call('expire', key, time)
    return 1
end
current = redis.call('incr', key)
if current > count then
    return 0
end
return current
package com.ikingtech.framework.sdk.cmd.configuration;

import com.ikingtech.framework.sdk.cmd.CmdInitializer;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

/**
 * @author tie yan
 */
@RequiredArgsConstructor
public class CmdConfiguration {

    @Bean
    public CmdInitializer cmdInitializer() {
        return new CmdInitializer();
    }
}

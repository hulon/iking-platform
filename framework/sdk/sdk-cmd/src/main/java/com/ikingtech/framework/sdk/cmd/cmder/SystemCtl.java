package com.ikingtech.framework.sdk.cmd.cmder;

import com.ikingtech.framework.sdk.utils.Tools;

import java.util.List;

/**
 * @author tie yan
 */
public class SystemCtl extends AbstractCmder {

    private final String prompt = "systemctl";

    protected final List<String> commands = Tools.Coll.newList(this.prompt);

    public void execute() {
        this.execute(this.commands);
    }

    public static SystemCtl daemonReload() {
        SystemCtl daemonReload = new SystemCtl();
        daemonReload.commands.add("daemon-reload");
        return daemonReload;
    }

    public static Restart restart() {
        Restart restart = new Restart();
        restart.commands.add("restart");
        return new Restart();
    }

    public static class Restart extends SystemCtl {

        public Restart service(String service) {
            this.commands.add(service);
            return this;
        }
    }
}

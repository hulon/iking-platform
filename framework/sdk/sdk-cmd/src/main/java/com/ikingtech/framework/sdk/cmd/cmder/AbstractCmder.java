package com.ikingtech.framework.sdk.cmd.cmder;

import com.ikingtech.framework.sdk.cmd.Cmd;
import com.ikingtech.framework.sdk.cmd.CmdTracer;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;

import java.util.List;

/**
 * @author tie yan
 */
public abstract class AbstractCmder {

    protected Cmd cmd = new Cmd();

    private final String osName = System.getProperty("os.name").toLowerCase();

    protected void execute(List<String> command) {
        CmdTracer.trace(command);
        if (!this.cmd.execute(command)) {
            throw new FrameworkException("命令执行失败");
        }
    }

    protected boolean isWin() {
        return osName.contains("windows");
    }
}

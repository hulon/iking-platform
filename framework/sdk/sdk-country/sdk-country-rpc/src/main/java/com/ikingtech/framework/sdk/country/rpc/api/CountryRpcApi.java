package com.ikingtech.framework.sdk.country.rpc.api;

import com.ikingtech.framework.sdk.country.api.CountryApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "CountryRpcApi", path = "/system/country")
public interface CountryRpcApi extends CountryApi {
}

package com.ikingtech.framework.sdk.country.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.country.model.CountryDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;

import java.util.List;

/**
 * @author tie yan
 */
public interface CountryApi {

    /**
     * 获取所有国家信息
     *
     * @return 返回所有国家信息的列表
     */
    @PostRequest(order = 1, value = "/list/all", summary = "获取所有国家信息", description = "获取所有国家信息")
    R<List<CountryDTO>> all();
}

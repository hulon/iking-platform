package com.ikingtech.framework.sdk.variable.rpc.api;

import com.ikingtech.framework.sdk.variable.api.VariableApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "VariableApi", path = "/system/variable")
public interface VariableRpcApi extends VariableApi {
}

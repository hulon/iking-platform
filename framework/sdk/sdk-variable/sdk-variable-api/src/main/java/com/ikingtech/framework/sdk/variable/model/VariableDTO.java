package com.ikingtech.framework.sdk.variable.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.system.variable.VariableTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统参数信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "VariableDTO", description = "系统参数信息")
public class VariableDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -7415080876046208914L;

    @Schema(name = "id", description = "主键")
    private String id;

    @NotBlank(message = "variableName")
    @Length(max = 32, message = "variableName")
    @Schema(name = "name", description = "参数名称")
    private String name;

    @NotBlank(message = "variableKey")
    @Length(max = 128, message = "variableKey")
    @Schema(name = "variableKey", description = "参数键")
    private String variableKey;

    @NotBlank(message = "variableValue")
    @Length(max = 128, message = "variableValue")
    @Schema(name = "variableValue", description = "参数值")
    private String variableValue;

    @Schema(name = "type", description = "参数类型")
    private VariableTypeEnum type;

    @Schema(name = "typeName", description = "参数类型")
    private String typeName;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

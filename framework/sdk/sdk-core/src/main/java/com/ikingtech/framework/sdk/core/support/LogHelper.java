package com.ikingtech.framework.sdk.core.support;

import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.utils.Tools;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class LogHelper {

    public static void info(String label, String content) {
        log.info("[{} | {} | {}][{}]{}",
                Tools.Str.isNotBlank(Me.domainCode()) ? Me.domainCode() : "UNKNOWN",
                Tools.Str.isNotBlank(Me.tenantCode()) ? Me.tenantCode() : "UNKNOWN",
                Tools.Str.isNotBlank(Me.appCode()) ? Me.appCode() : "UNKNOWN",
                label,
                content);
    }

    public static void info(String label, String contentTemplate, Object... content) {
        log.info("[{}][{}][{}][{}]{}",
                Tools.Str.isNotBlank(Me.domainCode()) ? Me.domainCode() : "UNKNOWN",
                Tools.Str.isNotBlank(Me.tenantCode()) ? Me.tenantCode() : "UNKNOWN",
                Tools.Str.isNotBlank(Me.appCode()) ? Me.appCode() : "UNKNOWN",
                label,
                Tools.Str.format(contentTemplate, content));
    }
}

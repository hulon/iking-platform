package com.ikingtech.framework.sdk.job.embedded.runner;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.job.api.JobApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class JobStopAgentRunner implements FrameworkAgentRunner {

    private final JobApi api;

    /**
     * 客户端发起远程调用请求
     *
     * @param data 数据
     * @return 执行结果
     */
    @Override
    public R<Object> run(Object data) {

        try {
            return this.api.stop((String) data);
        } catch (Exception e) {
            return R.failed();
        }
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.JOB_STOP;
    }
}

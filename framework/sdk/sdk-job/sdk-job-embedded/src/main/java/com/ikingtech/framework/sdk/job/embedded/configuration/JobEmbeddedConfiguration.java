package com.ikingtech.framework.sdk.job.embedded.configuration;

import com.ikingtech.framework.sdk.job.api.JobApi;
import com.ikingtech.framework.sdk.job.embedded.JobExecutorBeanFactory;
import com.ikingtech.framework.sdk.job.embedded.JobResolver;
import com.ikingtech.framework.sdk.job.embedded.caller.JobServerFeedbackCaller;
import com.ikingtech.framework.sdk.job.embedded.caller.JobStartAgentCaller;
import com.ikingtech.framework.sdk.job.embedded.caller.JobStopAgentCaller;
import com.ikingtech.framework.sdk.job.embedded.runner.JobServerFeedbackRunner;
import com.ikingtech.framework.sdk.job.embedded.runner.JobStartAgentRunner;
import com.ikingtech.framework.sdk.job.embedded.runner.JobStopAgentRunner;
import com.ikingtech.framework.sdk.job.rpc.api.JobRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentRunner;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackCaller;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class JobEmbeddedConfiguration {

    @Bean
    public JobExecutorBeanFactory jobExecutorRepository() {
        return new JobExecutorBeanFactory();
    }

    @Bean
    public JobResolver jobResolver() {
        return new JobResolver();
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkAgentCaller jobStartAgentCaller(JobRpcApi rpcApi) {
        return new JobStartAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkAgentCaller jobStopAgentCaller(JobRpcApi rpcApi) {
        return new JobStopAgentCaller(rpcApi);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkServerFeedbackCaller jobFeedbackCaller(LoadBalancerClient loadBalancerClient) {
        return new JobServerFeedbackCaller(loadBalancerClient);
    }

    @Bean
    public FrameworkAgentRunner jobStartAgentRunner(JobApi api) {
        return new JobStartAgentRunner(api);
    }

    @Bean
    public FrameworkAgentRunner jobStopAgentRunner(JobApi api) {
        return new JobStopAgentRunner(api);
    }

    @Bean
    public FrameworkServerFeedbackRunner jobFeedbackRunner(JobResolver jobResolver) {
        return new JobServerFeedbackRunner(jobResolver);
    }
}

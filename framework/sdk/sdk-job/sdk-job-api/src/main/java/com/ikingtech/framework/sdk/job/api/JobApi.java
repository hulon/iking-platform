package com.ikingtech.framework.sdk.job.api;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.job.model.JobDTO;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author tie yan
 */
public interface JobApi {

    /**
     * 启动任务
     *
     * @param job 任务信息
     * @return 返回结果
     */
    @PostRequest(order = 1, value = "/start", summary = "启动任务", description = "启动任务")
    R<Object> start(@Parameter(name = "job", description = "任务信息")
                    @RequestBody JobDTO job);

    /**
     * 停止任务
     *
     * @param jobId 任务编号
     * @return 返回结果
     */
    @PostRequest(order = 2, value = "/stop", summary = "停止任务", description = "停止任务")
    R<Object> stop(@Parameter(name = "jobId", description = "任务编号")
                   @RequestBody String jobId);
}

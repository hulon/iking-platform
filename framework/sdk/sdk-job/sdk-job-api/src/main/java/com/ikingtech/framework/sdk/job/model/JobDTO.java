package com.ikingtech.framework.sdk.job.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.job.JobStatusEnum;
import com.ikingtech.framework.sdk.enums.job.JobTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
public class JobDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -3706876604967722115L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "任务名称")
    private String name;

    @Schema(name = "type", description = "任务类型")
    private JobTypeEnum type;

    @Schema(name = "executorClientId", description = "执行器客户端标识")
    private String executorClientId;

    @Schema(name = "executorHandler", description = "执行器标识")
    private String executorHandler;

    @Schema(name = "cron", description = "cron表达式")
    private String cron;

    @Schema(name = "nextTime", description = "下次执行时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime nextTime;

    @Schema(name = "param", description = "任务参数")
    private String param;

    @Schema(name = "status", description = "任务状态")
    private JobStatusEnum status;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

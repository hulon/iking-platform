package com.ikingtech.framework.sdk.job.model.rpc;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * @author tie yan
 */
@Data
public class JobExecutorBean{

    /**
     * 执行器标识
     */
    private String handler;

    /**
     * 执行器所在Bean
     */
    private Object bean;

    /**
     * 执行器方法
     */
    private Method method;
}

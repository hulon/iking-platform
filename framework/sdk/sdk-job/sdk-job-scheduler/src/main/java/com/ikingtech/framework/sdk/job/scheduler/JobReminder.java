package com.ikingtech.framework.sdk.job.scheduler;

import java.io.Serializable;

/**
 * @author tie yan
 */
public interface JobReminder<T extends Serializable> {

    void remind(ScheduledJob<T> job);
}

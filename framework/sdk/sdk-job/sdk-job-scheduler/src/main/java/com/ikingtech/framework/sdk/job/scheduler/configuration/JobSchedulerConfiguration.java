package com.ikingtech.framework.sdk.job.scheduler.configuration;

import com.ikingtech.framework.sdk.job.scheduler.JobScheduler;
import org.springframework.context.annotation.Bean;

import java.io.Serializable;

/**
 * @author tie yan
 */
public class JobSchedulerConfiguration {

    @Bean
    public <T extends Serializable> JobScheduler<T> jobScheduler() {
        return new JobScheduler<>();
    }
}

package com.ikingtech.framework.sdk.datasource.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.datasource.model.enums.DatasourceSchemaTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DatasourceDTO", description = "数据源")
public class DatasourceDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2874632610744235000L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "name", description = "数据源名称")
    private String name;

    @Schema(name = "code", description = "数据源标识")
    private String code;

    @Schema(name = "ip", description = "数据源IP地址")
    private String ip;

    @Schema(name = "code", description = "数据源端口")
    private String port;

    @Schema(name = "type", description = "数据库类型")
    private DatasourceSchemaTypeEnum type;

    @Schema(name = "typeName", description = "数据库类型名称")
    private String typeName;

    @Schema(name = "username", description = "用户名")
    private String username;

    @Schema(name = "password", description = "密码")
    private String password;

    @Schema(name = "remark", description = "描述")
    private String remark;

    @Schema(name = "driverClassName", description = "驱动类名")
    private String driverClassName;

    @Schema(name = "schemaName", description = "数据库名称")
    private String schemaName;

    @Schema(name = "createName", description = "创建人姓名", hidden = true)
    private String createName;

    @Schema(name = "updateName", description = "更新人姓名", hidden = true)
    private String updateName;

    @Schema(name = "createTime", description = "创建时间", hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间", hidden = true)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

}

package com.ikingtech.framework.sdk.datasource.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "DatasourceTableFieldDTO", description = "数据源表字段信息")
public class DatasourceTableFieldDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 5121830165964420452L;

    @Schema(name = "name", description = "字段名称")
    private String name;

    @Schema(name = "comment", description = "注释")
    private String comment;

    @Schema(name = "type", description = "字段类型")
    private String type;

    @Schema(name = "length", description = "字段长度")
    private Integer length;

    @Schema(name = "nullable", description = "允许空值")
    private Boolean nullable;

    @Schema(name = "primaryKey", description = "是否为主键")
    private Boolean primaryKey;

}

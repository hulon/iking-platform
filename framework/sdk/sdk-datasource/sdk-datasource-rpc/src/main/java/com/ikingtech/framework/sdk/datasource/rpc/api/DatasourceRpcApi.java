package com.ikingtech.framework.sdk.datasource.rpc.api;

import com.ikingtech.framework.sdk.datasource.api.DatasourceApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "DatasourceRpcApi", path = "/datasource")
public interface DatasourceRpcApi extends DatasourceApi {
}

package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.ApproveFormWidgetTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormWidgetDTO", description = "工作流表单控件配置")
public class ApproveFormWidgetDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -5785395930401794638L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "widgetName", description = "表单控件名")
    private String widgetName;

    @Schema(name = "widgetLabel", description = "表单控件标签")
    private String widgetLabel;

    @Schema(name = "widgetType", description = "表单控件类型")
    private ApproveFormWidgetTypeEnum widgetType;

    @Schema(name = "widgetTypeName", description = "表单控件字段类型名称")
    private String widgetTypeName;

    @Schema(name = "required", description = "是否必填字段")
    private Boolean required;

    @Schema(name = "remain", description = "是否为上一表单保留字段")
    private Boolean remain;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.base.model.PageParam;
import com.ikingtech.framework.sdk.enums.approve.ApproveProcessNodeTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ApproveFormInstanceQueryParamDTO", description = "审批表单实例查询参数")
public class ApproveFormInstanceQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -1302835110679714700L;

    @Schema(name = "name", description = "表单名称")
    private String name;

    @Schema(name = "initiatorViewOnly", description = "发起审批页面/表单管理页面")
    private Boolean initiatorViewOnly;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "initiatorTime", description = "创建日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate initiatorTime;

    @Schema(name = "executorUserId", description = "审批人编号")
    private String executorUserId;

    @Schema(name = "approvedByExecutorUser", description = "待审/已审")
    private Boolean approvedByExecutorUser;

    @Schema(name = "carbonCopyUserId", description = "抄送人编号")
    private String carbonCopyUserId;

    @Schema(name = "sharedUserId", description = "被分享人编号")
    private String sharedUserId;

    @Schema(name = "title", description = "表单标题")
    private String title;

    @Schema(name = "initiatorIds", description = "发起人编号")
    private List<String> initiatorIds;

    @Schema(name = "initiatorName", description = "发起人姓名")
    private String initiatorName;

    @Schema(name = "initiatorDeptName", description = "发起人所在部门名称")
    private String initiatorDeptName;

    @Schema(name = "processStatus", description = "流程状态")
    private List<String> processStatus;

    @Schema(name = "serialNo", description = "审批流水号")
    private String serialNo;

    @Schema(name = "businessDataIds", description = "业务数据编号")
    private List<String> businessDataIds;

    @Schema(name = "nodeType", description = "审批记录节点类型")
    private ApproveProcessNodeTypeEnum nodeType;
}

package com.ikingtech.framework.sdk.approve.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ApproveFormBeanDefinitionReportParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -5411731112560766481L;

    private List<ApproveFormBeanDefinition> beanDefinitions;
}

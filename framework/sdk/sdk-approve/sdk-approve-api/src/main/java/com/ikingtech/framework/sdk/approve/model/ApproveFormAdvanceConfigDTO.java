package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.ApproveExecutorDistinctTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormAdvanceConfigDTO", description = "工作流表单高级配置")
public class ApproveFormAdvanceConfigDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 7713727962884651695L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "approveDistinctType", description = "是否自动去重审批人")
    private Boolean approveAutoDistinct;

    @Schema(name = "approveDistinctType", description = "审批人去重类型")
    private ApproveExecutorDistinctTypeEnum approveDistinctType;

    @Schema(name = "allowAddApprove", description = "是否允许加签")
    private Boolean allowAddApprove;

    @Schema(name = "allowRevokePassedApprove", description = "是否允许撤销已通过的审批")
    private Boolean allowRevokePassedApprove;

    @Schema(name = "allowRevokeApproving", description = "是否允许撤销审批中的审批")
    private Boolean allowRevokeApproving;

    @Schema(name = "allowModifyPassedApprove", description = "是否允许修改已通过的审批")
    private Boolean allowModifyPassedApprove;

    @Schema(name = "approveOpinionRequired", description = "审批意见是否必填")
    private Boolean approveOpinionRequired;

    @Schema(name = "approveOpinion", description = "审批意见")
    private String approveOpinion;

    @Schema(name = "approveCommentInvisible", description = "评语是否仅管理员和审批人可见")
    private Boolean approveCommentInvisible;

    @Schema(name = "allowSubmitByOther", description = "是否允许代他人提交")
    private Boolean allowSubmitByOther;

    @Schema(name = "titleTemplate", description = "标题模板")
    private String titleTemplate;

    @Schema(name = "summaryFieldNames", description = "摘要字段名称")
    private List<String> summaryFieldNames;

    @Schema(name = "notifyDay", description = "提醒发送日期")
    private LocalDate notifyDay;

    @Schema(name = "notifyTime", description = "提醒发送时间")
    private LocalTime notifyTime;

    @Schema(name = "approveFormConfigInvisible", description = "表单配置是否仅管理员可见")
    private Boolean approveFormConfigInvisible;

    @Schema(name = "triggerConditions", description = "流程触发条件")
    private List<ApproveCondGroupDTO> triggerConditions;

    @Schema(name = "reportTemplateId", description = "报表模板编号")
    private String reportTemplateId;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

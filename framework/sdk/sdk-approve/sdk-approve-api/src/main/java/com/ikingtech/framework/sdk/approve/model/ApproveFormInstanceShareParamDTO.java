package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormInstanceShareParamDTO", description = "审批实例分享参数")
public class ApproveFormInstanceShareParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -6707368615986879728L;

    @Schema(name = "formInstanceId", description = "审批实例编号")
    private String formInstanceId;

    @Schema(name = "userIds", description = "用户编号列表")
    private List<String> userIds;
}

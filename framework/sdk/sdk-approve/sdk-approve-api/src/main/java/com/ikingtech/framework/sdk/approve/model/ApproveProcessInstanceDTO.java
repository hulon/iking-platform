package com.ikingtech.framework.sdk.approve.model;

import com.ikingtech.framework.sdk.enums.approve.ApproveProcessInstanceStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveProcessInstanceDTO", description = "审批流程实例信息")
public class ApproveProcessInstanceDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1950718245815401154L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "所属表单编号")
    private String formId;

    @Schema(name = "processId", description = "所属审批流编号")
    private String processId;

    @Schema(name = "formInstanceId", description = "表单实例编号")
    private String formInstanceId;

    @Schema(name = "businessDataId", description = "表单实例编号")
    private String businessDataId;

    @Schema(name = "formData", description = "表单数据")
    private String formData;

    @Schema(name = "name", description = "审批流程实例名称")
    private String name;

    @Schema(name = "status", description = "审批流程实例状态")
    private ApproveProcessInstanceStatusEnum status;

    @Schema(name = "statusName", description = "审批流程实例状态名称")
    private String statusName;

    @Schema(name = "recordNodes", description = "审批记录节点列表")
    private List<ApproveRecordNodeDTO> recordNodes;

    @Schema(name = "instanceNodes", description = "审批流程实例节点列表")
    private List<ApproveProcessInstanceNodeDTO> instanceNodes;
}

package com.ikingtech.framework.sdk.approve.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormGroupNameCheckParamDTO", description = "表单分组名称检查参数")
public class ApproveFormGroupNameCheckParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -2185036301113273746L;

    @Schema(name = "id", description = "表单分组编号")
    private String id;

    @Schema(name = "name", description = "表单分组名称")
    private String name;
}

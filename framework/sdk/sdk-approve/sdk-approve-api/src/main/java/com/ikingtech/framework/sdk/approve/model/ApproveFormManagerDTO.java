package com.ikingtech.framework.sdk.approve.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.approve.ApproveExecutorTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApproveFormManagerDTO", description = "表单管理者信息")
public class ApproveFormManagerDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 5730479838758799960L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "formId", description = "表单编号")
    private String formId;

    @Schema(name = "managerId", description = "表单管理者编号")
    private String managerId;

    @Schema(name = "managerName", description = "表单管理者名称")
    private String managerName;

    @Schema(name = "managerType", description = "管理者类型")
    private ApproveExecutorTypeEnum managerType;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

package com.ikingtech.framework.sdk.approve.model.rpc;

import com.ikingtech.framework.sdk.enums.approve.ApproveFormWidgetTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class ApproveFormWidgetBeanDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 7918521993286880773L;

    /**
     * 字段名称
     */
    private String widgetName;

    /**
     * 字段描述
     */
    private String widgetLabel;

    /**
     * 字段类型
     */
    private ApproveFormWidgetTypeEnum widgetType;

    /**
     * 是否必填
     */
    private Boolean required;
}

package com.ikingtech.framework.sdk.approve.embedded.caller;

import com.ikingtech.framework.sdk.approve.model.rpc.ApproveFormBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.approve.rpc.api.ApproveFormRpcApi;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApproveFormReportCaller implements FrameworkAgentCaller {

    private final ApproveFormRpcApi rpcApi;

    @Override
    public R<Object> call(Object data) {
        try {
            R<Object> result = this.rpcApi.report((ApproveFormBeanDefinitionReportParam) data);
            return result.isSuccess() ? R.ok(result.getData()) : R.failed(result.getMsg());
        } catch (Exception e) {
            return R.failed();
        }
    }

    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.APPROVE_FORM_REPORT;
    }

    @Override
    public String provider() {
        return "server";
    }
}

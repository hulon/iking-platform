package com.ikingtech.framework.sdk.approve.embedded.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author tie yan
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface Form {

    String businessType();

    String name();

    boolean visible() default true;
}

package com.ikingtech.framework.sdk.approve.embedded;

import com.ikingtech.framework.sdk.enums.approve.ApproveAppendTypeEnum;
import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class ApproveAppendOpsArgs extends ApproveOpsArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -2106599866267376511L;

    public ApproveAppendOpsArgs(Builder builder) {
        super(builder);
        this.appendType = builder.appendType;
        this.appendExecutorUserIds = builder.appendExecutorUserIds;
    }

    private final ApproveAppendTypeEnum appendType;

    private final List<String> appendExecutorUserIds;

    public Builder builder() {
        return new Builder();
    }

    public static class Builder extends ApproveOpsArgs.Builder {

        private ApproveAppendTypeEnum appendType;

        private List<String> appendExecutorUserIds;

        public Builder appendType(ApproveAppendTypeEnum appendType) {
            this.appendType = appendType;
            return this;
        }

        public Builder appendExecutorUserIds(List<String> appendExecutorUserIds) {
            this.appendExecutorUserIds = appendExecutorUserIds;
            return this;
        }

        public ApproveAppendOpsArgs build() {
            return new ApproveAppendOpsArgs(this);
        }
    }
}

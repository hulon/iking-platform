package com.ikingtech.framework.sdk.approve.embedded.caller;

import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceBasicDTO;
import com.ikingtech.framework.sdk.approve.model.ApproveFormInstanceDTO;
import com.ikingtech.framework.sdk.approve.rpc.api.ApproveFormInstanceRpcApi;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class ApproveSubmitAgentCaller implements FrameworkAgentCaller {

    private final ApproveFormInstanceRpcApi rpcApi;

    /**
     * 执行客户端请求
     *
     * @param data 请求数据
     * @return 执行结果
     */
    @Override
    public R<Object> call(Object data) {
        try {
            R<ApproveFormInstanceBasicDTO> result = this.rpcApi.submit((ApproveFormInstanceDTO) data);
            return result.isSuccess() ? R.ok(result.getData()) : R.failed(result.getMsg());
        } catch (Exception e) {
            return R.failed();
        }
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.APPROVE_SUBMIT;
    }

    /**
     * 服务提供方名称
     *
     * @return 客户端类型
     */
    @Override
    public String provider() {
        return "server";
    }
}

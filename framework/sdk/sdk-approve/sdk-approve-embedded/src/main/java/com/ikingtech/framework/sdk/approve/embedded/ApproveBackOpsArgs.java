package com.ikingtech.framework.sdk.approve.embedded;

import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class ApproveBackOpsArgs extends ApproveOpsArgs implements Serializable {

    @Serial
    private static final long serialVersionUID = -478128399278451214L;

    public ApproveBackOpsArgs(Builder builder) {
        super(builder);
        this.backToInstanceNodeId = builder.backToInstanceNodeId;
        this.backToInstanceNodeUserIds = builder.backToInstanceNodeUserIds;
    }

    private final String backToInstanceNodeId;

    private final List<String> backToInstanceNodeUserIds;

    public Builder builder() {
        return new Builder();
    }

    public static class Builder extends ApproveOpsArgs.Builder {

        @Serial
    private static final long serialVersionUID = 2472520885321495384L;

        private String backToInstanceNodeId;

        private List<String> backToInstanceNodeUserIds;

        public Builder backToInstanceNodeId(String backToInstanceNodeId) {
            this.backToInstanceNodeId = backToInstanceNodeId;
            return this;
        }

        public Builder backToInstanceNodeUserIds(List<String> backToInstanceNodeUserIds) {
            this.backToInstanceNodeUserIds = backToInstanceNodeUserIds;
            return this;
        }

        public ApproveBackOpsArgs build() {
            return new ApproveBackOpsArgs(this);
        }
    }
}

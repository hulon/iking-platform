package com.ikingtech.framework.sdk.approve.callback.configuration;

import com.ikingtech.framework.sdk.approve.callback.ApproveCallbackCollector;
import com.ikingtech.framework.sdk.approve.callback.ApproveCallbackResolver;
import com.ikingtech.framework.sdk.approve.callback.ApproveProcessCallback;
import com.ikingtech.framework.sdk.approve.callback.caller.ApproveCallbackFeedbackCaller;
import com.ikingtech.framework.sdk.approve.callback.runner.ApproveCallbackFeedbackRunner;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackCaller;
import com.ikingtech.framework.sdk.web.support.server.FrameworkServerFeedbackRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;

import static com.ikingtech.framework.sdk.context.constant.CommonConstants.GLOBAL_CONFIG_PREFIX;

/**
 * @author tie yan
 */
public class ApproveCallbackConfiguration {

    @Bean(initMethod = "init")
    public ApproveCallbackResolver approveCallbackResolver(List<ApproveProcessCallback> callbacks) {
        return new ApproveCallbackResolver(callbacks);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public ApproveCallbackCollector approveCallbackCollector(StringRedisTemplate redisTemplate, List<ApproveProcessCallback> callbacks) {
        return new ApproveCallbackCollector(redisTemplate, callbacks);
    }

    @Bean
    @ConditionalOnProperty(prefix = GLOBAL_CONFIG_PREFIX + ".web", name = "arch", havingValue = "micro_service")
    public FrameworkServerFeedbackCaller approveCallbackFeedbackCaller(StringRedisTemplate redisTemplate, LoadBalancerClient loadBalancerClient) {
        return new ApproveCallbackFeedbackCaller(redisTemplate, loadBalancerClient);
    }

    @Bean
    public FrameworkServerFeedbackRunner approveCallbackFeedbackRunner(ApproveCallbackResolver resolver) {
        return new ApproveCallbackFeedbackRunner(resolver);
    }
}

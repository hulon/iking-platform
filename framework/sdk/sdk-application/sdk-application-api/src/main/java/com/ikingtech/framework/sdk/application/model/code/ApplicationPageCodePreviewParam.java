package com.ikingtech.framework.sdk.application.model.code;

import lombok.Data;

/**
 * @author tie yan
 */
@Data
public class ApplicationPageCodePreviewParam {

    /**
     * 文件路径
     */
    private String path;

    /**
     * 文件内容
     */
    private String content;
}

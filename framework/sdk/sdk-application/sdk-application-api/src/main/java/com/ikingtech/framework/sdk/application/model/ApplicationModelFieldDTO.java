package com.ikingtech.framework.sdk.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.common.JavaFieldTypeEnum;
import com.ikingtech.framework.sdk.enums.common.TableFieldTypeEnum;
import com.ikingtech.framework.sdk.enums.scffold.ScaffoldQueryCompareTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author zhang qiang
 */
@Data
@Schema(name = "ScaffoldModelFieldDTO", description = "业务模型字段信息")
public class ApplicationModelFieldDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 3122313363922269570L;

    @Schema(name = "id", description = "id")
    private String id;

    @Schema(name = "appCode", description = "应用标识")
    private String appCode;

    @Schema(name = "modelId", description = "业务模型编号")
    private String modelId;

    @Schema(name = "name", description = "字段名称")
    private String name;

    @Schema(name = "type", description = "字段类型")
    private JavaFieldTypeEnum type;

    @Schema(name = "typeName", description = "字段类型名称")
    private String typeName;

    @Schema(name = "genericType", description = "字段泛型")
    private JavaFieldTypeEnum genericType;

    @Schema(name = "genericTypeName", description = "字段泛型名称")
    private String genericTypeName;

    @Schema(name = "remark", description = "字段说明")
    private String remark;

    @Schema(name = "dbTableField", description = "是否为数据库表字段")
    private Boolean dbTableField;

    @Schema(name = "length", description = "数据库表字段长度")
    private String length;

    @Schema(name = "dbTableFieldType", description = "数据库表字段类型")
    private TableFieldTypeEnum dbTableFieldType;

    @Schema(name = "dbTableFieldTypeName", description = "数据库表字段类型名称")
    private String dbTableFieldTypeName;

    @Schema(name = "nullable", description = "数据库表字段是否允许空值")
    private Boolean nullable;

    @Schema(name = "queryField", description = "是否为查询字段")
    private Boolean queryField;

    @Schema(name = "compareType", description = "查询比较类型")
    private ScaffoldQueryCompareTypeEnum compareType;

    @Schema(name = "compareTypeName", description = "查询比较类型名称")
    private String compareTypeName;

    @Schema(name = "syncWithDatasource", description = "是否与数据源保持同步")
    private Boolean syncWithDatasource;

    @Schema(name = "sortOrder", description = "排序值")
    private Integer sortOrder;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

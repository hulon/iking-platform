package com.ikingtech.framework.sdk.application.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.scffold.ScaffoldModelCreationTypeEnum;
import com.ikingtech.framework.sdk.enums.scffold.ScaffoldModelStatusEnum;
import com.ikingtech.framework.sdk.enums.scffold.ScaffoldModelTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldModelDTO", description = "业务模型定义")
public class ApplicationModelDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -8326070440809315735L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "appCode", description = "所属应用标识")
    private String appCode;

    @Schema(name = "name", description = "业务模型名称")
    private String name;

    @Schema(name = "code", description = "业务模型标识")
    private String code;

    @Schema(name = "remark", description = "业务模型描述")
    private String remark;

    @Schema(name = "enableTenant", description = "数据是否租户隔离")
    private Boolean enableTenant;

    @Schema(name = "logicDelete", description = "数据是否逻辑删除")
    private Boolean logicDelete;

    @Schema(name = "type", description = "模型类型")
    private ScaffoldModelTypeEnum type;

    @Schema(name = "typeName", description = "模型类型名称")
    private String typeName;

    @Schema(name = "type", description = "模型状态")
    private ScaffoldModelStatusEnum status;

    @Schema(name = "statusName", description = "模型状态名称")
    private String statusName;

    @Schema(name = "creationType", description = "模型创建类型")
    private ScaffoldModelCreationTypeEnum creationType;

    @Schema(name = "creationTypeName", description = "模型创建类型名称")
    private String creationTypeName;

    @Schema(name = "datasourceId", description = "数据源编号")
    private String datasourceId;

    @Schema(name = "syncTableName", description = "同步表名称")
    private String syncTableName;

    @Schema(name = "fields", description = "业务模型字段集合")
    private List<ApplicationModelFieldDTO> fields;

    @Schema(name = "apis", description = "业务模型API集合")
    private List<ApplicationModelApiDTO> apis;

    @Schema(name = "slaveModels", description = "附属模型集合")
    private List<ApplicationModelRelationDTO> slaveModels;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

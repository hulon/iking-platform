package com.ikingtech.framework.sdk.application.model.code;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ApplicationFieldParam", description = "代码模型-模型字段信息")
public class ApplicationFieldParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -4969966083097535437L;

    @Schema(name = "name", description = "字段名称")
    private String name;

    @Schema(name = "upperCamelName", description = "字段名称(大驼峰)")
    private String upperCamelName;

    @Schema(name = "queryName", description = "查询字段名称")
    private String queryName;

    @Schema(name = "upperCamelQueryName", description = "查询字段名称(大驼峰)")
    private String upperCamelQueryName;

    @Schema(name = "snakeCaseName", description = "字段名称(下划线分割)")
    private String snakeCaseName;

    @Schema(name = "modifier", description = "字段类型")
    private String modifier;

    @Schema(name = "remark", description = "字段说明")
    private String remark;

    @Schema(name = "compareType", description = "查询比较类型")
    private String compareType;

    @Schema(name = "condition", description = "查询条件")
    private String condition;

    @Schema(name = "relateModel", description = "关联模型")
    private ApplicationModelCodeGenParam relateModel;
}

package com.ikingtech.framework.sdk.application.model;

import com.ikingtech.framework.sdk.enums.scffold.ScaffoldModelRelationTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "ScaffoldModelRelationDTO", description = "业务附属模型定义")
public class ApplicationModelRelationDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6389601761590575748L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "modelId", description = "附属模型编号")
    private String modelId;

    @Schema(name = "modelName", description = "附属模型名称")
    private String modelName;

    @Schema(name = "masterModelId", description = "主模型编号")
    private String masterModelId;

    @Schema(name = "foreignKeyFieldId", description = "外键字段编号")
    private String foreignKeyFieldId;

    @Schema(name = "enableQuery", description = "是否开启子表查询")
    private Boolean enableQuery;

    @Schema(name = "relationType", description = "是否开启子表查询")
    private ScaffoldModelRelationTypeEnum relationType;
}

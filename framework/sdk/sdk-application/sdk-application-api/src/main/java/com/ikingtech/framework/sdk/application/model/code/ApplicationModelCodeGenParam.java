package com.ikingtech.framework.sdk.application.model.code;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class ApplicationModelCodeGenParam implements Serializable {

    @Serial
    private static final long serialVersionUID = -8329824927872703004L;

    private String groupId;

    private String artifactId;

    private Boolean slaveModel;

    private String packageName;

    private String modelName;

    private String modelCode;

    private String hyphenModelCode;

    private String upperCamelModelCode;

    private String snakeCaseModelCode;

    private String foreignKeyFieldName;

    private String upperCamelForeignKeyFieldName;

    private List<ApplicationFieldParam> slaveFields;

    private List<ApplicationFieldParam> dbTableFields;

    private List<ApplicationFieldParam> modelFields;

    private List<ApplicationFieldParam> queryFields;

    private String queryExecuteCondition;

    private String masterModelCode;

    private String upperCamelMasterModelCode;

    private String author;

    private String dbTableSerialVersionUid;

    private String modelSerialVersionUid;

    private String queryParamSerialVersionUid;

    private Boolean relateToApprove;

    private String approveBusinessType;
}

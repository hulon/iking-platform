package com.ikingtech.framework.sdk.user.extension;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.user.extension.model.UserInfoEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class UserEventResolver {

    private final List<UserInfoEventReminder> reminders;

    @PostMapping("/user/event/add")
    public R<Map<String, Object>> add(@RequestBody UserInfoEvent userInfo) {
        if (null == userInfo) {
            return R.failed("user extension resolver param is blank");
        }
        this.reminders.forEach(reminder -> reminder.add(userInfo));
        return R.ok();
    }

    @PostMapping("/user/event/delete")
    public R<Map<String, Object>> delete(@RequestBody String userId) {
        this.reminders.forEach(reminder -> reminder.delete(userId));
        return R.ok();
    }

    @PostMapping("/user/event/update")
    public R<Map<String, Object>> update(@RequestBody UserInfoEvent userInfo) {
        if (null == userInfo) {
            return R.failed("user extension resolver param is blank");
        }
        this.reminders.forEach(reminder -> reminder.update(userInfo));
        return R.ok();
    }
}

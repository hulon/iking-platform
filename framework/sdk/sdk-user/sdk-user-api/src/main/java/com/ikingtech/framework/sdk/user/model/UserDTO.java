package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 用户详细信息
 *
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "UserDTO", description = "用户详细信息")
public class UserDTO extends UserBasicDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 2882132595240474031L;

    @Schema(name = "postIds", description = "用户岗位编号列表")
    private List<String> postIds;

    @Schema(name = "posts", description = "用户岗位列表")
    private List<UserPostDTO> posts;

    @Schema(name = "deptIds", description = "用户所属部门编号列表")
    private List<String> deptIds;

    @Schema(name = "departments", description = "用户所属部门列表")
    private List<UserDeptDTO> departments;

    @Schema(name = "roleIds", description = "用户所属角色编号列表")
    private List<String> roleIds;

    @Schema(name = "roles", description = "用户所属角色列表")
    private List<UserRoleDTO> roles;
}

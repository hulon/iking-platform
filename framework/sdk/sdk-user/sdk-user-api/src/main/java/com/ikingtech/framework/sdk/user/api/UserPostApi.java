package com.ikingtech.framework.sdk.user.api;

import com.ikingtech.framework.sdk.user.model.UserPostDTO;

import java.util.List;

/**
 * @author tie yan
 */
public interface UserPostApi {

    /**
     * 根据岗位ID列表加载帖子数据
     *
     * @param postIds 岗位ID列表
     * @return 岗位数据列表
     */
    List<UserPostDTO> loadByIds(List<String> postIds);
}

package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserSocialQueryParamDTO", description = "用户社交号查询参数")
public class UserSocialQueryParamDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 4646212049618538953L;

    @Schema(name = "socialId", description = "第三方平台编号")
    private String socialId;

    @Schema(name = "socialNo", description = "第三方平台用户标识")
    private String socialNo;
}

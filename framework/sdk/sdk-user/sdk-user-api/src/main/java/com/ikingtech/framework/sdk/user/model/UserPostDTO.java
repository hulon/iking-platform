package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "UserPostDTO", description = "用户岗位")
public class UserPostDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -4472789713561231877L;

    @Schema(name = "postId", description = "岗位编号")
    private String postId;

    @Schema(name = "postName", description = "岗位名称")
    private String postName;
}

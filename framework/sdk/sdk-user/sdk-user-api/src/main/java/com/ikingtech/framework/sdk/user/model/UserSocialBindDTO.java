package com.ikingtech.framework.sdk.user.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 用户绑定信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "UserSocialBindDTO", description = "用户绑定信息")
public class UserSocialBindDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6905012656270398627L;

    @Schema(name = "socials", description = "用户第三方平台标识信息")
    private List<UserSocialDTO> socials;

    @Schema(name = "addWhenNotFound", description = "用户不存在时是否新增用户")
    private Boolean addWhenNotFound;

    @Schema(name = "validatePassword", description = "绑定时是否校验密码")
    private Boolean validatePassword;

    @Schema(name = "username", description = "用户名")
    private String username;

    @Schema(name = "password", description = "用户密码")
    private String password;

    @Schema(name = "name", description = "用户姓名，用户不存在时是否新增用户为true时指定")
    private String name;

    @Schema(name = "nickName", description = "用户昵称，用户不存在时是否新增用户为true时指定")
    private String nickName;

    @Schema(name = "avatar", description = "用户头像，用户不存在时是否新增用户为true时指定")
    private String avatar;

    @Schema(name = "phone", description = "用户联系电话，用户不存在时是否新增用户为true时指定")
    private String phone;
}

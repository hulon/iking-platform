package com.ikingtech.framework.sdk.context.datasource;

/**
 * 数据源上下文
 *
 * @author tie yan
 */
public class DataSourceContext {

    private DataSourceContext() {

    }

    private static final InheritableThreadLocal<String> THREAD_LOCAL = new InheritableThreadLocal<>();

    /**
     * 切换数据源
     */
    public static void switchTo(String datasourceId) {
        THREAD_LOCAL.remove();
        THREAD_LOCAL.set(datasourceId);
    }

    public static String get() {
        return THREAD_LOCAL.get();
    }


    /**
     * 删除数据源
     */
    public static void remove() {
        THREAD_LOCAL.remove();
    }
}

package com.ikingtech.framework.sdk.context.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Getter
public class SystemInitEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = -437955586042546846L;

    public SystemInitEvent(Object source) {
        super(source);
    }
}

package com.ikingtech.framework.sdk.context.event.application;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Getter
public class DevApplicationMenuInitEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = 2585732091739829177L;

    private final String appCode;

    private final List<String> tenantCodes;

    public DevApplicationMenuInitEvent(Object source, List<String> tenantCodes, String appCode) {
        super(source);
        this.tenantCodes = tenantCodes;
        this.appCode = appCode;
    }
}

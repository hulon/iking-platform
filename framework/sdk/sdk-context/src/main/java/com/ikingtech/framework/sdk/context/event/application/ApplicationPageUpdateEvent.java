package com.ikingtech.framework.sdk.context.event.application;

import lombok.Data;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Getter
public class ApplicationPageUpdateEvent extends ApplicationEvent implements Serializable {

    @Serial
    private static final long serialVersionUID = 1390703357167290054L;

    private final String id;

    private final String parentId;

    private final String fullPath;

    private final String name;

    private final String component;

    private final String link;

    private final String icon;

    public ApplicationPageUpdateEvent(Object source,
                                      String id,
                                      String parentId,
                                      String fullPath,
                                      String name,
                                      String component,
                                      String link,
                                      String icon) {
        super(source);
        this.id = id;
        this.parentId = parentId;
        this.fullPath = fullPath;
        this.name = name;
        this.component = component;
        this.link = link;
        this.icon = icon;
    }
}

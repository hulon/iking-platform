package com.ikingtech.framework.sdk.message.rpc.api;

import com.ikingtech.framework.sdk.message.api.MessageTemplateApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author tie yan
 */
@FeignClient(value = "server", contextId = "MessageTemplateRpcApi", path = "/message/template")
public interface MessageTemplateRpcApi extends MessageTemplateApi {
}

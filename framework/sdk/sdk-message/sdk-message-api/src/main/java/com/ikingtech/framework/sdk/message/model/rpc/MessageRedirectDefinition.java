package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageRedirectDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = -8781125695855054477L;

    /**
     * 跳转链接
     */
    private String redirectTo;

    /**
     * 跳转链接名称
     */
    private String redirectName;

    /**
     * 排序值
     */
    private Integer sortOrder;
}

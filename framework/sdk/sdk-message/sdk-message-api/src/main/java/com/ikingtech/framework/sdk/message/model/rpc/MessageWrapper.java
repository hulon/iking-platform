package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageWrapper implements Serializable {

    @Serial
    private static final long serialVersionUID = 7404528414923464123L;

    private Boolean ignoreRedirect;
}

package com.ikingtech.framework.sdk.message.model.rpc;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageSendParam {

    /**
     * 租户标识
     */
    @Schema(name = "tenantCode", description = "租户标识")
    private String tenantCode;

    /**
     * 消息标识
     */
    @Schema(name = "messageTemplateKey", description = "消息标识")
    private String messageTemplateKey;

    /**
     * 消息体
     */
    @Schema(name = "message", description = "消息体")
    private Object message;

    /**
     * 忽略用户列表
     */
    @Schema(name = "ignoreUserIds", description = "忽略用户列表")
    private List<String> ignoreUserIds;
}

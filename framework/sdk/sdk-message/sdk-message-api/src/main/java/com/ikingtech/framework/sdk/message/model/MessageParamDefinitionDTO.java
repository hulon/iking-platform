package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageParamDefinitionDTO", description = "消息参数定义信息")
public class MessageParamDefinitionDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = -1643389575066357927L;

    @Schema(name = "id", description = "主键")
    private String id;

    @Schema(name = "template_id", description = "消息模板编号")
    private String templateId;

    @Schema(name = "channel_definition_id", description = "消息发送渠道定义编号")
    private String channelDefinitionId;

    @Schema(name = "paramName", description = "参数名称")
    private String paramName;

    @Schema(name = "paramDescription", description = "参数描述")
    private String paramDescription;

    @Schema(name = "mappedParamName", description = "映射参数名称")
    private String mappedParamName;
}

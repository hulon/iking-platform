package com.ikingtech.framework.sdk.message.model.rpc;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
public class MessageTemplateBeanDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = 3265491568335152863L;

    private String tenantCode;

    private String businessName;

    private String businessKey;

    private String messageTemplateTitle;

    private String messageTemplateKey;

    private Boolean allUser;

    private Boolean internal;

    private String templateContent;

    private List<MessageChannelDefinition> channelDefinitions;

    private List<MessageParamDefinition> paramDefinitions;

    private List<MessageReceiverDefinition> receiverDefinitions;
}

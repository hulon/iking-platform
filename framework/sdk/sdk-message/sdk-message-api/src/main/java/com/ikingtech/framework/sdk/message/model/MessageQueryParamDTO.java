package com.ikingtech.framework.sdk.message.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author tie yan
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(name = "MessageQueryParamDTO", description = "消息查询参数")
public class MessageQueryParamDTO extends PageParam implements Serializable {

    @Serial
    private static final long serialVersionUID = 8767303210649373544L;

    @Schema(name = "businessKey", description = "消息所属业务类型")
    private String businessKey;

    @Schema(name = "businessKeys", description = "消息所属业务类型集合")
    private List<String> businessKeys;

    @Schema(name = "content", description = "消息内容")
    private String messageContent;

    @Schema(name = "deliverStatus", description = "消息发送类型", allowableValues = "DELIVERED, DELIVER_FAILED")
    private String deliverStatus;

    @Schema(name = "readStatus", description = "消息读取类型", allowableValues = "ALL, READ, NO_READ")
    private String readStatus;

    @Schema(name = "channel", description = "推送渠道")
    private String channel;

    @Schema(name = "receiverId", description = "接收者编号")
    private String receiverId;
}

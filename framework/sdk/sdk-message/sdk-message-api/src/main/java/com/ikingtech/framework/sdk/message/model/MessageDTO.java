package com.ikingtech.framework.sdk.message.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ikingtech.framework.sdk.enums.message.MessageDeliverStatusEnum;
import com.ikingtech.framework.sdk.enums.message.MessageReadStatusEnum;
import com.ikingtech.framework.sdk.enums.message.MessageSendChannelEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author tie yan
 */
@Data
@Schema(name = "MessageDTO", description = "消息信息")
public class MessageDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 716579114226034366L;

    @Schema(name = "id", description = "接收人接收消息")
    private String id;

    @Schema(name = "templateId", description = "模板ID")
    private String templateId;

    @Schema(name = "sendChannel", description = "发送消息渠道")
    private MessageSendChannelEnum channel;

    @Schema(name = "sendChannelName", description = "发送消息渠道名称")
    private String channelName;

    @Schema(name = "businessName", description = "业务名称")
    private String businessName;

    @Schema(name = "businessKey", description = "业务标识")
    private String businessKey;

    @Schema(name = "messageTemplateKey", description = "消息模板标识")
    private String messageTemplateKey;

    @Schema(name = "messageTitle", description = "消息的标题")
    private String messageTitle;

    @Schema(name = "messageContent", description = "消息内容")
    private String messageContent;

    @Schema(name = "redirectTo", description = "跳转页面地址")
    private String redirectTo;

    @Schema(name = "receiverId", description = "接收人编号")
    private String receiverId;

    @Schema(name = "receiverName", description = "接收人姓名")
    private String receiverName;

    @Schema(name = "readStatus", description = "消息查看类型")
    private MessageReadStatusEnum readStatus;

    @Schema(name = "readStatusName", description = "消息查看类型名称")
    private String readStatusName;

    @Schema(name = "readTime", description = "消息查看时间")
    private LocalDateTime readTime;

    @Schema(name = "deliverStatus", description = "消息发送状态")
    private MessageDeliverStatusEnum deliverStatus;

    @Schema(name = "deliverStatusName", description = "消息发送状态名称")
    private String deliverStatusName;

    @Schema(name = "cause", description = "发送失败原因")
    private String cause;

    @Schema(name = "deliverTime", description = "发送时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime deliverTime;

    @Schema(name = "createBy", description = "创建人编号")
    private String createBy;

    @Schema(name = "createName", description = "创建人姓名")
    private String createName;

    @Schema(name = "createTime", description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(name = "updateBy", description = "更新人编号")
    private String updateBy;

    @Schema(name = "updateName", description = "更新人姓名")
    private String updateName;

    @Schema(name = "updateTime", description = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;
}

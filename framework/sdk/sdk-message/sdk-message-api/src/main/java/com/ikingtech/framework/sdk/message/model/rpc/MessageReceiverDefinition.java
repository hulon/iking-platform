package com.ikingtech.framework.sdk.message.model.rpc;

import com.ikingtech.framework.sdk.enums.message.MessageReceiverTypeEnum;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author tie yan
 */
@Data
public class MessageReceiverDefinition implements Serializable {

    @Serial
    private static final long serialVersionUID = -1160512517500611003L;

    /**
     * 接收者类型
     */
    private MessageReceiverTypeEnum receiverType;

    /**
     * 参数名称
     */
    private String receiverParamName;
}

package com.ikingtech.framework.sdk.message.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息模板标识信息
 *
 * @author tie yan
 */
@Data
@Schema(name = "MessageBusinessTypeDTO", description = "消息业务类型信息")
public class MessageBusinessTypeDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 8506381701223479030L;

    @Schema(name = "businessKey", description = "消息业务类型标识")
    private String businessKey;

    @Schema(name = "businessName", description = "消息业务类型名称")
    private String businessName;
}

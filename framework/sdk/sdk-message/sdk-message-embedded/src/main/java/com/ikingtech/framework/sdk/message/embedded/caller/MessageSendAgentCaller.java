package com.ikingtech.framework.sdk.message.embedded.caller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.model.rpc.MessageSendParam;
import com.ikingtech.framework.sdk.message.rpc.api.MessageManagementRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author tie yan
 */
@Slf4j
@RequiredArgsConstructor
public class MessageSendAgentCaller implements FrameworkAgentCaller {

    private final MessageManagementRpcApi rpcApi;


    /**
     * 客户端发起远程调用请求
     *
     * @param data 数据
     * @return 执行结果
     */
    @Override
    public R<Object> call(Object data) {
        try {
            return this.rpcApi.send((MessageSendParam) data);
        } catch (Exception e) {
            return R.failed();
        }
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.MESSAGE_SEND;
    }

    /**
     * 服务提供方名称
     *
     * @return 客户端类型
     */
    @Override
    public String provider() {
        return "server";
    }
}

package com.ikingtech.framework.sdk.message.embedded;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.ikingtech.framework.sdk.message.embedded.MessageTemplateBeanDefinitionFactory.MESSAGE_TEMPLATE_BEAN_DEFINITION_MAP;

/**
 * @author tie yan
 */
@Slf4j
public class MessageTemplateBeanDefinitionReporter {

    private boolean stop = false;

    @Async
    public void run() {
        while (!stop) {
            if (Tools.Coll.isNotBlankMap(MESSAGE_TEMPLATE_BEAN_DEFINITION_MAP)) {
                MessageTemplateBeanDefinitionReportParam reportParam = new MessageTemplateBeanDefinitionReportParam();
                reportParam.setBeanDefinitions(new ArrayList<>(MESSAGE_TEMPLATE_BEAN_DEFINITION_MAP.values()));
                R<Object> result = FrameworkAgentProxy.agent().execute(FrameworkAgentTypeEnum.MESSAGE_TEMPLATE_REPORT, reportParam);
                if (result.isSuccess()) {
                    this.stop = true;
                }
            }
            try {
                TimeUnit.MINUTES.sleep(5);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}

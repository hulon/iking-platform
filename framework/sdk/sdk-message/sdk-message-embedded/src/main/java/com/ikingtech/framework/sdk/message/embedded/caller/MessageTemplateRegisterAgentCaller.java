package com.ikingtech.framework.sdk.message.embedded.caller;

import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.enums.common.FrameworkAgentTypeEnum;
import com.ikingtech.framework.sdk.message.model.rpc.MessageTemplateBeanDefinitionReportParam;
import com.ikingtech.framework.sdk.message.rpc.api.MessageTemplateRpcApi;
import com.ikingtech.framework.sdk.web.support.agent.FrameworkAgentCaller;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author tie yan
 */
@Service
@RequiredArgsConstructor
public class MessageTemplateRegisterAgentCaller implements FrameworkAgentCaller {

    private final MessageTemplateRpcApi api;

    /**
     * 执行消息体注册信息
     *
     * @param data 请求数据
     * @return 执行结果
     */
    @Override
    public R<Object> call(Object data) {
        MessageTemplateBeanDefinitionReportParam reportParam = (MessageTemplateBeanDefinitionReportParam) data;
        this.api.report(reportParam);
        return R.ok();
    }

    /**
     * 获取客户端类型
     *
     * @return 客户端类型
     */
    @Override
    public FrameworkAgentTypeEnum type() {
        return FrameworkAgentTypeEnum.MESSAGE_TEMPLATE_REPORT;
    }

    /**
     * 服务提供方名称
     *
     * @return 客户端类型
     */
    @Override
    public String provider() {
        return "server";
    }
}
